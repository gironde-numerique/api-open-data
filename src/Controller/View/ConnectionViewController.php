<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Controller\View;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Classe ConnectionViewController
 * Connection page to swagger API
 *
 * @package ApiOpenData\Controller\View
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
*/
final class ConnectionViewController extends ViewController {

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $args) : ResponseInterface {
        return $this->twig->render($response, 'connexion.html', [
            'currentUrl'        => $request->getUri()->getPath()
        ]);
    }
}