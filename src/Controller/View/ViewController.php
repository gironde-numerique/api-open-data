<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Controller\View;

use Psr\Container\ContainerInterface;
use ApiOpenData\Entity\Enum\Rank;

/**
 * ViewController interface
 * Interface to load page's commons objects
 *
 * @package ApiOpenData\Controller\View
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class ViewController {

     protected $container;

     protected $csrf;

     protected $flash;

     protected $twig;

     protected $user;

     public function __construct(ContainerInterface $container) {
          // Slim container
          $this->container = $container;

          // CSRF guard
          $this->csrf = $this->container->get('csrf');

          // Flash messages
          $this->flash = $this->container->get('flash');

          // Twig view
          $this->twig = $this->container->get('view');

          // Authenticated user
          $this->user = $this->container->get('user');
          $this->twig->getEnvironment()->addGlobal('user', $this->user);

          // Document's total
          $solr = $this->container->get('solr');
          $this->twig->getEnvironment()->addGlobal('totalDocuments', $solr->countDocuments());

          // CSRF tokens
          $csrfArray = array(
               'keys' => ['name' => $this->csrf->getTokenNameKey(), 'value' => $this->csrf->getTokenValueKey()],
               'name'  => $this->csrf->getTokenName(),
               'value' => $this->csrf->getTokenValue()
          );
          $this->twig->getEnvironment()->addGlobal('csrf', $csrfArray);

          // General parameters
          $this->twig->getEnvironment()->addGlobal('adminName',  ADMIN_NAME);
          $this->twig->getEnvironment()->addGlobal('adminEmail', ADMIN_EMAIL);
          $this->twig->getEnvironment()->addGlobal('domain',     DOMAIN);
          $this->twig->getEnvironment()->addGlobal('ranks',      Rank::toArray());
     }
}