<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Controller\View;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Classe HomeViewController
 * Main swagger API page
 *
 * @package ApiOpenData\Controller\View
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
*/
final class HomeViewController extends ViewController {

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $args) : ResponseInterface {
        return $this->twig->render($response, 'accueil.html', [
            'currentUrl'        => $request->getUri()->getPath()
        ]);
    }
}