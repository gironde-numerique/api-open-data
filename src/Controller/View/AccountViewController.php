<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Controller\View;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Classe AccountViewController
 * Connection page to swagger API
 *
 * @package ApiOpenData\Controller\View
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
*/
final class AccountViewController extends ViewController {

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $args) : ResponseInterface {
        return $this->twig->render($response, 'compte.html', [
            'currentUrl'        => $request->getUri()->getPath()
        ]);
    }
}