<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Controller;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use ApiOpenData\Utils\ArrayUtils;
use ApiOpenData\Utils\StringUtils;

/**
 * Abstract class Controller to centralize common properties and functionnalities between controllers
 *
 * @package ApiOpenData\Controller
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
abstract class Controller {

    protected $container;

    protected $sirene;

    protected $solr;

    static protected $startTime;

    public function __construct(ContainerInterface $container) {
        $this->container    = $container;
        $this->sirene       = $this->container->get('sirene');
        $this->solr         = $this->container->get('solr');
        static::$startTime  = $this->container->get('startTime');
    }

    /**
     * Prepare response object with status, header and object results in JSON format.
     *
     * @param Response $response Response from Slim framework routing
     * @param integer $code HTTP status code
     * @param bool $success Flag to indicate if the query was successful
     * @param string $message Query status message
     * @param array $objectArray (optional) Query results as array
     * @param integer $total (optional) Total query results without pagination
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return Response
     * @access public
     */
    public static function writeResponse(Response $response, int $code, bool $success, string $message, $objectArray = null, $total = 0) {
        $time = round(microtime(true) * 1000 - static::$startTime); 
        $response = $response->withStatus($code);
        $response->withHeader('Content-type', 'application/json');
        $responseContent = ['time' => $time, 'success' => $success, 'total' => $total, 'message' => $message];
        if ($objectArray != null && is_array($objectArray) && sizeof($objectArray) > 0) {
            $responseContent[key($objectArray)] = current($objectArray) != null ? json_encode(array_values(current($objectArray))) : '';
        }
        $response->getBody()->write(json_encode($responseContent, JSON_UNESCAPED_UNICODE));

        return $response;
    }

    /**
     * Prepare statement with criterias and dateTime for filtering results in Solr query.
     *
     * @param array $params Parameters array from request
     * @param array $criterias Criterias array to feed
     * @param DateTime $startdatetime Start date as DateTime for filtering results
     * @param DateTime $enddatetime End date as DateTime for filtering results
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @access public
     */
    public static function prepareStatementCriterias(array $params, &$criterias, &$startdatetime, &$enddatetime) {
        if (!empty($params)) {
            $zipcode    = ArrayUtils::get($params, 'zipcode');
            $siren 	    = ArrayUtils::get($params, 'siren');
            $startdate  = ArrayUtils::get($params, 'startdate');
            $enddate    = ArrayUtils::get($params, 'enddate');

            $criterias = array();
            if (!empty($zipcode)) {
                $criterias['codepostal'] = $zipcode;
            }
            if (!empty($siren)) {
                $criterias['siren'] = $siren;
            }
            if (!empty($startdate)) {
                $startdatetime = StringUtils::formatDate($startdate);
            }
            if (!empty($enddate)) {
                $enddatetime = StringUtils::formatDate($enddate);
            }
        }
    }

    /**
     * Prepare statement with criterias and dateTime for filtering results in Solr query.
     *
     * @param array $params Parameters array from request
	 * @param integer $start Search start pointer
	 * @param integer $rows Search number of results
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @access public
     */
    public static function prepareStatementParams(array $params, &$start, &$rows) {
        if (!empty($params)) {
            $queryStart  = ArrayUtils::get($params, 'start');
            $queryRows   = ArrayUtils::get($params, 'size');

            if (!empty($queryStart)) {
                $start = $queryStart;
            }
            if (!empty($queryRows)) {
                $rows = $queryRows;
            }
        }
    }
}
