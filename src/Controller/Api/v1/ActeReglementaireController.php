<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Controller\Api\v1;

use DateTime;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ApiOpenData\Controller\IController;
use ApiOpenData\Controller\Controller;
use ApiOpenData\Entity\ActeReglementaire;

/**
 * ActeReglementaire controller class to get "actes reglementaires" from Solr server.
 *
 * @package ApiOpenData\Controller\Api\v1
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class ActeReglementaireController extends Controller implements IController {

	/**
	 * @OA\Get(
	 *     path="/actes-reglementaires",
	 *     tags={"Actes réglementaires"},
	 *     summary="Service de récupération des actes réglementaires classés par collectivité pour les collectivités du département de la Gironde (33).",
	 *     description="Service de récupération des actes administratifs qui matérialisent les décisions des autorités locales.",
	 *     operationId="getActesReglementaires",
	 *     deprecated=false,
	 *     @OA\Parameter(
	 *         name="zipcode",
	 *         in="query",
	 *         description="Code postal d'une collectivité",
	 *         required=false,
	 *         @OA\Schema(
	 *             type="string"
	 *         )
	 *     ),
	 *     @OA\Parameter(
	 *         name="siren",
	 *         in="query",
	 *         description="SIREN d'une collectivité",
	 *         required=false,
	 *         @OA\Schema(
	 *             type="string"
	 *         )
	 *     ),
	 *     @OA\Parameter(
	 *         name="startdate",
	 *         in="query",
	 *         description="Date de début de la recherche (format dd-mm-yyyy)",
	 *         required=false,
	 *         @OA\Schema(
	 *             type="string",
	 *             format="datetime"
	 *         )
	 *     ),
	 *     @OA\Parameter(
	 *         name="enddate",
	 *         in="query",
	 *         description="Date de fin de la recherche (format dd-mm-yyyy)",
	 *         required=false,
	 *         @OA\Schema(
	 *             type="string",
	 *             format="datetime"
	 *         )
	 *     ),
	 *     @OA\Parameter(
	 *         name="start",
	 *         in="query",
	 *         description="Rang du premier élément demandé dans la réponse, défaut 0",
	 *         required=false,
	 *         @OA\Schema(
	 *             type="integer",
	 *             default=0
	 *         )
	 *     ),
	 *     @OA\Parameter(
	 *         name="size",
	 *         in="query",
	 *         description="Nombre d'éléments demandés dans la réponse, défaut 100",
	 *         required=false,
	 *         @OA\Schema(
	 *             type="integer",
	 *             default=100
	 *         )
	 *     ),
	 *     @OA\Response(
	 *         response=200,
	 *         description="Succès, tous les résultats sont retournés.",
	 *         @OA\JsonContent(
	 *             type="array",
	 *             @OA\Items(ref="#/components/schemas/Collectivite")
	 *         ) 
	 *     ),
	 *     @OA\Response(
	 *         response=204,
	 *         description="Aucun acte réglementaire pour les paramètres saisis."
	 *     ),
	 *     @OA\Response(
	 *         response=206,
	 *         description="Succès, une partie des résultats est retournée par pagination.",
	 *         @OA\JsonContent(
	 *             type="array",
	 *             @OA\Items(ref="#/components/schemas/Collectivite")
	 *         ) 
	 *     ),
	 *     @OA\Response(
	 *         response=400,
	 *         description="Requête erronée, il se peut que le(s) paramètre(s) soi(en)t mal saisi(s)."
	 *     ),
	 *     @OA\Response(
	 *         response=401,
	 *         description="Informations d'authentification invalides."
	 *     ),
	 *     @OA\Response(
	 *         response=403,
	 *         description="Vous n'êtes pas autorisé à accéder à cette ressource."
	 *     ), 
	 *     @OA\Response(
	 *         response=404,
	 *         description="Service introuvable ou non implémenté."
	 *     ),
	 *     @OA\Response(
	 *         response=406,
	 *         description="Informations d'identification manquantes."
	 *     ),
	 *     @OA\Response(
	 *         response=429,
	 *         description="Trop de requêtes, vous avez dépassé votre quota de 30 requêtes par minute."
	 *     ),
	 *     @OA\Response(
	 *         response=500,
	 *         description="Une erreur interne s'est produite lors du traitement de votre requête."
	 *     )
	 * )
	 */
	public function __invoke(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
		// Prepare statement
		$criterias = $startdatetime = $enddatetime = null;
		self::prepareStatementCriterias($request->getQueryParams(), $criterias, $startdatetime, $enddatetime);
		$start = 0;
		$rows = SOLR_ROWS;
		self::prepareStatementParams($request->getQueryParams(), $start, $rows);

		if ($this->solr->testConnection(SOLR_CORE_DOCUMENT)) {
			$total = $totalResults = 0;
			$actesArray = $this->getData($start, $rows, $criterias, $startdatetime, $enddatetime, $total, $totalResults);

			if ($totalResults > 0) {
				return self::writeResponse(
						$response, 
						$totalResults < $total ? 206 : 200,
						true, 
						$totalResults.' sur '.$total.' actes réglementaires retournés', 
						array('actesreglementaires' => $actesArray),
						$total
				);
			} else {
				return self::writeResponse(
						$response, 
						204, 
						true, 
						'Aucun acte réglementaire pour les paramètres saisis.', 
						array('actesreglementaires' => null)
				);
			}
		}
	}

	public function getData(int $start, int $rows, array $criterias = null, ?DateTime $startdatetime = null, ?DateTime $enddatetime = null, ?int &$total = 0, ?int &$totalResults = 0) : array {
		$solrDocuments = $this->solr->getActesReglementaires($start, $rows, $criterias, $startdatetime, $enddatetime);
		$actesArray = array();
		if ($solrDocuments != null 
				&& $solrDocuments->response != null 
				&& $solrDocuments->response->numFound > 0) {

			$total = $solrDocuments->response->numFound;
			$totalResults = sizeof($solrDocuments->response->docs);
			foreach ($solrDocuments->response->docs as $solrActe) {
				// Format actereglementaire.
				$acteReglementaire = new ActeReglementaire(
					$solrActe->documentidentifier[0],
					$solrActe->date[0],
					html_entity_decode(utf8_decode($solrActe->description[0])),
					$solrActe->entity[0],
					$solrActe->siren[0].str_pad($solrActe->nic[0], 5, '0', STR_PAD_LEFT),
					$solrActe->filepath[0]
				);
				$classificationArray = explode(' ', $solrActe->classification[0]);
				if (!empty($classificationArray)) {
					$acteReglementaire->setSubject_code($classificationArray[0]);
					$acteReglementaire->setSubject_name(utf8_decode(str_replace($classificationArray[0].' ', '', $solrActe->classification[0])));
				}

				// New organization
				if (!array_key_exists($solrActe->siren[0], $actesArray)) {
					$collectivite = $this->sirene->getOrganizationInformations($solrActe->siren[0]);
					$collectivite->getActes()->append($acteReglementaire);
					$actesArray[$solrActe->siren[0]] = $collectivite;
				} 
				// Or add act to an existing organization
				else {
					$actesArray[$solrActe->siren[0]]->getActes()->append($acteReglementaire);
				}
			}
		}
		return $actesArray;
	}

    public function getTotal(?array $criterias, DateTime $startdatetime = null, DateTime $enddatetime = null) : int {
		$solrDocuments = $this->solr->getActesReglementaires(0, SOLR_ROWS, $criterias, $startdatetime, $enddatetime);
		$total = 0;
		if ($solrDocuments != null 
				&& $solrDocuments->response != null) {
			$total = $solrDocuments->response->numFound;
		}
		return $total;
	}

}
