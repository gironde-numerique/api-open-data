<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Controller\Api\v1;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ApiOpenData\Controller\Controller;
use ApiOpenData\Utils\ArrayUtils;

/**
 * Equipement public controller class to get public equipments from AccesLibre API.
 *
 * @package ApiOpenData\Controller\Api\v1
 * @author  Alexis ZUCHER <a.zucher@girondenumerique.fr>
 */
final class EquipementPublicController extends Controller {

	/**
	 * @OA\Get(
	 *     path="/equipements-publics",
	 *     tags={"Équipements"},
	 *     summary="Service de récupération des équipements publics pour une collectivité dont le numéro de SIREN a été passé en paramètre sur la base de l'API AccesLibre.",
	 *     description="Service de récupération des équipements publics.",
	 *     operationId="getEquipementsPublics",
	 *     deprecated=false,
	 *     @OA\Parameter(
	 *         name="siren",
	 *         in="query",
	 *         description="SIREN d'une collectivité",
	 *         required=false,
	 *         @OA\Schema(
	 *             type="string"
	 *         )
	 *     ),
	 *     @OA\Response(
	 *         response=200,
	 *         description="Succès, tous les équipements sont retournés.",
	 *         @OA\JsonContent(
	 *             type="array",
	 *             @OA\Items(ref="#/components/schemas/Equipement")
	 *         ) 
	 *     ),
	 *     @OA\Response(
	 *         response=204,
	 *         description="Aucun équipement pour les paramètres saisis."
	 *     ),
	 *     @OA\Response(
	 *         response=206,
	 *         description="Succès, une partie des équipements est retourné par pagination.",
	 *         @OA\JsonContent(
	 *             type="array",
	 *             @OA\Items(ref="#/components/schemas/Equipement")
	 *         ) 
	 *     ),
	 *     @OA\Response(
	 *         response=400,
	 *         description="Requête erronée, il se peut que le(s) paramètre(s) soi(en)t mal saisi(s)."
	 *     ),
	 *     @OA\Response(
	 *         response=401,
	 *         description="Informations d'authentification invalides."
	 *     ),
	 *     @OA\Response(
	 *         response=403,
	 *         description="Vous n'êtes pas autorisé à accéder à cette ressource."
	 *     ), 
	 *     @OA\Response(
	 *         response=404,
	 *         description="Service introuvable ou non implémenté."
	 *     ),
	 *     @OA\Response(
	 *         response=406,
	 *         description="Informations d'identification manquantes."
	 *     ),
	 *     @OA\Response(
	 *         response=429,
	 *         description="Trop de requêtes, vous avez dépassé votre quota de 30 requêtes par minute."
	 *     ),
	 *     @OA\Response(
	 *         response=500,
	 *         description="Une erreur interne s'est produite lors du traitement de votre requête."
	 *     )
	 * )
	 */
	public function __invoke(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
		$accesLibre     = $this->container->get('accesLibre');

		$siren = ArrayUtils::get($request->getQueryParams(), 'siren');
		if (empty($siren)) {
			return $response->withStatus(400);
		} else {
			// Get informations of collectivity
			$collectivite = $this->sirene->getOrganizationInformations($siren);

			// Get every equipements for this collectivity
			$publicEquipmentArray = $accesLibre->getEquipmentInformations($collectivite->getAdresse()->getCode_postal());

			if ($publicEquipmentArray != null && sizeof($publicEquipmentArray) > 0) {
				$total = sizeof($publicEquipmentArray);

				foreach($publicEquipmentArray as $publicEquipement) {
					// Push data of the organization into $equipement
					$publicEquipement->setOrganization_name($collectivite->getName());
					$publicEquipement->setOrganization_siret($collectivite->getSiren().str_pad($collectivite->getNic(), 5, '0', STR_PAD_LEFT));
					// Push data of $equipement into the organization
					$collectivite->getEquipements()->append($publicEquipement);
				}

				// Push the Object "collectivite" into $equipementArray
				$equipementsArray[$collectivite->getSiren()] = $collectivite;

				return self::writeResponse(
					$response, 
					200, 
					true, 
					$total.' équipement(s) retourné(s)', 
					array('equipements' => $equipementsArray),
					$total
				);
			} else {
				return self::writeResponse(
					$response, 
					204, 
					true, 
					'Aucun équipement pour les paramètres saisis.', 
					array('equipements' => null)
				);
			}
		}
	}

}
