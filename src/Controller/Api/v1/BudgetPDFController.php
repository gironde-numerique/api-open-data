<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Controller\Api\v1;

use DomDocument;
use GuzzleHttp\Psr7\LazyOpenStream;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ApiOpenData\Utils\ArrayUtils;

/**
 * Budget PDF controller class to generate budget PDF document from XML file.
 *
 * @package ApiOpenData\Controller\Api\v1
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class BudgetPDFController {

     const CACHE_DIRECTORY = DIR_ROOT.'/src/Controller/cache/';

     /**
     * @OA\Get(
     *     path="/budgetPDF-from-XML",
     *     tags={"Budgets"},
     *     summary="Service de génération d'un document budgétaire à partir du fichier source XML généré par les logiciels métiers.",
     *     description="Service de génération du fichier PDF d'un document budgétaire qui s'appuie sur le composant PRINT-COMP de la suite logiciel TotEM - DéMatérialisation des Actes Budgétaires et dont la génération se base sur des feuilles de styles semblables à celles utilisées dans la visualisation autonome. Pour pouvoir utiliser cette fonctionnalité, il faut que le fichier DocumentBudgetaire soit enrichi et comporte toutes les totalisations.",
     *     operationId="getBudgetPDFfromXML",
     *     deprecated=false,
     *     @OA\Parameter(
     *         name="xmlurl",
     *         in="query",
     *         description="URL du fichier XML stocké sur un serveur de fichier de type données citoyennes",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
	*         response=200,
	*         description="Document budgétaire généré au format PDF.",
	*         @OA\Schema(
     *             type="file"
     *         ) 
	*     ),
	*     @OA\Response(
	*         response=204,
	*         description="Aucun document PDF généré pour ce fichier XML."
     *     ),
     *     @OA\Response(
	*         response=400,
	*         description="Requête erronée, le fichier XML n'est pas un document budgétaire."
     *     ),
	*     @OA\Response(
	*         response=401,
	*         description="Informations d'authentification invalides."
     *     ),
     *     @OA\Response(
	*         response=403,
	*         description="Vous n'êtes pas autorisé à accéder à cette ressource."
	*     ), 
	*     @OA\Response(
	*         response=404,
	*         description="Service introuvable ou non implémenté."
     *     ),
     *     @OA\Response(
	*         response=406,
	*         description="Informations d'identification manquantes."
     *     ),
     *     @OA\Response(
	*         response=429,
	*         description="Trop de requêtes, vous avez dépassé votre quota de 30 requêtes par minute."
	*     ),
	*     @OA\Response(
	*         response=500,
	*         description="Une erreur interne s'est produite lors du traitement de votre requête."
	*     )
     * )
     */
     public function __invoke(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
          if ($request->getQueryParams() != null && sizeof($request->getQueryParams()) > 0) {
               $url = ArrayUtils::get($request->getQueryParams(), 'xmlurl');

               $time = time();
               $xmlTempFile   = self::CACHE_DIRECTORY.'BudgetTmp'.$time.'.xml';
               $pdfBudgetFile = self::CACHE_DIRECTORY.'Budget'.$time.'.pdf';
               set_time_limit(0);
               $fp = fopen($xmlTempFile, 'w+');
               $ch = curl_init($url);
               curl_setopt($ch, CURLOPT_TIMEOUT, 50);
               curl_setopt($ch, CURLOPT_FILE, $fp);
               curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
               curl_exec($ch);
               curl_close($ch);
               fclose($fp);

               if (file_exists($xmlTempFile)) {
                    $xml = new DomDocument();
                    $xml->load($xmlTempFile);
                    if ($xml->getElementsByTagName('DocumentBudgetaire')->length != 0) {
                        exec(JAVA_HOME.' -jar '.DIR_ROOT.'/src/Utils/print-comp.jar '.$xmlTempFile.' '.$pdfBudgetFile);
                    } else {
                         $response = $response->withStatus(400);
                    }

                    unlink($xmlTempFile);
                    if (file_exists($pdfBudgetFile) && filesize($pdfBudgetFile) > 0) {
                         $stream = new LazyOpenStream($pdfBudgetFile, 'r');

                         $response = $response->withHeader('Content-Type', 'application/pdf');
                         $response = $response->withHeader('Content-Description', 'File Transfer');
                         $response = $response->withHeader('Content-Disposition', 'attachment; filename="'.basename($pdfBudgetFile).'"');
                         $response = $response->withHeader('Content-Transfer-Encoding', 'binary');
                         $response = $response->withHeader('Expires', '0');
                         $response = $response->withHeader('Cache-Control', 'must-revalidate');
                         $response = $response->withHeader('Pragma', 'public');
                         $response = $response->withHeader('Content-Length', filesize($pdfBudgetFile));
                         return $response->withBody($stream);
                    } else {
                         $response = $response->withStatus(204);
                    }
               } else {
                    $response = $response->withStatus(204);
               }
          } else {
               $response = $response->withStatus(400);
          }

          return $response;
     }

}
