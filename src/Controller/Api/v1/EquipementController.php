<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Controller\Api\v1;

use DateTime;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ApiOpenData\Controller\IController;
use ApiOpenData\Controller\Controller;
use ApiOpenData\Entity\Equipement;
use ApiOpenData\Entity\Adresse;

/**
 * Equipement controller class to get "equipements" from Solr server
 *
 * @package ApiOpenData\Controller\Api\v1
 * @author  Alexis ZUCHER <a.zucher@girondenumerique.fr>
 */
final class EquipementController extends Controller implements IController {

    /**
	 * @OA\Get(
	 *     path="/equipements-collectifs",
	 *     tags={"Équipements"},
	 *     summary="Service de récupération des équipements collectifs classés par collectivité pour les collectivités du département de la Gironde (33).",
	 *     description="Service de récupération des équipements collectifs.",
	 *     operationId="getEquipements",
	 *     deprecated=false,
	 *     @OA\Parameter(
	 *         name="siren",
	 *         in="query",
	 *         description="SIREN d'une collectivité",
	 *         required=false,
	 *         @OA\Schema(
	 *             type="string"
	 *         )
	 *     ),
	 *     @OA\Parameter(
	 *         name="start",
	 *         in="query",
	 *         description="Rang du premier élément demandé dans la réponse, défaut 0",
	 *         required=false,
	 *         @OA\Schema(
	 *             type="integer",
	 *             default=0
	 *         )
	 *     ),
	 *     @OA\Parameter(
	 *         name="size",
	 *         in="query",
	 *         description="Nombre d'éléments demandés dans la réponse, défaut 100",
	 *         required=false,
	 *         @OA\Schema(
	 *             type="integer",
	 *             default=100
	 *         )
	 *     ),
	 *     @OA\Response(
	 *         response=200,
	 *         description="Succès, tous les équipements sont retournés.",
	 *         @OA\JsonContent(
	 *             type="array",
	 *             @OA\Items(ref="#/components/schemas/Equipement")
	 *         ) 
	 *     ),
	 *     @OA\Response(
	 *         response=204,
	 *         description="Aucun équipement pour les paramètres saisis."
	 *     ),
	 *     @OA\Response(
	 *         response=206,
	 *         description="Succès, une partie des équipements est retourné par pagination.",
	 *         @OA\JsonContent(
	 *             type="array",
	 *             @OA\Items(ref="#/components/schemas/Equipement")
	 *         ) 
	 *     ),
	 *     @OA\Response(
	 *         response=400,
	 *         description="Requête erronée, il se peut que le(s) paramètre(s) soi(en)t mal saisi(s)."
	 *     ),
	 *     @OA\Response(
	 *         response=401,
	 *         description="Informations d'authentification invalides."
	 *     ),
	 *     @OA\Response(
	 *         response=403,
	 *         description="Vous n'êtes pas autorisé à accéder à cette ressource."
	 *     ), 
	 *     @OA\Response(
	 *         response=404,
	 *         description="Service introuvable ou non implémenté."
	 *     ),
	 *     @OA\Response(
	 *         response=406,
	 *         description="Informations d'identification manquantes."
	 *     ),
	 *     @OA\Response(
	 *         response=429,
	 *         description="Trop de requêtes, vous avez dépassé votre quota de 30 requêtes par minute."
	 *     ),
	 *     @OA\Response(
	 *         response=500,
	 *         description="Une erreur interne s'est produite lors du traitement de votre requête."
	 *     )
	 * )
	 */
	public function __invoke(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
		// Prepare statement
		$criterias = $startdatetime = $enddatetime = null;
		self::prepareStatementCriterias($request->getQueryParams(), $criterias, $startdatetime, $enddatetime);
		$start = 0;
		$rows = SOLR_ROWS;
		self::prepareStatementParams($request->getQueryParams(), $start, $rows);

		if ($this->solr->testConnection(SOLR_CORE_EQUIPEMENT)) {
			$total = $totalResults = 0;
			$equipementsArray = $this->getData($start, $rows, $criterias, $startdatetime, $enddatetime, $total, $totalResults);

			if ($totalResults > 0) {
				return self::writeResponse(
					$response, 
					$totalResults < $total ? 206 : 200, 
					true, 
					$totalResults.' sur '.$total.' équipements retournés',
					array('equipements' => $equipementsArray),
					$total
				);
			} else {
				return self::writeResponse(
					$response, 
					204, 
					true, 
					'Aucun équipement pour les paramètres saisis.', 
					array('equipements' => null)
				);
			}
		}
	}
	
	public function getData(int $start, int $rows, array $criterias = null, ?DateTime $startdatetime = null, ?DateTime $enddatetime = null, ?int &$total = 0, ?int &$totalResults = 0) : array {
		$solrEquipements = $this->solr->getEquipements($start, $rows, $criterias, $startdatetime, $enddatetime);
		$equipementsArray = array();
		if ($solrEquipements != null 
				&& $solrEquipements->response != null 
				&& $solrEquipements->response->numFound > 0) {

			$total = $solrEquipements->response->numFound;
			$totalResults = sizeof($solrEquipements->response->docs);
			// Array of every equipments in Json
			$equipementsArray = array();
			foreach ($solrEquipements->response->docs as $solrEquipement) {
				// Object Address
				$adresse = new Adresse();
				if (!empty($solrEquipement->adr_numero[0])) {
					$adresse->setNumero_voie($solrEquipement->adr_numero[0]);
				}
				$adresse->setLibelle_voie($solrEquipement->adr_nomvoie[0]);
				$adresse->setCode_postal($solrEquipement->adr_cp[0]);
				$adresse->setCommune($solrEquipement->adr_ville[0]);

				// Object Equipment
				$equipement = new Equipement();
				if (!empty($solrEquipement->equip_uid[0])) {
					$equipement->setUid($solrEquipement->equip_uid[0]);
				}
				$equipement->setNom($solrEquipement->equip_nom[0]);
				$equipement->setType($solrEquipement->equip_type[0]);
				$equipement->setTheme($solrEquipement->equip_theme[0]);
				$equipement->setTel($solrEquipement->tel[0]);
				$equipement->setAdresse($adresse);
				$equipement->setLatitude($solrEquipement->lat[0]);
				$equipement->setLongitude($solrEquipement->long[0]);
				$equipement->setEmail($solrEquipement->email[0]);
				$equipement->setWeb($solrEquipement->web[0]);

				// New organization
				if (!array_key_exists($solrEquipement->siren[0], $equipementsArray)) {
					// Get information of the organization
					$collectivite = $this->sirene->getOrganizationInformations($solrEquipement->siren[0]);
					// Push data of the organization into $equipement
					$equipement->setOrganization_name($collectivite->getName());
					$equipement->setOrganization_siret($collectivite->getSiren().str_pad($collectivite->getNic(), 5, '0', STR_PAD_LEFT));
					// Push data of $equipement into the organization
					$collectivite->getEquipements()->append($equipement);
					// Push the Object "collectivite" into $equipementArray
					$equipementsArray[$solrEquipement->siren[0]] = $collectivite;
				} 
				// Or add equipement to an existing organization
				else {
					// Push data of the organization into $equipement
					$equipement->setOrganization_name($collectivite->getName());
					$equipement->setOrganization_siret($collectivite->getSiren().str_pad($collectivite->getNic(), 5, '0', STR_PAD_LEFT));
					$equipementsArray[$solrEquipement->siren[0]]->getEquipements()->append($equipement);
				}
			}
		}
		return $equipementsArray;
	}

	public function getTotal(?array $criterias, DateTime $startdatetime = null, DateTime $enddatetime = null) : int {
		$solrDocuments = $this->solr->getEquipements(0, SOLR_ROWS, $criterias, $startdatetime, $enddatetime);
		$total = 0;
		if ($solrDocuments != null 
				&& $solrDocuments->response != null) {
				$total = $solrDocuments->response->numFound;
		}
		return $total;
	}

}
