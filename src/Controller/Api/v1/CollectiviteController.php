<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Controller\Api\v1;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ApiOpenData\Controller\Controller;

/**
 * Collectivite controller class to get "collectivites" from Solr server.
 *
 * @package ApiOpenData\Controller\Api\v1
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class CollectiviteController extends Controller {

     /**
     * @OA\Get(
     *     path="/collectivites",
     *     tags={"Collectivités"},
     *     summary="Service de récupération des collectivités du département de la Gironde (33), adhérentes du syndicat mixte Gironde Numérique et productrices de données publiques.",
     *     description="Service de récupération des collectivités du département de la Gironde (33), adhérentes du syndicat mixte Gironde Numérique et ayant souscrit à l'offre Open Data du catalogue de service de Gironde Numérique et pour lesquels des données sous la forme de documents ont été déposées au sein de l'entrepôt de données de la structure Gironde Numérique faisant office d'agrégateur de données pour ses adhérents. Ce service permet de lister les collectivités pour lesquelles des documents sont consultables.",
     *     operationId="getCollectivites",
     *     deprecated=false,
     *     @OA\Response(
	*         response=200,
	*         description="Succès, tous les résultats sont retournés.",
	*         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Collectivite")
     *         ) 
	*     ),
	*     @OA\Response(
	*         response=204,
	*         description="Aucune collectivité référencée dans le serveur de données."
     *     ),
     *     @OA\Response(
	*         response=400,
	*         description="Requête erronée, il se peut que le(s) paramètre(s) soi(en)t mal saisi(s)."
     *     ),
	*     @OA\Response(
	*         response=401,
	*         description="Informations d'authentification invalides."
     *     ),
     *     @OA\Response(
	*         response=403,
	*         description="Vous n'êtes pas autorisé à accéder à cette ressource."
	*     ), 
	*     @OA\Response(
	*         response=404,
	*         description="Service introuvable ou non implémenté."
     *     ),
     *     @OA\Response(
	*         response=406,
	*         description="Informations d'identification manquantes."
     *     ),
     *     @OA\Response(
	*         response=429,
	*         description="Trop de requêtes, vous avez dépassé votre quota de 30 requêtes par minute."
	*     ),
	*     @OA\Response(
	*         response=500,
	*         description="Une erreur interne s'est produite lors du traitement de votre requête."
	*     )
     * )
     */
     public function __invoke(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
          $sirenArray = $this->solr->getSirens();
          if ($sirenArray != null && sizeof($sirenArray) > 0) {
               $total = sizeof($sirenArray);
               $collectiviteArray = array();
               foreach($sirenArray as $siren) {
                    array_push($collectiviteArray, $this->sirene->getOrganizationInformations($siren));
               }

               return self::writeResponse(
                    $response, 
                    200,
                    true, 
                    $total.' collectivité(s) retournée(s)', 
                    array('collectivites' => $collectiviteArray),
                    $total
               );
          } else {
               return self::writeResponse(
                    $response, 
                    204, 
                    true, 
                    'Aucune collectivité pour les paramètres saisis.', 
                    array('collectivites' => null)
               );
          }          
     }

}
