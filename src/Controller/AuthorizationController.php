<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Controller;

use \Firebase\JWT\JWT;
use \Firebase\JWT\ExpiredException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use ApiOpenData\Controller\Controller;
use ApiOpenData\Entity\Exception\ApiOpenDataException;
use ApiOpenData\Utils\AuthorizationUtils;

/**
 * Authorization controller class to generate and verify bearer token.
 *
 * @package ApiOpenData\Controller
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class AuthorizationController extends Controller {

    const LOCK_FILE_EXTENSION = '.lock';

    /**
     * Generate bearer from a request authentication calling.
     *
     * @param ServerRequestInterface $request Request from Slim framework routing
     * @param RequestHandlerInterface $response Response from Slim framework routing
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return Response
     * @throws ApiOpenDataException if authentication is missing or invalid
     * @access public
     */
    public function generateBearer(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
        $basicAuthentication = $request->getHeader('Authorization');
        if (empty($basicAuthentication) || empty($basicAuthentication[0])) {
            throw new ApiOpenDataException('Informations d\'identification manquantes', 406);
        } else {
            if (strpos($basicAuthentication[0], 'Basic ') !== false) {
                $authentication = base64_decode(str_replace('Basic ', '', $basicAuthentication[0]));
                if (strpos($authentication, ':')) {
                    $authenticationArray = explode(':', $authentication);
                    if (sizeOf($authenticationArray) == 2) {
                        // First verify if consumer is not locked and then verify if call rate is not exceeded
                        $consumerKeyArray = explode('-', $authenticationArray[0]);
                        if ($consumerKeyArray != null && sizeof($consumerKeyArray) > 1) {
                            $consumerId = $consumerKeyArray[0];
                            if (!$this->isConsumerLocked($consumerId) && $this->checkCallQuota($consumerId)) {
                                $response = $response->withStatus(200);
                                $response->withHeader('Content-type', 'application/json');
                                $response->getBody()->write(
                                    json_encode(AuthorizationUtils::generateBearerForKeyAndSecret(
                                        $request, 
                                        $authenticationArray[0], 
                                        $authenticationArray[1]), 
                                        JSON_UNESCAPED_UNICODE
                                    )
                                );
                                return $response;
                            } else {
                                throw new ApiOpenDataException('Compte utilisateur bloqué', 401);
                            }
                        } else {
                            throw new ApiOpenDataException('Informations d\'identification incomplétes', 401);
                        }
                    } else {
                        throw new ApiOpenDataException('Informations d\'identification absentes ou incomplétes', 401);
                    }
                }
            }
            throw new ApiOpenDataException('Informations d\'identification invalides', 401);
        }
    }

    /**
     * Check bearer validity before execute each API main functions queries.
     *
     * @param ServerRequestInterface $request Request from Slim framework routing
     * @param RequestHandlerInterface $response Response from Slim framework routing
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return Response
     * @throws ApiOpenDataException if bearer is missing or invalid
     * @access public
     */ 
    public function checkBearerValidity(ServerRequestInterface $request, RequestHandlerInterface $handler) {
        $token = $request->getHeader('Authorization');
        if (empty($token) || empty($token[0])) {
            throw new ApiOpenDataException('Informations d\'authentification manquantes', 406);
        } else {
            $bearer = $token[0];
            // First verify if bearer is not locked
            if (!self::isBearerLocked($bearer)) {
                $consumerId = $this->isBearerValid($bearer);
                // Then check if call rate is not exceeded
                if ($consumerId > 0 && $this->checkCallQuota($consumerId, $bearer)) {
                    $dao = $this->container->get('dao');
                    if ($dao->registerActivity($consumerId, $request->getUri()->getPath())) {
                        return $handler->handle($request);
                    } else {
                        throw new ApiOpenDataException('Une erreur est survenue lors de la vérification de votre jeton d\'accès', 500);
                    }
                } else {
                    throw new ApiOpenDataException('Informations d\'authentification invalides', 401);
                }
            }
        }
    }

    /**
     * Check JWT bearer validity.
     *
     * @param string $bearer JWT bearer string
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return int consumer id
     * @throws ApiOpenDataException if bearer is unvalid, expired or user does not exist
     * @access private
     */
    private function isBearerValid(string $bearer) {
        if (strpos($bearer, 'Bearer ') !== false) {
            $bearer = str_replace('Bearer ', '', $bearer);
            $dao = $this->container->get('dao');
            $token = $dao->getBearer($bearer);
            if ($token != null) {
                try {
                    $decodedToken = JWT::decode($token->getBearer(), $token->getKey(), array('HS256'));
                    return $decodedToken->data->id;
                } catch (ExpiredException $e) {
                    throw new ApiOpenDataException('Jeton d\'authentification expiré', 401);
                }
            }
        }
        throw new ApiOpenDataException('Informations d\'authentification invalides', 401);
    }

    /**
     * Write a lock file for a specific bearer.
     *
     * @param string $bearer JWT token
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @access private
     */
    private static function lockBearer(string $bearer) {
        $bearerArray = explode('.', $bearer);
        if ($bearerArray != null && sizeof($bearerArray) > 1) {
            self::createLockFile(end($bearerArray).self::LOCK_FILE_EXTENSION);
        } else {
            throw new ApiOpenDataException('Informations d\'authentification invalides', 401);
        }
    }

    /**
     * Write a lock file for a specific consumer.
     *
     * @param int $consumerId Consumer identifier
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @access private
     */
    private static function lockConsumerId(int $consumerId) {
        self::createLockFile($consumerId.self::LOCK_FILE_EXTENSION);
    }

    /**
     * Write a lock file with current timestamp, then throw an exception.
     *
     * @param string $filename Lock file name
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @access private
     */
    private static function createLockFile($filename) {
        $lockFile = fopen(DIR_ROOT.'/src/Controller/cache/'.$filename, 'wb');
        $executionDate = new \DateTime('now');
        fwrite($lockFile, $executionDate->getTimestamp());
        fclose($lockFile);
    }

    /**
     * Check if lock file exists for a specific bearer.
     * The bearer lock file is named with the last part of JWT token.
     * If exists, verify delay, if locking delay is passed, the lock file is deleted.
     *
     * @param string $bearer Tocken  bearer
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return boolean
     * @throws \Exception if the bearer is invalid
     * @access private
     */
    private static function isBearerLocked(string $bearer) {
        $bearerArray = explode('.', $bearer);
        if ($bearerArray != null && sizeof($bearerArray) > 1) {
            $bearerLockFilePath = DIR_ROOT.'/src/Controller/cache/'.end($bearerArray).self::LOCK_FILE_EXTENSION;
            if (file_exists($bearerLockFilePath)) {
                return self::verifyDelayInLockFile($bearerLockFilePath);
            } else {
                return false;
            }
        } else {
            throw new ApiOpenDataException('Informations d\'authentification invalides', 401);
        }
    }

    /**
     * Check if lock file exists for a specific consumerId.
     * If exists, verify delay, if locking delay is passed, the lock file is deleted.
     *
     * @param int $consumerId Consumer identifier
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return boolean
     * @access private
     */
    private function isConsumerLocked(int $consumerId) {
        $consumerLockFilePath = DIR_ROOT.'/src/Controller/cache/'.$consumerId.self::LOCK_FILE_EXTENSION;
        if (file_exists($consumerLockFilePath)) {
            return self::verifyDelayInLockFile($consumerLockFilePath);
        } else {
            return false;
        }
    }

    /**
     * Verify timestamp delay in a lock file. 
     * Return true if delay is passed and delete lock file.
     *
     * @param string $filepath Lock file path
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return boolean
     * @throws \Exception if the delay is not passed
     * @access private
     */
    private static function verifyDelayInLockFile(string $filepath) {
        $timestamp = file_get_contents($filepath);

        // Then
        $expirationDate = new \DateTime();
        $expirationDate->setTimestamp($timestamp);
        // Now
        $executionDate = new \DateTime('now');
        // Difference in seconds
        $delay = $executionDate->getTimestamp() - $expirationDate->getTimestamp();

        if ($delay > LOCK_TIME) {
            unlink($filepath);
            return false;
        } else {
            throw new ApiOpenDataException('Votre compte utilisateur est bloqué pour encore '.(LOCK_TIME - $delay).' secondes', 401); 
        }
    }

    /**
     * Check if the call rate is not exceeded.
     *
     * @param int $consumerId Consumer identifier
     * @param string $bearer (optional) JWT token
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return boolean
     * @throws \Exception if the call rate is exceeded
     * @access private
     */
    private function checkCallQuota(int $consumerId, $bearer = null) {
        $dao = $this->container->get('dao');
        $lastActivitiesCount = $dao->countActivitiesForInterval($consumerId);
        if ($lastActivitiesCount > MAX_CALLS) {
            self::lockConsumerId($consumerId);
            if ($bearer != null && !empty($bearer)) {
                self::lockBearer($bearer);
            }
            throw new ApiOpenDataException('Vous avez dépassez votre quota de '.MAX_CALLS.' appels pour '.INTERVAL.' secondes. Votre compte utilisateur est bloqué pour un délai de '.LOCK_TIME.' secondes', 401);
        } else {
            return true;
        }
    }
}
