<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Controller;

use DateTime;

/**
 * Interface IController to force implementation of getTotal and getData functionnalities
 *
 * @package ApiOpenData\Controller
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
interface IController {

    /**
	 * Get data as array from Solr base.
	 *
 	 * @param int $start Start index
 	 * @param int $rows Number of line to return
 	 * @param array $criterias (optional) Search criterias array
	 * @param DateTime $startdatetime (optional) Start date for filtering
 	 * @param DateTime $enddatetime  (optional) Start date for filtering
 	 * @param int $total Total of deliberations in Solr base
 	 * @param int $totalResults Total of results
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return array
	 * @access public 
	 */
    public function getData(int $start, int $rows, array $criterias = null, ?DateTime $startdatetime = null, ?DateTime $enddatetime = null, ?int &$total = 0, ?int &$totalResults = 0) : array;

    /**
	 * Get total of data line in Solr base.
	 *
	 * @param array $criterias Search criterias array
	 * @param DateTime $startdatetime (optional) Start date for filtering
	 * @param DateTime $enddatetime  (optional) Start date for filtering
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return int
	 * @access public
	 */
	public function getTotal(?array $criterias, ?DateTime $startdatetime = null, ?DateTime $enddatetime = null) : int;

}
