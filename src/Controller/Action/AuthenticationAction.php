<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Controller\Action;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ApiOpenData\Utils\ArrayUtils;
use ApiOpenData\Utils\StringUtils;
use Slim\Exception\HttpBadRequestException;

/**
 * Login action class to authenticate users on swagger UI.
 *
 * @package ApiOpenData\Controller\Action
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class AuthenticationAction extends ActionController {

	protected $session;

	public function __construct(ContainerInterface $container) {
		$this->session = $container->get('session');
		parent::__construct($container);
	}

	public function login(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
		$params = $request->getParsedBody();
		if (!empty($params)) {
			$url      = ArrayUtils::get($params, 'current-url');
			$login    = strtolower(ArrayUtils::get($params, 'login'));
			$password = ArrayUtils::get($params, 'password');

			// Get consumer informations from database
			$dao = $this->container->get('dao');
			$consumer = $dao->getConsumerByEmail($login);
			if ($consumer != null) {
				$compiledPassword = StringUtils::generateHashedPassword($consumer->getCredentials()->getSalt(), md5($password));
				if ($compiledPassword == $consumer->getCredentials()->getHashedPassword()) {
						$this->session->set('user', \serialize($consumer));
						return $response->withHeader('Location', '/swagger');
				}
			}
			$this->flash->addMessage('error', 'Adresse email ou mot de passe erron&eacute;.');

			return $response->withHeader('Location', $url);
		} else {
			throw new HttpBadRequestException($request);
		}
	}

	public function logout(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
		$params = $request->getParsedBody();
		if (!empty($params)) {
			$this->session->clear();
			$this->session->save();
			
			return $response->withHeader('Location', DOMAIN.'/swagger');
		} else {
			throw new HttpBadRequestException($request);
		}
	}
}
