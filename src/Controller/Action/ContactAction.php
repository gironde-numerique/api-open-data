<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Controller\Action;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ApiOpenData\Utils\ArrayUtils;
use ApiOpenData\Utils\MailGenerator;
use Slim\Exception\HttpBadRequestException;

/**
 * Contact action class to send message from contact form.
 *
 * @package ApiOpenData\Controller\Action
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class ContactAction extends ActionController {

     public function __invoke(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
          $params = $request->getParsedBody();
          if (!empty($params)) {
               $url      = ArrayUtils::get($params, 'current-url');
               $subject  = '[Open Data] Message depuis la plateforme';
               if ($this->user != null) {
                    $email1 = $this->user->getEmail();
                    $subject .= ' de '.$this->user->getFirstname().' '.$this->user->getLastname();
                    $emailChecked = true;
               } else {
                    $email1    = ArrayUtils::get($params, 'email');
                    $email2    = ArrayUtils::get($params, 'email-confirmation');
                    $emailChecked = false;
               }
               $position  = ArrayUtils::get($params, 'position');
               $message   = ArrayUtils::get($params, 'message');

               if ($emailChecked || $email1 == $email2) {
                    $emailGenerator = new MailGenerator($this->container->get('view'));
                    $params = array(
                         'user'         => $this->user,
                         'email'        => $email1,
                         'position'     => $position,
                         'message'      => $message
                    );
                    if ($emailGenerator->message('contact', $email1, ADMIN_MAIL, $subject, $params)) {
                         $this->flash->addMessage('success', 'Votre message a bien &eacute;t&eacute; envoy&eacute;, nous vous r&eacute;pondrons dans les meilleurs d&eacute;lais.');
                    } else {
                         $this->flash->addMessage('error', 'Votre message n\'a pas &eacute;t&eacute; envoy&eacute;, veuillez r&eacute;essayer.');
                    }
               } else {
                    $this->flash->addMessage('error', 'Les adresses emails renseign&eacute;es ne correspondent pas, veuillez v&eacute;rifier votre saisie.');
               }

               return $response->withHeader('Location', $url);
          } else {
               throw new HttpBadRequestException($request);
          }
     }
}