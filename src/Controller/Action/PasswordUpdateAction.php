<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Controller\Action;

use ApiOpenData\Utils\ArrayUtils;
use ApiOpenData\Utils\StringUtils;
use ApiOpenData\Entity\Consumer;
use ApiOpenData\Lib\OpenDataDAOConsumer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpBadRequestException;

/**
 * Contact action class to send message from contact form.
 *
 * @package ApiOpenData\Controller\Action
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class PasswordUpdateAction extends ActionController {

     public function __invoke(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
          $params = $request->getParsedBody();
          if (!empty($params)) {
               $url                     = ArrayUtils::get($params, 'current-url');
               $oldPassword             = ArrayUtils::get($params, 'old-password');
               $newPassword             = ArrayUtils::get($params, 'new-password');
               $newPasswordConfirmation = ArrayUtils::get($params, 'new-password-confirmation');

               if ($newPassword == $newPasswordConfirmation) {
                    $dao = $this->container->get('dao');
                    $consumer = $dao->getConsumerByEmail($this->user->getEmail());
                    if ($consumer != null) {
                         $this->updateConsumerPassword($consumer, $dao, $oldPassword, $newPassword);
                    } else {
                         $this->flash->addMessage('error', 'Une erreur a eu lieu lors de la mise &agrave; jour de votre mot de passe, op&eacute;ration annul&eacute;e.');
                    }
               } else {
                    $this->flash->addMessage('warning', 'La confirmation du mot de passe n\'est pas identique au nouveau mot de passe saisi.');
               }

               return $response->withHeader('Location', $url);
          } else {
               throw new HttpBadRequestException($request);
          }
     }

     private function updateConsumerPassword(Consumer $consumer, OpenDataDAOConsumer $dao, string $oldPassword, string $newPassword) {
          $compiledPassword = StringUtils::generateHashedPassword($consumer->getCredentials()->getSalt(), md5($oldPassword));
          if ($compiledPassword == $consumer->getCredentials()->getHashedPassword()) {
               $newPasswordHashed = StringUtils::generateHashedPassword($consumer->getCredentials()->getSalt(), md5($newPassword));
               if ($dao->updateConsumerPassword($this->user->getId(), $newPasswordHashed)) {
                    $this->flash->addMessage('success', 'Votre mot de passe a bien &eacute;t&eacute; modifi&eacute;.');
               }
          } else {
               $this->flash->addMessage('warning', 'Votre mot de passe actuel est incorrect, op&eacute;ration annul&eacute;e.');
          }
     }

}
