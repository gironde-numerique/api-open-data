<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Controller\Action;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ApiOpenData\Utils\AuthorizationUtils;
use ApiOpenData\Entity\Consumer;
use ApiOpenData\Entity\Credentials;
use ApiOpenData\Lib\OpenDataDAOConsumer;
use ApiOpenData\Utils\ArrayUtils;
use ApiOpenData\Utils\MailGenerator;
use ApiOpenData\Utils\StringUtils;
use Slim\Exception\HttpBadRequestException;

/**
 * Subscription action class to subscribe new user on Open Data API.
 *
 * @package ApiOpenData\Controller\Action
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class SubscriptionAction extends ActionController {

     public function __invoke(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
          $params = $request->getParsedBody();
          if (!empty($params)) {
               $url                     = ArrayUtils::get($params, 'current-url');
               $firstname               = ArrayUtils::get($params, 'firstname');
               $lastname                = ArrayUtils::get($params, 'lastname');
               $email                   = ArrayUtils::get($params, 'email');
               $emailConfirmation       = ArrayUtils::get($params, 'email-confirmation');
               $password                = ArrayUtils::get($params, 'password');
               $passwordConfirmation    = ArrayUtils::get($params, 'password-confirmation');

               
               $dao = $this->container->get('dao');
               if ($this->checkSubscriptionInformation($dao, $email, $emailConfirmation, $password, $passwordConfirmation)) {
                    $salt			= StringUtils::generatePasswordSalt();
                    $hashedPassword     = StringUtils::generateHashedPassword($salt, md5($password));
				
				$consumer = new Consumer(0, $firstname, $lastname, $email);
                    $consumer->setCredentials(new Credentials(0, $hashedPassword, $salt));
				$consumerId = $dao->addConsumer($consumer);
				
				if ($consumerId > 0) {
					if (!AuthorizationUtils::generateKeys($consumerId)) {
                              $this->flash->addMessage('warning', 'Vos identifiants API n\'ont pas &eacute;t&eacute; g&eacute;n&eacute;r&eacute;s, veuillez le faire ult&eacute;rieurement &agrave; partir de votre espace utilisateur.');
					}
					if (!AuthorizationUtils::generateBearerForConsumer($email)) {
                              $this->flash->addMessage('warning', 'Votre jeton d\'acc&egrave;s API n\'a pas &eacute;t&eacute; g&eacute;n&eacute;r&eacute;, veuillez le faire ult&eacute;rieurement &agrave; partir de votre espace utilisateur.');
					}

                         $subject = '[Open Data Gironde Numérique] Inscription';
                         $emailGenerator = new MailGenerator($this->container->get('view'));
                         $params = array(
                              'email'        => $email,
                              'password'     => $password
                         );
                         if ($emailGenerator->message('inscription', ADMIN_MAIL, $email, $subject, $params)) {
                              $this->flash->addMessage('success', 'Votre compte a bien &eacute;t&eacute; cr&eacute;&eacute;, un email de confirmation vous a &eacute;t&eacute; adress&eacute; avec vos identifiants &agrave; conserver pr&eacute;cieusement.<br />Vous pouvez d&eacute;sormais vous connecter et utiliser l\'API Open Data Gironde Num&eacute;rique par le biais de l\'&eacute;cran de <a href="/swagger/connexion">connexion</a>.');
                         } else {
                              $this->flash->addMessage('warning', 'L\'email confirmant votre inscription n\'a pas pu &ecirc;tre envoy&eacute;.');

                              $this->flash->addMessage('success', 'Votre compte a bien &eacute;t&eacute; cr&eacute;&eacute;.<br />Vous pouvez d&eacute;sormais vous connecter et utiliser l\'API Open Data Gironde Num&eacute;rique par le biais de l\'&eacute;cran de <a href="/swagger/connexion">connexion</a>.');
                         }
				} else {
                         $this->flash->addMessage('error', 'Une erreur est survenue lors de la cr&eacute;ation de votre compte, veuillez r&eacute;essayer.<br />
					Si le probl&egrave;me persiste, veuillez contacter l\'administrateur de cette plateforme.');
				}
               }

               return $response->withHeader('Location', $url);
		} else {
               throw new HttpBadRequestException($request);
          }
     }

     private function checkSubscriptionInformation(OpenDataDAOConsumer $dao, string $email, string $emailConfirmation, string $password, string $passwordConfirmation) : bool {
          $continue = true;
          if ($email != $emailConfirmation) {
               $this->flash->addMessage('warning', 'Les adresses emails renseign&eacute;es ne correspondent pas, veuillez v&eacute;rifier votre saisie.');
               $continue = false;
          }
          if ($password != $passwordConfirmation) {
               $this->flash->addMessage('warning', 'La confirmation du mot de passe n\'est pas identique au nouveau mot de passe saisi.');
               $continue = false;
          }
          if ($continue && $dao->getConsumerByEmail($email) != null) {
               $this->flash->addMessage('warning', 'Un compte utilisateur existe d&eacute;j&agrave; pour cette adresse email.<br />Veuillez r&eacute;initialiser votre mot de passe par le biais de la page de connexion.');
               $continue = false;
          }
          
          return $continue;
     }

}
