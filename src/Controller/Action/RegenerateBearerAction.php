<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Controller\Action;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ApiOpenData\Utils\AuthorizationUtils;
use ApiOpenData\Utils\ArrayUtils;
use Slim\Exception\HttpBadRequestException;

/**
 * Regenerate bearer action class to regenerate bearer from swagger UI.
 *
 * @package ApiOpenData\Controller\Action
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class RegenerateBearerAction extends ActionController {

     public function __invoke(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
          $params = $request->getParsedBody();
          if (!empty($params)) {
              $url = ArrayUtils::get($params, 'current-url');

              if ($this->user->getKey() != null && $this->user->getSecret() != null) {
                    if (AuthorizationUtils::generateBearerForKeyAndSecret($request, $this->user->getKey(), $this->user->getSecret())) {
                         // Get regenerated bearer from database then update it on session
                         $dao = $this->container->get('dao');
                         $token = $dao->getBearerByConsumerId($this->user->getId());
                         $this->user->getToken()->setBearer($token->getBearer());
                         $this->user->getToken()->setKey($token->getKey());
                         $_SESSION['user']= \serialize($this->user);
                         $this->flash->addMessage('success', 'Votre jeton d\'acc&egrave;s a bien &eacute;t&eacute; reg&eacute;n&eacute;r&eacute;.');
                    } else {
                         $this->flash->addMessage('error', 'Une erreur est survenue lors de la reg&eacute;n&eacute;ration de votre jeton d\'acc&egrave;s. Votre jeton actuel est conserv&eacute;.');
                    }
               } else {
                    $this->flash->addMessage('error', 'Vous devez pr&eacute;alablement g&eacute;n&eacute;r&eacute; vos identifiants API afin de pouvoir g&eacute;n&eacute;rer un jeton d\'acc&egrave;s.');
               }

               return $response->withHeader('Location', $url);
          } else {
               throw new HttpBadRequestException($request);
          }
     }

}
