<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Controller\Action;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ApiOpenData\Entity\Publication;
use ApiOpenData\Utils\ArrayUtils;
use Slim\Exception\HttpBadRequestException;

/**
 * Publication action class to manage data's organization publication.
 *
 * @package ApiOpenData\Controller\Action
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class PublicationAction extends ActionController {

     protected $dao;

     public function __construct(ContainerInterface $container) {
          parent::__construct($container);
          $this->dao = $this->container->get('dao');
     }

     public function load(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
          $results = $this->dao->getPublications();
          $response->getBody()->write(json_encode($results));
          return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
     }

     public function add(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
          $params = $request->getParsedBody();
          if (!empty($params)) {
               $url           = ArrayUtils::get($params, 'current-url');
               $siren         = trim(ArrayUtils::get($params, 'publication-siren'));
               $name          = ArrayUtils::get($params, 'publication-name');
               $frequency     = ArrayUtils::get($params, 'publication-frequency');
               $active        = ArrayUtils::get($params, 'publication-active') == 1 ? true : false;

               $publication = new Publication($siren, $name, $frequency, $active);
               if ($this->dao->addPublication($publication)) {
                    $this->flash->addMessage('success', 'La publication a bien &eacute;t&eacute; enregistr&eacute;e.');
               } else {
                    $this->flash->addMessage('error', 'Une erreur est survenue lors de l\'ajout de la publication.');
               }
               return $response->withHeader('Location', $url);
		} else {
               throw new HttpBadRequestException($request);
          }
     }

     public function update(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
          $params = $request->getParsedBody();
          if (!empty($params)) {
               $url           = ArrayUtils::get($params, 'current-url');
               $siren         = trim(ArrayUtils::get($params, 'publication-siren'));
               $name          = ArrayUtils::get($params, 'publication-name');
               $frequency     = ArrayUtils::get($params, 'publication-frequency');
               $active        = ArrayUtils::get($params, 'publication-active') == 1 ? true : false;

               $publication = new Publication($siren, $name, $frequency, $active);
               if ($this->dao->updatePublication($publication)) {
                    $this->flash->addMessage('success', 'Les informations de la publication ont bien &eacute;t&eacute; modifi&eacute;es.');
               } else {
                    $this->flash->addMessage('error', 'Une erreur est survenue lors de l\'enregistrement de la publication.');
               }
               return $response->withHeader('Location', $url);
		} else {
               throw new HttpBadRequestException($request);
          }
     }

     public function delete(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
          $params = $request->getParsedBody();
          if (!empty($params)) {
               $url      = ArrayUtils::get($params, 'current-url');
               $siren    = trim(ArrayUtils::get($params, 'publication-siren'));

               if (!empty($siren) && $this->dao->deletePublication($siren)) {
                    $this->flash->addMessage('success', 'La publication a bien &eacute;t&eacute; supprim&eacute;e.');
               } else {
                    $this->flash->addMessage('error', 'Une erreur est survenue lors de la suppression de la publication.');
               }
               return $response->withHeader('Location', $url);
		} else {
               throw new HttpBadRequestException($request);
          }
     }

}
