<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Controller\Action;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ApiOpenData\Utils\AuthorizationUtils;
use ApiOpenData\Utils\ArrayUtils;
use Slim\Exception\HttpBadRequestException;

/**
 * Regenerate keys action class to regenerate user key and secret.
 *
 * @package ApiOpenData\Controller\Action
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class RegenerateKeysAction extends ActionController {

     public function __invoke(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
          $params = $request->getParsedBody();
          if (!empty($params)) {
              $url = ArrayUtils::get($params, 'current-url');

              if (AuthorizationUtils::generateKeys($this->user->getId())) {
                    $this->flash->addMessage('success', 'Vos identifiants API ont bien &eacute;t&eacute; reg&eacute;n&eacute;r&eacute;s.');
               } else {
                    $this->flash->addMessage('error', 'Une erreur est survenue lors de la reg&eacute;n&eacute;ration de vos identifiants. Vos anciens identifiants sont conserv&eacute;s.');
               }

               return $response->withHeader('Location', $url);
          } else {
               throw new HttpBadRequestException($request);
          }
     }

}
