<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Controller\Action;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ApiOpenData\Utils\ArrayUtils;
use ApiOpenData\Utils\MailGenerator;
use ApiOpenData\Utils\StringUtils;
use Slim\Exception\HttpBadRequestException;

/**
 * Forgotten password action class to reinitialize password and send an email to consumer with his new password.
 *
 * @package ApiOpenData\Controller\Action
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class ForgottenPasswordAction extends ActionController {

     public function __invoke(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
          $params = $request->getParsedBody();
          if (!empty($params)) {
               $url   = ArrayUtils::get($params, 'current-url');
               $email = ArrayUtils::get($params, 'email');

               $dao = $this->container->get('dao');
               if ($dao->getConsumerByEmail($email)) {
                    $salt               = StringUtils::generatePasswordSalt();
                    $newPassword        = StringUtils::generateRandomString(10);
                    $newPasswordHashed  = StringUtils::generateHashedPassword($salt, md5($newPassword));
                    
                    if ($dao->reinitializePassword($email, $newPasswordHashed, $salt)) {

                         $subject = '[Open Data Gironde Numérique] Réinitialisation mot de passe';
                         $emailGenerator = new MailGenerator($this->container->get('view'));
                         $params = array(
                              'email'        => $email,
                              'newPassword'  => $newPassword
                         );
                         if ($emailGenerator->message('reinitilisation', ADMIN_MAIL, $email, $subject, $params)) {
                              $this->flash->addMessage('success', 'Votre mot de passe a bien &eacute;t&eacute; r&eacute;initialis&eacute;.<br />Un email vous a &eacute;t&eacute; adress&eacute; avec votre nouveau mot de passe. Nous vous invitons &agrave; changer celui-ci par un de votre choix lors de votre prochaine connexion.');
                         } else {
                              $this->flash->addMessage('error', 'L\'email vous adressant votre nouveau mot de passe n\a pas pu vous &ecirc;tre adress&eacute;.<br />Nous vous invitons &agrave; contacter l\'administrateur de cette plateforme depuis le formulaire de contact afin de r&eacute;soudre cette situation');
                         }
                    } else {
                         $this->flash->addMessage('error', 'Une erreur a eu lieu lors de la r&eacute;initialisation de votre mot de passe, veuillez r&eacute;essayer.<br />Si le probl&egrave;me persiste, veuillez contacter l\'administrateur de cette plateforme.');
                    }
               } else {
                    $this->flash->addMessage('warning', 'Aucun utilisateur enregistr&eacute; pour l\'email '.$email.'.');
               }

               return $response->withHeader('Location', $url);
          } else {
               throw new HttpBadRequestException($request);
          }
     }

}
