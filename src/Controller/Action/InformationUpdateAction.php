<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Controller\Action;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ApiOpenData\Utils\ArrayUtils;
use Slim\Exception\HttpBadRequestException;

/**
 * Contact action class to send message from contact form.
 *
 * @package ApiOpenData\Controller\Action
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class InformationUpdateAction extends ActionController {

     public function __invoke(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface {
          $params = $request->getParsedBody();
          if (!empty($params)) {
               $url           = ArrayUtils::get($params, 'current-url');
               $firstname     = ArrayUtils::get($params, 'firstname');
               $lastname      = ArrayUtils::get($params, 'lastname');

               $dao = $this->container->get('dao');
               if ($dao->updateConsumerInformation($this->user->getId(), $firstname, $lastname)) {
                    $this->user->setFirstname($firstname);
                    $this->user->setLastname($lastname);
                    $_SESSION['user'] = \serialize($this->user);
                    $this->flash->addMessage('success', 'Vos informations ont bien &eacute;t&eacute; mises &agrave; jour.');
               } else {
                    $this->flash->addMessage('error', 'Une erreur a eu lieu lors de la mise &agrave; jour de vos informations, op&eacute;ration annul&eacute;e.');
               }

               return $response->withHeader('Location', $url);
          } else {
               throw new HttpBadRequestException($request);
          }
     }

}
