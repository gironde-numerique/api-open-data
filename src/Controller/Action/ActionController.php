<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Controller\Action;

use Psr\Container\ContainerInterface;

/**
 * ActionController interface class to load commons object for action controllers.
 *
 * @package ApiOpenData\Controller\Action
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
*/
class ActionController {

     protected $container;

     protected $flash;

     protected $user;

     public function __construct(ContainerInterface $container) {
          // Slim container
          $this->container = $container;

          // Flash messages
          $this->flash = $this->container->get('flash');

          // Authenticated user
          $this->user = $this->container->get('user');
     }
}