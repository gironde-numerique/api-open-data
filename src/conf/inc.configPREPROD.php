<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData;

/**
 * API properties file
 *
 * @package ApiOpenData
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */

// General properties
define('DIR_ROOT',      __DIR__.'/../');
define('ADMIN_NAME',    'Xavier MADIOT');
define('ADMIN_EMAIL',   'x.madiot@girondenumerique.fr');
define('DOMAIN',        'https://api-opendata-test.girondenumerique.fr');

// Environment properties
define('DEVELOPMENT',   false);
define('PRODUCTION',    true);
define('DEBUG',         false);
define('LOGGING',       false);

// Java
define('JAVA_HOME',     '/opt/java/bin/java');

// JWT properties
define('ISS',           'http://www.girondenumerique.fr');
define('AUD',           'https://api-opendata.girondenumerique.fr');

// Sirene V3 API properties
define('SIRENE_KEY',    '****************************');
define('SIRENE_SECRET', '****************************');

// Solr server properties
define('SOLR_SERVER',           '********************');
define('SOLR_USER',             'solr');
define('SOLR_PASSWORD',         '******');
define('SOLR_PORT',             8983);
define('SOLR_CORE_DOCUMENT',    'documents');
define('SOLR_CORE_EQUIPEMENT',  'equipements');
define('SOLR_ROWS',             100);

// Database properties
define('DB_SERVER',     'localhost');
define('DB_USER',       'api-opendata');
define('DB_PASSWORD',   '************');
define('DB_BASE',       'api-opendata');

// API limitation
define('MAX_CALLS',     30);        // Max for calls / interval
define('INTERVAL',      60);        // In seconds
define('LOCK_TIME',     30);        // In seconds