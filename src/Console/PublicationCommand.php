<?php

namespace ApiOpenData\Console;

use DateTime;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use ApiOpenData\Controller\Controller;
use ApiOpenData\Controller\Api\v1\ActeReglementaireController;
use ApiOpenData\Controller\Api\v1\BudgetController;
use ApiOpenData\Controller\Api\v1\DeliberationController;
use ApiOpenData\Controller\Api\v1\EquipementController;
use ApiOpenData\Entity\Dataset;
use ApiOpenData\Entity\Publication;
use ApiOpenData\Entity\State;
use ApiOpenData\Entity\Enum\Datatype;
use ApiOpenData\Entity\Enum\Status;
use ApiOpenData\Entity\Exception\ApiOpenDataException;
use ApiOpenData\Entity\JsonLD\DCATCatalog;
use ApiOpenData\Entity\JsonLD\DCATDataset;
use ApiOpenData\Entity\JsonLD\DCATDistribution;
use ApiOpenData\Utils\ArrayUtils;
use ApiOpenData\Utils\StringUtils;

/** * Publication command class to publish data in DCAT format
 *
 * @package ApiOpenData\Console
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
final class PublicationCommand extends Command {

	/** @var ContainerInterface */
	private $container;

	/** @var OpenDataDAOConsumer */
	private $dao;

	/** @var SireneConsumer */
	private $sirene;

	protected $dataDirectory = DIRECTORY_SEPARATOR.'datasets'.DIRECTORY_SEPARATOR;

	private $io = null;

	private $datatypes 	= null;

	private $dcatDatasets 	= array();

	/**
	 * Constructor.
	 *
	 * @param ContainerInterface $container The container
	 * @param string|null $name The script name
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 */
	public function __construct(ContainerInterface $container, ?string $name = null) {
		parent::__construct($name);
		$this->container 	= $container;
		$this->container->set('startTime', function() { return microtime(true) * 1000; });
		$this->dao			= $this->container->get('dao');
		$this->sirene		= $this->container->get('sirene');
		$this->datatypes	= array(
			Datatype::DELIB 	=> new DeliberationController($this->container),
			Datatype::ACTEREG 	=> new ActeReglementaireController($this->container),
			Datatype::BUDG 		=> new BudgetController($this->container),
			Datatype::EQUIP		=> new EquipementController($this->container)
		);
		setlocale(LC_ALL, 'fr_FR.UTF-8');
	}

	/**
	 * Configure command.
	 *
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return void
	 */
	protected function configure() : void {
		parent::configure();
		$this->setName('publication');
		$this->setDescription('Data publication in DCAT format');
	}

	/**
	 * Publication execute command.
	 *
	 * @param InputInterface $input The input
	 * @param OutputInterface $output The output
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return int The error code, 0 on success
	 * @access protected
	 */
	protected function execute(InputInterface $input, OutputInterface $output) : int {
		$this->io = new SymfonyStyle($input, $output);
		$this->io->title('Exécution du script de publication des données Open Data au format DCAT');
		if ($input->hasArgument('siren') && !empty($input->getArgument('siren'))) {
			$siren = trim($input->getArgument('siren'));
			$publication = $this->dao->getPublication($siren);
			if ($publication != null) {
				$this->getOrganizationAndData($publication);
			} else {
				$this->io->warning('Aucune publication paramétrée pour le numéro SIREN : '.$siren);
			}
		} else {
			$publications = $this->dao->getPublications();
			if ($publications != null) {
				foreach ($publications as $publication) {
					$this->getOrganizationAndData($publication);
					// Sleep 10 seconds to avoid Sirene V3 problems...
					sleep(10);
				}
			} else {
				$this->io->warning('Aucune publication paramétrée');
			}
		}
		
		return 0;
	}

	/**
	 * Get organization informations from Sirene et each data from Solr server.
	 *
	 * @param Publication $publication Parameterized publication line
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return void
	 * @access private
	 */
	private function getOrganizationAndData(Publication $publication) : void {
		unset($this->dcatDatasets);
		$this->dcatDatasets = array();
		$statusCode 	= Status::OK();
		$statusMessage 	= '';
		$executionDate 	= new DateTime();
		// Publication can be executed if execution date is null, or if the delay has been passed, or is in error
		if (($publication->getState() != null && $publication->getState()->getCode() == Status::ERR()->getValue())
				|| $publication->getLastExecution() == null 
				|| $executionDate->diff($publication->getLastExecution())->format('%a') > $publication->getFrequency()) {
			$this->io->section('Début publication des données de la collectivité SIREN : '.$publication->getSiren());
			// Get organization informations from Sirene V3 API.
			$siren = $publication->getSiren();
			$organization = $this->sirene->getOrganizationInformations($siren);
			$totalData = 0;
			if ($organization != null) {
				// Process each datasets type
				foreach ($this->datatypes as $type => $controller) {
					$this->processDatatypes($controller, $publication, $type, $totalData, $statusMessage);
				}

				// Set DCAT catalog
				if ($totalData == 0) {
					$statusCode = Status::WARN();
					$statusMessage = 'Aucun jeu de données pour la collectivité '.$organization->getName();
					$this->io->warning($statusMessage);
				} else {
					// Register DCAT JSON-LD file
					$publication->setUrl($this->saveCatalogFile($siren, new DCATCatalog($this->dcatDatasets)));
					$statusMessage .= $totalData.' lignes de données récupérées pour la collectivité '.$organization->getName();
					$this->io->success($statusMessage);
				}
			} else {
				$statusCode = Status::ERR();
				$statusMessage = 'La collectivité au numéro SIREN '.$publication->getSiren().' n\'existe pas';
				$this->io->error($statusMessage);
			}

			// Save publication status in database
			$publication->setState(new State($statusCode->getValue(), $statusMessage));
			$this->dao->saveStatusInPublication($publication);
		} else {
			$this->io->info('La publication de la collectivité '.$publication->getName().' ('.$publication->getSiren().') a déjà été exécutée');
		}
	}

	/**
	 * Process data extraction and files writing for a specific dataset type.
	 *
	 * @param Controller $controller GN Open Data API controller
	 * @param Publication $publication Publication information
	 * @param string $type Dataset type
	 * @param int $totalData Total of data for each previous processed dataset
	 * @param string $statusMessage Publication execution status message
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return void
	 * @access private
	 */
	private function processDatatypes(Controller $controller, Publication $publication, string $type, int &$totalData, string &$statusMessage) {
		$siren = $publication->getSiren();
		$criterias = array('siren' => $siren);
		$this->io->info('Jeu de données : '.$type);
		$total = $controller->getTotal($criterias);
		$totalData += $total;
		$this->io->text('Nombre de lignes : '.$total);
		$data = $controller->getData(0, $total, $criterias);
		// Save data in JSON and CSV files
		if ($total > 0) {
			if ($publication->getDatasets() != null 
				&& sizeof($publication->getDatasets()) > 0) {
				$dataset = ArrayUtils::get($publication->getDatasets(), $type);
				// Update dataset information only if there is a changement : 
				// - Difference between number of line
				if ($dataset != null 
						&& $dataset->getNumberOfLine() != $total) {
					$dataset->setNumberOfLine($total);
					$dataset->setUpdateDate(new DateTime('NOW'));
				}
			} else {
				$dataset = new Dataset($siren, new Datatype($type), $total);
				$dataset->setCreationDate(new DateTime('NOW'));
			}
			$distributions = array();
			array_push($distributions, $this->saveDataInJSONFile($dataset, $siren, array($type => $data), $statusMessage));
			$content = ($type == Datatype::EQUIP) ? $data[$siren]->getEquipements() : $data[$siren]->getActes();
			array_push($distributions, $this->saveDataInCSVFile($siren, $content, $type, $statusMessage));

			// Add specific organization's keywords
			$keywords = ArrayUtils::get(Datatype::getProperty($type), 'keywords');
			array_push($keywords, $publication->getName());
			array_push($keywords, strval($publication->getSiren()));
			$dcatDataset = new DCATDataset(
				// Unique identifier like http://localhost/datasets/200010049/deliberations_20210312150817786
				DOMAIN.'/datasets/'.$siren.'/'.$type.'_'.$this->getMicroTime($dataset->getCreationDate()),
				ArrayUtils::get(Datatype::getProperty($type), 'title'),
				ArrayUtils::get(Datatype::getProperty($type), 'description'),
				$keywords,
				StringUtils::formatDateTime($dataset->getCreationDate(), 'Y-m-d\TH:i:s\Z'),
				$publication->getFrequency(),
				StringUtils::formatDateTime($dataset->getUpdateDate(), 'Y-m-d\TH:i:s\Z'),
				$distributions
			);
			array_push($this->dcatDatasets, $dcatDataset->getArray());
			$this->dao->saveDataset($dataset);
		}
	}

	/**
	 * Write DCAT JSON-LD file.
	 *
	 * @param int $siren The organization's siren
	 * @param DCATCatalog $catalog DCAT Catalog datasets content
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return string
	 * @access private
	 */
	private function saveCatalogFile(int $siren, DCATCatalog $catalog) : string {
		try {
			// Create organization directory if doesn't exist
			$directory = $this->checkOrganizationDirectory($siren);
			if ($catalog != null) {
				$filepath = $directory.DIRECTORY_SEPARATOR.$siren.'.json';
				$fp = fopen(DIR_ROOT.$filepath, 'w');
				fwrite($fp, StringUtils::replaceHTMLEntities($catalog->generate()));
				fclose($fp);

				$this->io->info('Fichier DCAT au format JSON-LD '.$filepath.' créé');
				return DOMAIN.$filepath;
			}
		} catch (\Exception $e) {
			$this->io->error('Une erreur est survenue lors de l\'écriture du fichier DCAT au format JSON-LD pour l\'organisation : '.$siren);
		}
	}

	/**
	 * Write data in a JSON file.
	 *
	 * @param Dataset $dataset The current dataset
	 * @param int $siren The organization's siren
	 * @param array $content Data contents
	 * @param string $type Data content type
	 * @param string $statusMessage Publication execution status message
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return array
	 * @access private
	 */
	private function saveDataInJSONFile(Dataset &$dataset, int $siren, array $content, string &$statusMessage) : array {
		try {
			// Create organization directory if doesn't exist
			$directory = $this->checkOrganizationDirectory($siren);
			if (!empty($content)) {
				$jsonContent = current($content) != null ? json_encode(array_values(current($content)), JSON_UNESCAPED_SLASHES) : '';
				$filepath = $directory.DIRECTORY_SEPARATOR.key($content).'.json';
				$fp = fopen(DIR_ROOT.$filepath, 'w');
				// Replace HTML entities in JSON content
				fwrite($fp, StringUtils::replaceHTMLEntities($jsonContent));
				fclose($fp);
				$filesize = filesize(DIR_ROOT.$filepath);
				$checksum = sha1_file(DIR_ROOT.$filepath);

				$this->io->info('Fichier JSON '.$filepath.' créé');
				$distribution = new DCATDistribution(
					ArrayUtils::get(Datatype::getProperty($dataset->getType()->getValue()), 'distribution'), 
					DCATDistribution::JSON,
					$filesize,
					$checksum,
					DOMAIN.$filepath
				);
				return $distribution->getArray();
			}
		} catch (\Exception $e) {
			$statusMessage .= 'Une erreur est survenue lors de l\'écriture du fichier JSON de type : '.key($content).'<br /><i>'.$e->getMessage().'</i>';
		}
	}

	/**
	 * Write data in a CSV file.
	 *
	 * @param int $siren The organization's siren
	 * @param ArrayObject $content Data contents
	 * @param string $type Data content type
	 * @param string $statusMessage Publication execution status message
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return array
	 * @access private
	 */
	private function saveDataInCSVFile(int $siren, \ArrayObject $content, string $type, string &$statusMessage) : array {
		try {
			// Create organization directory if doesn't exist
			$directory = $this->checkOrganizationDirectory($siren);
			if ($content != null && is_object($content) && sizeof($content) > 0) {
				$filepath = $directory.DIRECTORY_SEPARATOR.$type.'.csv';
				$fp = fopen(DIR_ROOT.$filepath, 'w');
				fputcsv($fp, ArrayUtils::get(Datatype::getProperty($type), 'columns'));
				foreach ($content as $line) {
					fputcsv($fp, $line->toArray());
				}
				fclose($fp);
				$filesize = filesize(DIR_ROOT.$filepath);
				$checksum = sha1_file(DIR_ROOT.$filepath);

				$this->io->info('Fichier CSV '.$siren.DIRECTORY_SEPARATOR.$type.'.csv créé');
				$distribution = new DCATDistribution(
					ArrayUtils::get(Datatype::getProperty($type), 'distribution'),
					DCATDistribution::CSV, 
					$filesize, 
					$checksum, 
					DOMAIN.$filepath
				);
				return $distribution->getArray();
			}
		} catch (\Exception $e) {
			$statusMessage .= 'Une erreur est survenue lors de l\'écriture du fichier CSV de type : '.$type.'<br /><i>'.$e->getMessage().'</i>';
		}
	}

	/**
	 * Check if organization directory exists, if not create it
	 *
	 * @param int $siren The organization's siren
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return string
	 * @throws Exception if directory cannot be created
	 * @access private
	 */
	private function checkOrganizationDirectory(int $siren) : string {
		$directory = $this->dataDirectory.$siren;
		if (!is_dir(DIR_ROOT.$directory) && !mkdir(DIR_ROOT.$directory)) {
			throw new ApiOpenDataException('Le répertoire des données ne peut être créé');
		}
		return $directory;
	}

	/**
     * Get micro time date to unify string.
	 * 
	 * @param DateTime $date (optional) Dataset creation date
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return string
	 * @access private
     */
    private function getMicroTime(?DateTime $date = null) : string {
		if ($date == null) {
			$t = microtime(true);
			$micro = sprintf('%6d', ($t - floor($t)) * 1000000);
			$date = new DateTime(date('Y-m-d H:i:s.'.trim($micro), $t));

			return $date->format('YmdHisu');
		} else {
			return $date->format('YmdHisv');
		}
    }
}