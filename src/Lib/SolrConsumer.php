<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Lib;

use DateTime;
use SolrQuery;
use SolrDisMaxQuery;
use SolrClient;
use SolrUtils;
use SolrClientException;
use ApiOpenData\Entity\Exception\ApiOpenDataException;

/**
 * SolrConsumer class to interact with Solr server.
 *
 * @package ApiOpenData\Lib
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class SolrConsumer {

	public function __construct() {
		// Default constructor
	}

	/**
	 * Count documents in Solr server.
	 *
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return SolrQueryResponse->SolrObject
	 * @access public 
	 */
	public function countDocuments() {
		try {
			$query = new SolrQuery('*:*');
			$response = $this->getSolrClient(SOLR_CORE_DOCUMENT)->query($query)->getResponse();

			return $response->response->numFound;
 
		} catch (\Exception $e) {
			if (strpos($e->getMessage(), '403')) {
				throw new ApiOpenDataException('Vous n\'êtes pas authorisé à accèder à cette ressource', 403);
			} else {
				throw new ApiOpenDataException($e->getMessage());
			}
		}
	}

	/**
	 * Search "collectivite" from documents in Solr server.
	 *
	 * @param integer $start Search start pointer
	 * @param integer $rows Search number of results
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return SolrQueryResponse->SolrObject
	 * @access public 
	 */
	public function getSirens() {
		try {
			$query = new SolrQuery();
			$query->setStart(0);
			$query->setRows(0);
			$query->setFacet(true);
			$query->addFacetField('siren');
 
			$response = $this->getSolrClient(SOLR_CORE_DOCUMENT)->query($query)->getResponse();
			if ($response->facet_counts != null 
					&& $response->facet_counts->facet_fields != null
					&& $response->facet_counts->facet_fields->siren != null) {
				$sirenArray = array();
				foreach ($response->facet_counts->facet_fields->siren as $siren) {
					if (is_string($siren)) {
						array_push($sirenArray, $siren);
					}
				}
				return $sirenArray;
			} else {
				return null;
			}
 
		} catch (\Exception $e) {
			if (strpos($e->getMessage(), '403')) {
				throw new ApiOpenDataException('Vous n\'êtes pas authorisé à accèder à cette ressource', 403);
			} else {
				throw new ApiOpenDataException($e->getMessage());
			}
		}
	}

	/**
	 * Search "délibérations" documents in Solr server from search criteria.
	 *
	 * @param integer $start Search start pointer
	 * @param integer $rows Search number of results
	 * @param array $criteria (optional) Search criterias 'field' => 'value'
	 * @param DateTime $startDate (optional) Start date for filtering
	 * @param DateTime $endDate (optional) End date for filtering
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return SolrQueryResponse->SolrObject
	 * @access public 
	 */
	public function getDeliberations(int $start, int $rows, $criterias = null, $startDate = null, $endDate = null) {
		return $this->getDocuments('DÃ©libÃ©rations', $start, $rows, $criterias, $startDate, $endDate);
	}

	/**
	 * Search équipements in Solr server from search criteria.
	 *
	 * @param integer $start Search start pointer
	 * @param integer $rows Search number of results
	 * @param array $criteria (optional) Search criterias 'field' => 'value'
	 * @author Alexis ZUCHER <a.zucher@girondenumerique.fr>
	 * @return SolrQueryResponse->SolrObject
	 * @access public 
	 */
	public function getEquipements(int $start, int $rows, $criterias = null) {
		return $this->getRequestEquipements($start, $rows, $criterias);
	}

	/**
	 * Search "actes réglementaires" documents in Solr server from search criteria.
	 *
	 * @param integer $start Search start pointer
	 * @param integer $rows Search number of results
	 * @param array $criteria (optional) Search criterias 'field' => 'value'
	 * @param array $startDate (optional) Start date for filtering
	 * @param array $endDate (optional) End date for filtering
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return SolrQueryResponse->SolrObject
	 * @access public
	 */
	public function getActesReglementaires(int $start, int $rows, $criterias = null, $startDate = null, $endDate = null) {
		return $this->getDocuments('Actes rÃ©glementaires', $start, $rows, $criterias, $startDate, $endDate);
	}

	/**
	 * Search "documents bugétaires et financiers" documents in Solr server from search criteria.
	 *
	 * @param integer $start Search start pointer
	 * @param integer $rows Search number of results
	 * @param array $criteria (optional) Search criterias 'field' => 'value'
	 * @param array $startDate (optional) Start date for filtering
	 * @param array $endDate (optional) End date for filtering
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return SolrQueryResponse->SolrObject
	 * @access public
	 */
	public function getBudgets(int $start, int $rows, $criterias = null, $startDate = null, $endDate = null) {
		return $this->getDocuments('Documents budgÃ©taires et financiers', $start, $rows, $criterias, $startDate, $endDate);
	}
	
	/**
	 * Test a connection to the solr server.
	 *
	 * @throws \Exception If connection test fail
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @param string $core Core of Solr
	 * @return boolean
	 * @access public
	 */
	public function testConnection($core) {
		try {
			if ($this->getSolrClient($core)->ping()) {
				return true;
			}
		} catch (SolrClientException $e) {
			if (strpos($e->getMessage(), 'Error 6')) {
				throw new ApiOpenDataException('Impossible de résoudre le nom d\'hôte', 400);
			} else if (strpos($e->getMessage(), 'Error 3')) {
				throw new ApiOpenDataException('Mauvais format d\'URL ou URL manquante', 400);
			} else if (strpos($e->getMessage(), 'Code 404')) {
				throw new ApiOpenDataException('Noyau de documents introuvable', 404);
			} else if (strpos($e->getMessage(), '401')) {
                throw new ApiOpenDataException('Echec de l\'authentification Solr', 401);
            } else if (strpos($e->getMessage(), '403')) {
                throw new ApiOpenDataException('Vous n\'êtes pas authorisé à accèder au serveur Solr', 403);
            } else {
				throw new ApiOpenDataException('Erreur : '.$e->getMessage());
			}
		}
	}

	/**
	 * Get solr client
	 *
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @param string $core Core of Solr
	 * @return SolrClient
	 * @access protected
	 */
	protected function getSolrClient(string $core) {
		$config = array (
			'hostname' 	=> SOLR_SERVER,
			'login'    	=> SOLR_USER,
			'password' 	=> SOLR_PASSWORD,
			'port'     	=> SOLR_PORT,
			'timeout'  	=> 10,
			'path' 		=> '/solr/'.$core
		);

		$client = new SolrClient($config);
		$client->setResponseWriter('json');

		return $client;
	}

	/**
	* Search documents in Solr server from search criterias.
	*
	* @param string $documentType Solr document type
	* @param integer $start Search start pointer
	* @param integer $rows Search number of results
	* @param array $criterias (optional) Search criterias 'field' => 'value'
	* @param array $startDate (optional) Start date for filtering
	* @param array $endDate (optional) End date for filtering
	* @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	* @return SolrQueryResponse->SolrObject
	* @throws \Exception if solr query fails
	* @access private
	*/
    private function getDocuments(string $documentType, int $start, int $rows, $criterias = null, $startDate = null, $endDate = null) {
	   try {
			$query = new SolrDisMaxQuery('*:*');
			$query->setMinimumMatch('100%');
			$query->setStart($start);
			$query->setRows($rows);
			$query->addFilterQuery('documenttype:'.SolrUtils::queryPhrase($documentType));
			$query->addSortField('date', 1);

		    // Criteria by filter field
		    if ($criterias != null && sizeof($criterias) > 0) {
			    foreach ($criterias as $field => $value) {
				    $query->addFilterQuery($field.':'.$value);
			    }
		    }

		    // Date field
		    if ($startDate != null || $endDate != null) {
			   $query->addFilterQuery('date:'.$this->setDateFilter($startDate, $endDate));
		    }
		   
		    $this->setFieldsForActsResponse($query);
   
		    return $this->getSolrClient(SOLR_CORE_DOCUMENT)->query($query)->getResponse();

	    } catch (\Exception $e) {
		   	if (strpos($e->getMessage(), '403')) {
				throw new ApiOpenDataException('Vous n\'êtes pas authorisé à accèder à cette ressource', 403);
		   	} else {
				throw new ApiOpenDataException($e->getMessage());
		   	}
	    }
    }

	/**
	 * Construct a date range filter for query.
	 *
	 * @param DateTime $startDate Start date from search form
	 * @param DateTime $endDate End date from search form
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return string
	 * @access private
	 */
    private function setDateFilter(?DateTime $startDate, ?DateTime $endDate) {
        $filter = '[';
        if ($startDate != null && $startDate instanceof \Datetime) {
			$filter .= $startDate->format('Y-m-d').'T00:00:00Z';
        } else {
            $filter .= '*';
        }
        $filter .= ' TO ';
        if ($endDate != null && $endDate instanceof \Datetime) {
			$filter .= $endDate->format('Y-m-d').'T00:00:00Z';
        } else {
            $filter .= '*';
        }
		$filter .= ']';

        return $filter;
	}
	
	/**
	 * Set acts fields for Solr response.
	 *
     * @param SolrQuery $query Query to be configured
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @access protected
	 */
    protected function setFieldsForActsResponse(SolrDisMaxQuery &$query) {
		$query->addField('documentidentifier');     // Pastell document identifier
		$query->addField('date');                   // Document date
		$query->addField('description');            // Document object
		$query->addField('classification');         // Classification
		$query->addField('filepath');               // File OpenData URL
		$query->addField('entity');                 // Entity name
		$query->addField('siren');                  // Entity siren
		$query->addField('nic');                  	// Entity nic
	}

	/**
	* Search collectifs equipments in Solr server from search criterias.
	*
	* @param integer $start Search start pointer
	* @param integer $rows Search number of results
	* @param array $criterias (optional) Search criterias 'field' => 'value'
	* @author Alexis ZUCHER <a.zucher@girondenumerique.fr>
	* @return SolrQueryResponse->SolrObject
	* @throws \Exception if solr query fails
	* @access private
	*/
    private function getRequestEquipements(int $start, int $rows, $criterias = null) {
		try {
			$query = new SolrQuery('*:*');
			$query->setStart($start);
			$query->setRows($rows);
			$query->addSortField('siren', 1);

			// Criteria by filter field
			if ($criterias != null && sizeof($criterias) > 0) {
				foreach ($criterias as $field => $value) {
					$query->addFilterQuery($field.':'.$value);
				}
			}
			$this->setFieldsForEquipementsResponse($query);

			return $this->getSolrClient(SOLR_CORE_EQUIPEMENT)->query($query)->getResponse();

		} catch (\Exception $e) {
			if (strpos($e->getMessage(), '403')) {
				throw new ApiOpenDataException('Vous n\'êtes pas authorisé à accèder à cette ressource', 403);
			} else {
				throw new ApiOpenDataException($e->getMessage());
			}
		}
	}

	/**
	 * Set equipments fields for Solr response.
	 *
     * @param SolrQuery $query Query to be configured
	 * @author Alexis ZUCHER <a.zucher@girondenumerique.fr>
	 * @access protected
	 */
    protected function setFieldsForEquipementsResponse(SolrQuery &$query) {
		$query->addField('equip_uid');     	// Equipment identifier
		$query->addField('equip_nom');     	// Equipment name
		$query->addField('siren');         	// Siren
		$query->addField('equip_type');    	// Equipment type
		$query->addField('equip_theme');   	// Equipment theme
		$query->addField('adr_numero');    	// Equipment address number
		$query->addField('adr_nomvoie');   	// Equipment address name
		$query->addField('adr_cp');        	// Equipment postal code
		$query->addField('adr_ville');		// Equipment city
		$query->addField('lat');			// Equipment latitude
		$query->addField('long');			// Equipment longitude
		$query->addField('tel');			// Equipment phone of the author
		$query->addField('email');			// Equipment mail of the author
		$query->addField('web');			// Equipment url website of the author
	}

}