<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Lib;

use PDO;
use DateTime;
use ApiOpenData\Entity\Consumer;
use ApiOpenData\Entity\Credentials;
use ApiOpenData\Entity\Dataset;
use ApiOpenData\Entity\Publication;
use ApiOpenData\Entity\State;
use ApiOpenData\Entity\Enum\Datatype;
use ApiOpenData\Entity\Enum\Status;
use ApiOpenData\Entity\Token;
use ApiOpenData\Entity\Enum\Rank;
use ApiOpenData\Lib\DAOConnection;
use ApiOpenData\Utils\StringUtils;

/**
 * OpenData GN DAO Class to get, insert, update consumer transactions/informations in MySQL Database
 *
 * @package ApiOpenData\Lib
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class OpenDataDAOConsumer extends DAOConnection {

	/**
	* OpenDataDAOConsumer constructor extending DAOConnection
	* @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	* @access public
	*/
	public function __construct() {
		parent::__construct(DB_SERVER, DB_USER, DB_PASSWORD, DB_BASE);
	}

	/**
	 * Add consumer in database and return his id.
	 *
	 * @param Consumer $consumer Consumer object
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return int
	 * @access public
	 */
	public function addConsumer(Consumer $consumer) {
		$query = 'INSERT INTO consumer (firstname, lastname, email, passwordHash, salt) 
		VALUES (:firstname, :lastname, :email, :passwordHash, :salt)';
		$params = array(
			'firstname' 	=> $consumer->getFirstname(), 
			'lastname' 		=> $consumer->getLastname(),
			'email' 		=> $consumer->getEmail(), 
			'passwordHash' 	=> $consumer->getCredentials()->getHashedPassword(),
			'salt' 			=> $consumer->getCredentials()->getSalt()
		);

		return $this->insert($query, $params);
	}

	/**
	 * Get consumer from his email after an OpenLDAP information request.
	 *
	 * @param string $email Consumer email
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return Consumer|null
	 * @access public
	 */
	public function getConsumerByEmail(string $email) {
		$query = 'SELECT c.id, c.firstname, c.lastname, c.email, c.passwordHash, c.salt, c.userKey, c.userSecret, c.admin, b.token, b.tokenKey
		FROM consumer c
		LEFT JOIN bearer b ON b.consumerId = c.id
		WHERE email = :email';
		$params = array('email' => $email);

		$data = $this->query($query, $params);
		if (!empty($data)) {
			$consumer = new Consumer($data[0]['id'], $data[0]['firstname'], $data[0]['lastname'], $data[0]['email']);
			if (!empty($data[0]['userKey'])) {
				$consumer->setKey($data[0]['userKey']);
			}
			if (!empty($data[0]['userSecret'])) {
				$consumer->setSecret($data[0]['userSecret']);
			}
			if ($data[0]['admin'] === false) {
				$consumer->setRank(Rank::USER());
			} else {
				$consumer->setRank(Rank::ADMIN());
			}
			$consumer->setCredentials(new Credentials($data[0]['id'], $data[0]['passwordHash'], $data[0]['salt']));
			if (!empty($data[0]['token'])) {
				$consumer->setToken(new Token($data[0]['id'], $data[0]['token'], $data[0]['tokenKey']));
			}
			return $consumer;
		} else {
			return null;
		}
	}

	/**
	 * Get consumer from his key and secret string. 
	 * Select first in MySQL database then if query returns one result, get consumer informations from OpenLDAP.
	 *
	 * @param string $key Consumer key string
	 * @param string $secret Consumer secret string
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return Consumer|null
	 * @access public
	 */
	public function getConsumerByKeyAndSecret(string $key, string $secret) {
		$query = 'SELECT c.id, c.firstname, c.lastname, c.email, c.userKey, c.userSecret, b.token, b.tokenKey
		FROM consumer c
		LEFT JOIN bearer b ON b.consumerId = c.id
		WHERE userKey = :consumerKey
		AND userSecret = :consumerSecret';
		$params = array('consumerKey' => $key, 'consumerSecret' => $secret);

		$data = $this->query($query, $params);
		if (!empty($data)) {
			$consumer = new Consumer($data[0]['id'], $data[0]['firstname'], $data[0]['lastname'], $data[0]['email']);
			$consumer->setKey($data[0]['userKey']);
			$consumer->setSecret($data[0]['userSecret']);
			if (!empty($data[0]['token'])) {
				$consumer->setToken(new Token($data[0]['id'], $data[0]['token'], $data[0]['tokenKey']));
			}
			return $consumer;
		} else {
			return null;
		}
	}

	/**
	 * Get consumer bearer for a specific consumer Id.
	 *
	 * @param int $consumerId Consumer identifier
	 * @param string $secret Consumer secret string
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return Token|null
	 * @access public
	 */
	public function getBearerByConsumerId(int $consumerId) {
		$query = 'SELECT token, tokenKey, consumerId FROM bearer WHERE consumerId = :consumerId';
		$params = array('consumerId' => $consumerId);

		$data = $this->query($query, $params);
		if (!empty($data)) {
			return new Token($data[0]['consumerId'], $data[0]['token'], $data[0]['tokenKey']);
		} else {
			return null;
		}
	}

	/**
	 * Get consumer bearer with its key for API security validation.
	 *
	 * @param string $bearer Consumer token
	 * @param string $secret Consumer secret string
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return Token|null
	 * @access public
	 */
	public function getBearer(string $bearer) {
		$query = 'SELECT token, tokenKey, consumerId FROM bearer WHERE token = :token';
		$params = array('token' => $bearer);

		$data = $this->query($query, $params);
		if (!empty($data)) {
			return new Token($data[0]['consumerId'], $data[0]['token'], $data[0]['tokenKey']);
		} else {
			return null;
		}
	}

	/**
	 * Update consumer key and secret.
	 *
	 * @param int $consumerId Consumer id
	 * @param string $key Consumer new key string
	 * @param string $secret Consumer new secret string
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return boolean
	 * @access public
	 */
	public function updateConsumerKeys(int $consumerId, string $key, string $secret) {
		$query = 'UPDATE consumer SET userKey = :userKey, userSecret = :userSecret
		WHERE id = :consumerId';
		$params = array(
			'consumerId' 	=> $consumerId,
			'userKey' 		=> $key, 
			'userSecret' 	=> $secret
		);

		return $this->update($query, $params);
	}

	/**
	 * Update consumer information.
	 *
	 * @param int $consumerId Consumer id
	 * @param string $firstname Consumer firstname
	 * @param string $lastname Consumer lastname
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return boolean
	 * @access public
	 */
	public function updateConsumerInformation(int $consumerId, string $firstname, string $lastname) {
		$query = 'UPDATE consumer SET firstname = :firstname, lastname = :lastname
		WHERE id = :consumerId';
		$params = array('consumerId' => $consumerId, 'firstname' => $firstname, 'lastname' => $lastname);

		return $this->update($query, $params);
	}

	/**
	 * Reinitialize consumer password and salt
	 *
	 * @param string $email Consumer email
	 * @param string $passwordHash Consumer new password
	 * @param string $salt Consumer new salt
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return boolean
	 * @access public
	 */
	public function reinitializePassword(string $email, string $passwordHash, string $salt) {
		$query = 'UPDATE consumer SET passwordHash = :passwordHash, salt = :salt
		WHERE email = :email';
		$params = array('email' => $email, 'passwordHash' => $passwordHash, 'salt' => $salt);

		return $this->update($query, $params);
	}

	/**
	 * Update consumer password.
	 *
	 * @param int $consumerId Consumer id
	 * @param string $passwordHash Consumer new password
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return boolean
	 * @access public
	 */
	public function updateConsumerPassword(int $consumerId, string $passwordHash) {
		$query = 'UPDATE consumer SET passwordHash = :passwordHash
		WHERE id = :consumerId';
		$params = array('consumerId' => $consumerId, 'passwordHash' => $passwordHash);

		return $this->update($query, $params);
	}

	/**
	 * Update consumer bearer with its key for API security validation.
	 *
	 * @param int $userId Consumer id
	 * @param string $bearer JWT token string
	 * @param string $key JWT token key
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return boolean
	 * @access public
	 */
	public function updateBearer(int $userId, string $bearer, string $key) {
		$query = 'REPLACE INTO bearer SET consumerId = :consumerId, token = :token, tokenKey = :tokenKey';
		$params = array('consumerId' => $userId, 'token' => $bearer, 'tokenKey' => $key);

		return $this->update($query, $params);
	}

	/**
	 * Insert activity for each API consumption.
	 *
	 * @param int $consumerId Consumer id
	 * @param string $uri API function get from request URI
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return boolean
	 * @access public
	 */
	public function registerActivity(int $consumerId, string $uri) {
		$query = 'INSERT INTO activity (uri, consumerId) VALUES (:uri, :consumerId)';
		$params = array('consumerId' => $consumerId, 'uri' => $uri);

		return $this->update($query, $params);
	}

	/**
	 * Count activities for a specific consumer in an interval in seconds.
	 *
	 * @param int $consumerId Consumer id
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return int
	 * @access public
	 */
	public function countActivitiesForInterval(int $consumerId) {
		$query = 'SELECT COUNT(*) FROM activity WHERE consumerId = :consumerId AND executionDate > :intervalDate';
		date_default_timezone_set('Europe/Paris');
		$intervalTimestamp = time() - INTERVAL;
		$intervalDate = new \DateTime('now');
		$intervalDate->setTimestamp($intervalTimestamp);
		$params = array('consumerId' => $consumerId, 'intervalDate' => $intervalDate->format('Y-m-d H:i:s'));

		return $this->count($query, $params);
	}

	/**
	 * Get publication by SIREN.
	 *
	 * @param int $consumerId Consumer id
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return Publication|null
	 * @access public
	 */
	public function getPublication(int $siren) {
		$query = 'SELECT * FROM publication WHERE siren = :siren';
		$params = array('siren' => $siren);

		$data = $this->query($query, $params);
		if (!empty($data)) {
			$publication = new Publication( $data[0]['siren'], 
											stripslashes($data[0]['name']),
											$data[0]['frequency'],
											$data[0]['active'] == 1 ? true : false);
			if (!empty($data[0]['url'])) {
				$publication->setUrl(stripslashes($data[0]['url']));
			}
			if (!empty($data[0]['state'])) {
				$publication->setState(new State(new Status(intval($data[0]['state'])), stripslashes($data[0]['message'])));
			}
			if (!empty($data[0]['execution'])) {
				$publication->setLastExecution(new DateTime($data[0]['execution']));
			}
			$publication->setDatasets($this->getDatasetsByPublication($siren));
			return $publication;
		} else {
			return null;
		}
	}

	/**
	 * Get all publications.
	 *
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return array|null
	 * @access public
	 */
	public function getPublications() {
		$query = 'SELECT * FROM publication';

		$array = null;
		$data = $this->query($query, null, PDO::FETCH_ASSOC);
		if ($data != null && sizeof($data)) {
			$array = array();
			foreach ($data as $publicationLine) {
				$publication = new Publication( $publicationLine['siren'], 
												stripslashes($publicationLine['name']),
												$publicationLine['frequency'],
												$publicationLine['active'] == 1 ? true : false);
				if (!empty($publicationLine['url'])) {
					$publication->setUrl(stripslashes($publicationLine['url']));
				}
				if (!empty($publicationLine['state'])) {
					$publication->setState(new State(intval($publicationLine['state']), stripslashes($publicationLine['message'])));
				}
				if (!empty($publicationLine['execution'])) {
					$publication->setLastExecution(new DateTime($publicationLine['execution']));
				}
				$publication->setDatasets($this->getDatasetsByPublication($publicationLine['siren']));
				array_push($array, $publication);
			}		
		}

		return $array;
	}

	/**
	 * Get all publications.
	 *
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return array|null
	 * @access public
	 */
	public function getDatasetsByPublication(int $siren) {
		$query = 'SELECT * FROM dataset WHERE siren = :siren';
		$params = array('siren' => $siren);

		$array = null;
		$data = $this->query($query, $params, PDO::FETCH_ASSOC);
		if ($data != null && sizeof($data)) {
			$array = array();
			foreach ($data as $datasetLine) {
				$dataset = new Dataset( $datasetLine['siren'], 
										new Datatype($datasetLine['type']),
										$datasetLine['numberOfLine']);
				if (!empty($datasetLine['creationDate'])) {
					$dataset->setCreationDate(new DateTime($datasetLine['creationDate']));
				}
				if (!empty($datasetLine['updateDate'])) {
					$dataset->setUpdateDate(new DateTime($datasetLine['updateDate']));
				}
				$array[$datasetLine['type']] = $dataset;
			}		
		}

		return $array;
	}

	/**
	 * Add a new publication in database.
	 *
	 * @param Publication $publication Publication object
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return boolean
	 * @access public
	 */
	public function addPublication(Publication $publication) {
		$query = 'INSERT INTO publication (siren, name, frequency, active) VALUES (:siren, :name, :frequency, :active)';
		$params = array('siren' 	=> $publication->getSiren(), 
						'name' 		=> $publication->getName(), 
						'frequency' => $publication->getFrequency(),
						'active' 	=> $publication->getActive() ? 1 : 0);

		$result = $this->insert($query, $params);
		return $result == 0;
	}

	/**
	 * Update publication information.
	 *
	 * @param Publication $publication Publication object
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return boolean
	 * @access public
	 */
	public function updatePublication(Publication $publication) {
		$query = 'UPDATE publication SET name = :name, frequency = :frequency, active = :active WHERE siren = :siren';
		$params = array('siren' 	=> $publication->getSiren(), 
						'name' 		=> $publication->getName(), 
						'frequency' => $publication->getFrequency(),
						'active' 	=> $publication->getActive() ? 1 : 0);

		return $this->update($query, $params);
	}

	/**
	 * Save status in a specific publication.
	 *
	 * @param Publication $publication Publication object
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return boolean
	 * @access public
	 */
	public function saveStatusInPublication(Publication $publication) {
		$query = 'UPDATE publication SET url = :url, state = :state, message = :message, execution = now() WHERE siren = :siren';
		$params = array('url'		=> $publication->getUrl(),
						'state' 	=> $publication->getState()->getCode(),
						'message'	=> $publication->getState()->getMessage(),
						'siren' 	=> $publication->getSiren());

		return $this->update($query, $params);
	}

	/**
	 * Save publication information.
	 *
	 * @param Publication $publication Publication object
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return boolean
	 * @access public
	 */
	public function deletePublication(string $siren) {
		$query = 'DELETE FROM publication WHERE siren = :siren';
		$params = array('siren' => $siren);

		return $this->update($query, $params);
	}

	/**
	 * Save dataset information.
	 *
	 * @param Dataset $dataset Dataset object
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return boolean
	 * @access public
	 */
	public function saveDataset(Dataset $dataset) {
		$query = 'REPLACE INTO dataset SET siren = :siren, type = :type, creationDate = :creationDate, updateDate = :updateDate, numberOfLine = :numberOfLine';
		$params = array('siren' 		=> $dataset->getSiren(), 
						'type' 			=> $dataset->getType()->getValue(),
						'creationDate' 	=> $dataset->getCreationDate() != null ? StringUtils::formatDateTime($dataset->getCreationDate()) : null,
						'updateDate' 	=> $dataset->getUpdateDate() != null ? StringUtils::formatDateTime($dataset->getUpdateDate()) : null,
						'numberOfLine' 	=> $dataset->getNumberOfLine());

		return $this->update($query, $params);
	}
}