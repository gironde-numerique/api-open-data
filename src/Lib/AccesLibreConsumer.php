<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Lib;

use ApiOpenData\Entity\Equipement;
use ApiOpenData\Entity\Adresse;
use ApiOpenData\Entity\Exception\ApiOpenDataException;

/**
 * AccesLibreConsumer class to consume Acces Libre ERP API.
 *
 * @package ApiOpenData\Lib
 * @author  Alexis ZUCHER <a.zucher@girondenumerique.fr>
 */
class AccesLibreConsumer {

    // URL of Acces Libre Organization Public API
    private $erpsApi = 'https://acceslibre.beta.gouv.fr/api/erps/';

    public function __construct() {
        // Default constructor
    }

    /**
     * Get equipments informations for a specific zipcode. 
     *
     * @param string $zipCode Organization zipCode
     * @author Alexis ZUCHER <a.zucher@girondenumerique.fr>
     * @return array
     * @throws \Exception If curl call generate an error
     * @access public 
     */
    public function getEquipmentInformations($zipCode) {
        try {
            $params = 'code_postal='.$zipCode;
            $response = $this->apiCurlCalling($this->erpsApi, $params);

            $equipmentsArray = array();
            foreach ($response->results as $equipmentResult) {
                array_push($equipmentsArray, $this->extractEquipmentInformations($equipmentResult));
            }
            return $equipmentsArray;

        } catch (\Exception $e) {
            throw new ApiOpenDataException('Impossible de récupérer les informations des équipements pour le Code Postal '.$zipCode);
        }
    }

    /**
     * Execute a cURL call and return JSON response as object.
     *
     * @param string $url Acces Libre API service URL
     * @param string $params URL formed query params
     * @author Alexis ZUCHER <a.zucher@girondenumerique.fr>
     * @return object
     * @throws \Exception If curl call generate an error
     * @access private
     */
    private function apiCurlCalling($url, $params = null) {
        $errmsg = null;
        $ch = curl_init();
        $options = array(
            CURLOPT_URL             => $url.'?'.$params,
            CURLOPT_HEADER          => 0,
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
            CURLOPT_RETURNTRANSFER  => true
        );

        curl_setopt_array($ch, $options);
        $response = json_decode(curl_exec($ch));
        if (curl_errno($ch) != 0) {
            $errmsg = curl_error($ch);
        }
        curl_close($ch);

        if ($errmsg != null) {
            throw new ApiOpenDataException($errmsg);
        } else if ($response == null) {
            throw new ApiOpenDataException('L\'appel du service ERPS n\'a généré aucune réponse', 204);
        }

        return $response;
    }

    /**
     * Extract equipment useful informations from JSON response.
     *
     * @param stdClass $equipmentExtract Equipement from JSON response
     * @author Alexis ZUCHER <a.zucher@girondenumerique.fr>
     * @return Equipement
     * @access private
     */
    private function extractEquipmentInformations(\stdClass $equipmentToExtract) {
        $address = new Adresse();
        if (!empty($equipmentToExtract->adresse)) {
            // Take Address in Array
            $addressArray = explode(' ', $equipmentToExtract->adresse);
            if (intval($addressArray[sizeof($addressArray) - 2])) {
                $address->setCode_postal($addressArray[sizeof($addressArray) - 2]);
            }
            // Take only the address name
            $addressNameArray = array_slice($addressArray, 0, sizeof($addressArray) - 2);

            $laneName = '';
            // For each word which is address name
            foreach ($addressNameArray as $addressName) {
                $laneName .= $addressName.' ';
            }
            $address->setLibelle_voie(trim($laneName));
        }
        if (!empty($equipmentToExtract->commune)) {
            $address->setCommune($equipmentToExtract->commune);
        }

        $equipment = new Equipement();
        if (!empty($equipmentToExtract->commune)) {
            $equipment->setNom($equipmentToExtract->nom);
        }
        if (!empty($address)) {
            $equipment->setAdresse($address);
        }
        if (!empty($equipmentToExtract->telephone)) {
            $equipment->setTel($equipmentToExtract->telephone);
        }
        if (!empty($equipmentToExtract->contact_email)) {
            $equipment->setEmail($equipmentToExtract->contact_email);
        }
        if (!empty($equipmentToExtract->web_url)) {
            $equipment->setWeb($equipmentToExtract->web_url);
        }
        if (!empty($equipmentToExtract->activite->nom)) {
            $equipment->setType($equipmentToExtract->activite->nom);
        }
        if (!empty($equipmentToExtract->geom->coordinates[1])) {
            $equipment->setLatitude($equipmentToExtract->geom->coordinates[1]);
        }
        if (!empty($equipmentToExtract->geom->coordinates[0])) {
            $equipment->setLongitude($equipmentToExtract->geom->coordinates[0]);
        }
        
        return $equipment;
    }
}