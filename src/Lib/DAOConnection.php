<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Lib;

use PDO;
use PDOException;
use ApiOpenData\Entity\Exception\ApiOpenDataException;

/**
 * Connection class to allow access to OpenData MySQL database.
 *
 * @package ApiOpenData\Lib
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class DAOConnection {
	
	/**
	* Constructor with connection parameters.
	* @param string $server Database server name
	* @param string $user User login
	* @param string $password User password
	* @param string $base Basename
	* @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	* @throws PDOException if connection returns failure
	* @access public
	*/
	public function __construct($server, $user, $password, $base) {
		try {
			$this->db = new PDO('mysql:host='.$server.';port=8889;dbname='.$base, $user, $password);
		} catch (PDOException $e) {
			$this->throwException($e);
		}
	}

	/**
	* Execute select query with optional parameters in fetch assoc by default.
	* 
	* @param string $query SQL select query to perform
	* @param array $params (optional) Array of params with format key => value
	* @param const $fetchMode (optional) Fetch mode (assoc by default)
	* @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	* @return array
	* @throws PDOException if query fails
	* @access public
	*/
	public function query(string $query, $params = null, $fetchMode = PDO::FETCH_ASSOC) {
		$query = trim($query);
		try {
			$stmt = $this->db->prepare($query);
			if (!empty($params)) {
				foreach ($params as $key => $value) {
					$stmt->bindValue(':'.$key, $value);
				}
			}
			$stmt->execute();
			if (DEBUG) {
				$stmt->debugDumpParams();
			}
			$data = array();
			$index = 0;
			while ($row = $stmt->fetch($fetchMode)) {
				$data[$index] = $row;
				$index++;
			}
			$stmt = null;
		} catch (PDOException $e) {
			$this->throwException($e, $query);
		}

		return $data;
	}

	/**
	* Execute insert query with optional parameters and return inserted identifier.
	* 
	* @param string $query SQL insert query to perform
	* @param array $params (optional) Array of params with format key => value
	* @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	* @return int 
	* @throws PDOException if query fails
	* @access public
	*/
	public function insert(string $query, $params = null) {
		$query = trim($query);
		try {
			$this->db->beginTransaction();
			$stmt = $this->db->prepare($query);
			if (!empty($params)) {
				foreach ($params as $key => $value) {
					$stmt->bindValue(':'.$key, $value);
				}
			}
			$stmt->execute();
			if (DEBUG) {
				$stmt->debugDumpParams();
			}
			$id = $this->db->lastInsertId();
			$this->db->commit();

			return $id;
		} catch (PDOException $e) {
			$this->throwException($e, $query);
		}
	}

	/**
	* Execute update query with optional parameters.
	* 
	* @param string $query SQL update query to perform
	* @param array $params (optional) Array of params with format key => value
	* @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	* @return boolean 
	* @throws PDOException if query fails
	* @access public
	*/
	public function update(string $query, $params = null) {
		$query = trim($query);
		try {
			$this->db->beginTransaction();
			$stmt = $this->db->prepare($query);
			if (!empty($params)) {
				foreach ($params as $key => $value) {
					$stmt->bindValue(':'.$key, $value);
				}
			}
			$result = $stmt->execute();
			if (DEBUG) {
				$stmt->debugDumpParams();
			}
			$this->db->commit();

			return $result;
		} catch (PDOException $e) {
			$this->throwException($e, $query);
		}
	}

	/**
	* Execute select count query with optional parameters.
	* 
	* @param string $query SQL update query to perform
	* @param array $params (optional) Array of params with format key => value
	* @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	* @return int 
	* @throws PDOException if query fails
	* @access public
	*/
	public function count(string $query, $params = null) {
		$query = trim($query);
		try {
			$stmt = $this->db->prepare($query);
			if (!empty($params)) {
				foreach ($params as $key => $value) {
					$stmt->bindValue(':'.$key, $value);
				}
			}
			$stmt->execute();
			if (DEBUG) {
				$stmt->debugDumpParams();
			}
			return $stmt->fetchColumn();
		} catch (PDOException $e) {
			$this->throwException($e, $query);
		}
	}

	/**
	* Throw exception with formatted message.
	* 
	* @param PDOException $e PDOException from MySQL PDO transaction
	* @param string $query (optional) Query as string concerned by the PDOException
	* @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	* @return Exception
	* @access private
	*/
	private function throwException(PDOException $e, $query = '') {
		$message = 'Erreur lors de l\'accès à la base MySQL :<br /><i>'.$e->getMessage().'</i>';
		if (!empty($query)) {
			$message .= '<br />Requête concernée : '.$query;
		}

		throw new ApiOpenDataException($message);
	}
}