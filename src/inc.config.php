<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData;

/**
 * API properties file
 *
 * @package ApiOpenData
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */

// General properties
define('DIR_ROOT',      __DIR__.'/../');
define('ADMIN_NAME',    'Xavier MADIOT');
define('ADMIN_EMAIL',   'x.madiot@girondenumerique.fr');
define('DOMAIN',        'http://localhost');

// Environment properties
define('DEVELOPMENT',   true);
define('PRODUCTION',    false);
define('DEBUG',         true);
define('LOGGING',       true);

// Java
define('JAVA_HOME',     '/usr/bin/java');

// JWT properties
define('ISS',           'http://www.girondenumerique.fr');
define('AUD',           'https://api-opendata.girondenumerique.fr');

// Sirene V3 API properties
define('SIRENE_KEY',    '****************************');
define('SIRENE_SECRET', '****************************');

// Solr server properties
define('SOLR_SERVER',   'localhost');
define('SOLR_USER',     'solr');
define('SOLR_PASSWORD', 'root');
define('SOLR_PORT',     8983);
define('SOLR_CORE',     'core');
define('SOLR_ROWS',     100);

// Database properties
define('DB_SERVER',     'localhost');
define('DB_USER',       'root');
define('DB_PASSWORD',   '************');
define('DB_BASE',       'api-opendata');

// API limitation
define('MAX_CALLS',     30);        // Max for calls / interval
define('INTERVAL',      60);        // In seconds
define('LOCK_TIME',     30);        // In seconds