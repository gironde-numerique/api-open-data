<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Entity;

use DateTime;
use \Firebase\JWT\JWT;
use \Firebase\JWT\ExpiredException;

/**
 * Class Token to manipulate consumer bearer and theirs keys.
 *
 * @package ApiOpenData\Entity
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class Token {

	/**
	* Consumer identifier
	* @var int
	*/
	private $consumerId;

	/**
	* JWT bearer
	* @var string
	*/
	private $bearer;

	/**
	* Bearer hash key
	* @var string
	*/
	private $key;

    /**
	 * Fields construtor
	 * 
	 * @param int $consumerId Consumer identifier
	 * @param string $bearer Consumer JWT token
	 * @param string $key Bearer hash key
	 */
	function __construct(int $consumerId, string $bearer, string $key) {
		$this->consumerId 	= $consumerId;
		$this->bearer 		= $bearer;
		$this->key 			= $key;
		
	}

	/**
	 * Get the value of	consumerId
	 */ 
	public function getConsumerId() {
		return $this->consumerId;
	}

	/**
	 * Set the value of consumerId
	 *
	 * @param  int $consumerId
	 * @return  self
	 */ 
	public function setConsumerId(int $consumerId) {
		$this->consumerId = $consumerId;
		return $this;
	}

	/**
	 * Get the value of bearer
	 */ 
	public function getBearer() {
		return $this->bearer;
	}

	/**
	 * Set the value of bearer
	 *
	 * @param  string $bearer
	 * @return  self
	 */ 
	public function setBearer(string $bearer) {
		$this->bearer = $bearer;
		return $this;
	}

	/**
	 * Get the value of key
	 */ 
	public function getKey() {
		return $this->key;
	}

	/**
	 * Set the value of key
	 *
	 * @param  string $key
	 * @return  self
	 */ 
	public function setKey(string $key) {
		$this->key = $key;
		return $this;
	}

	/**
	 * Get the token creation date
	 */ 
	public function getCreationDate() {
		try {
			$decodedToken = JWT::decode($this->bearer, $this->key, array('HS256'));
			$creationDate = new DateTime();
			$creationDate->setTimestamp($decodedToken->iat);

			return $creationDate->format('d/m/Y à h:m:s');
		} catch (ExpiredException $e) {
			return null;
		}
	}

	/**
	 * Get the token expiration date
	 */ 
	public function getExpirationDate() {
		try {
			$decodedToken = JWT::decode($this->bearer, $this->key, array('HS256'));
			$expirationDate = new DateTime();
            $expirationDate->setTimestamp($decodedToken->exp);

			return $expirationDate->format('d/m/Y à h:m:s');
		} catch (ExpiredException $e) {
			return null;
		}
	}

	/**
	 * Get the token exception
	 */ 
	public function getJWTException() {
		try {
			JWT::decode($this->bearer, $this->key, array('HS256'));
			return false;
		} catch (ExpiredException $e) {
			return true;
		}
	}

	/**
	 * To string
	 */
	public function __toString() {
		return $this->consumerId.' - '.$this->bearer.' - '.$this->key;
	}
}