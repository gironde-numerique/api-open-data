<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Entity;

/**
 * Class Credentials to manipulate consumer password.
 *
 * @package ApiOpenData\Entity
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class Credentials {

	/**
	* Consumer identifier
	* @var int
	*/
	private $consumerId;

	/**
	* Hashed password
	* @var string
	*/
	private $hashedPassword;

	/**
	* Salt random string
	* @var string
	*/
	private $salt;

    /**
	 * Fields construtor
	 * 
	 * @param int $consumerId Consumer identifier
	 * @param string $hashedPassword Consumer hashed password
	 * @param string $key Consumer salt random string for password compilation
	 */
	function __construct(int $consumerId, string $hashedPassword, string $salt) {
		$this->consumerId 		= $consumerId;
		$this->hashedPassword 	= $hashedPassword;
		$this->salt 			= $salt;
	}

	/**
	 * Get the value of	consumerId
	 */ 
	public function getConsumerId() {
		return $this->consumerId;
	}

	/**
	 * Set the value of consumerId
	 *
	 * @param  int $consumerId
	 * @return  self
	 */ 
	public function setConsumerId(int $consumerId) {
		$this->consumerId = $consumerId;
		return $this;
	}

	/**
	 * Get the value of hashedPassword
	 */ 
	public function getHashedPassword() {
		return $this->hashedPassword;
	}

	/**
	 * Set the value of hashedPassword
	 *
	 * @param  string $hashedPassword
	 * @return  self
	 */ 
	public function setHashedPassword(string $hashedPassword) {
		$this->hashedPassword = $hashedPassword;
		return $this;
	}

	/**
	 * Get the value of salt
	 */ 
	public function getSalt() {
		return $this->salt;
	}

	/**
	 * Set the value of salt
	 *
	 * @param  string $salt
	 * @return  self
	 */ 
	public function setSalt(string $salt) {
		$this->salt = $salt;
		return $this;
	}

	/**
	 * To string
	 */
	public function __toString() {
		return $this->consumerId.' - '.$this->hashedPassword.' - '.$this->salt;
	}
}