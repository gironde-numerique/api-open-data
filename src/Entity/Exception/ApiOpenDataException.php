<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Entity\Exception;

use Exception;
use Throwable;

/**
 * Class ApiOpenData Exception to avoid throwing generic exception
 *
 * @package ApiOpenData\Entity\Exception
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class ApiOpenDataException extends Exception {

    public function __construct(string $message, ?int $code = 500, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }

    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}
