<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Entity;

use DateTime;
use JsonSerializable;
use ApiOpenData\Entity\State;

/**
 * Class Publication to manipulate DCAT catalog
 *
 * @package ApiOpenData\Entity
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class Publication implements JsonSerializable {

	/**
	* Publication organization siren
	* @var int
	*/
	private $siren;

	/**
	* Publication organization name
	* @var string
	*/
	private $name;

	/**
	* Publication frequency in days
	* @var int
	*/
	private $frequency;

	/**
	* Publication state
	* @var State
	*/
	private $state;

	/**
	* Publication active flag
	* @var boolean
	*/
	private $active;

	/**
	* Publication DCAT catalog
	* @var string
	*/
	private $url;

	/**
	* Publication last execution date
	* @var Datetime
	*/
	private $lastExecution;

	/**
	* Publication last execution formatted date
	* @var string
	*/
	private $formattedLastExecution;

	/**
	* Publication list of datasets
	* @var array
	*/
	private $datasets;

    /**
	 * Fields construtor
	 * 
	 * @param int $siren Publication organization siren
	 * @param string $name Publication organization name
	 * @param int $frequency Publication frequency in days
	 * @param bool $active Publication active flag
	 */
	function __construct(int $siren, string $name, int $frequency, bool $active) {
		$this->siren		= $siren;
		$this->name 		= $name;
		$this->frequency	= $frequency;
		$this->active 		= $active;
	}

	/**
	 * Get the publication organization siren value
	 */ 
	public function getSiren() {
		return $this->siren;
	}

	/**
	 * Set the publication organization siren value
	 *
	 * @param  int  $siren
	 * @return  self
	 */ 
	public function setSiren(int $siren) {
		$this->siren = $siren;
		return $this;
	}

	/**
	 * Get the publication organization name value
	 */ 
	public function getName() {
		return $this->name;
	}

	/**
	 * Set the publication organization name value
	 *
	 * @param  string  $name
	 * @return  self
	 */ 
	public function setName(string $name) {
		$this->name = $name;
		return $this;
	}

	/**
	 * Get the publication frequency in days value
	 */ 
	public function getFrequency() {
		return $this->frequency;
	}

	/**
	 * Set the publication frequency in days value
	 *
	 * @param  int  $frequency
	 * @return  self
	 */ 
	public function setFrequency(int $frequency) {
		$this->frequency = $frequency;
		return $this;
	}

	/**
	 * Get the publication state value
	 */ 
	public function getState() {
		return $this->state;
	}

	/**
	 * Set the publication state value
	 *
	 * @param  State  $state
	 * @return  self
	 */ 
	public function setState(?State $state) {
		$this->state = $state;
		return $this;
	}

	/**
	 * Get the publication active flag value
	 */ 
	public function getActive() {
		return $this->active;
	}

	/**
	 * Set publication active flag value
	 *
	 * @param  boolean  $active
	 * @return  self
	 */ 
	public function setActive(bool $active) {
		$this->active = $active;
		return $this;
	}

	/**
	 * Get the publication DCAT catalog URL
	 */ 
	public function getUrl() {
		return $this->url;
	}

	/**
	 * Set the publication DCAT URL value
	 *
	 * @param  string  $url
	 * @return  self
	 */ 
	public function setUrl(?string $url) {
		$this->url = $url;
		return $this;
	}

	/**
	 * Get the publication last execution date
	 */ 
	public function getLastExecution() {
		return $this->lastExecution;
	}

	/**
	 * Set the publication last execution date
	 *
	 * @param  Datetime  $lastExecution
	 * @return  self
	 */ 
	public function setLastExecution(Datetime $lastExecution) {
		$this->lastExecution = $lastExecution;
		if ($this->lastExecution != null) {
			$this->formattedLastExecution = $this->lastExecution->format('d/m/Y à H:m:s');
		}
		return $this;
	}

	/**
	 * Get the publication last execution formatted date
	 */ 
	public function getFormattedLastExecution() {
		if ($this->lastExecution != null) {
			$this->formattedLastExecution = $this->lastExecution->format('d/m/Y à H:m:s');
		}
		return $this->formattedLastExecution;
	}

	/**
	 * Get the publication list of datasets
	 */ 
	public function getDatasets() {
		return $this->datasets;
	}

	/**
	 * Set the publication list of datasets
	 *
	 * @param  array  $datasets
	 * @return  self
	 */ 
	public function setDatasets(?array $datasets) {
		$this->datasets = $datasets;
		return $this;
	}

	/**
	 * Transform the publication object to an array
	 *
	 * @return  array
	 */
	public function toArray() {
		return array(
			'siren' 	=> $this->siren,
			'name' 		=> $this->name,
			'frequency' => $this->frequency,
			'active' 	=> $this->active,
			'state'  	=> $this->state != null ? $this->state->getCode() : null,
			'url' 		=> $this->url
		);
	}

	/**
	 * To string
	 */
	public function __toString() {
		return $this->id.' - '.$this->firstname.' - '.$this->lastname.' - '.$this->email;
	}

	/**
	* JSON serialization.
	*/
	public function jsonSerialize() {
		return get_object_vars($this);
	}
}