<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Entity;

use ApiOpenData\Entity\Adresse;
use ApiOpenData\Entity\ActeReglementaire;
use ApiOpenData\Entity\Budget;
use ApiOpenData\Entity\Deliberation;

/**
 * Class Collectivite
 *
 * @package ApiOpenData\Entity
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 *
 * @OA\Schema(
 *	description="Spécification du modèle de données relatif aux collectivités permettant de classer les jeux de données pour chacune d'entres-elles.",
 *	title="Modèle collectivité",
 *	required={"siren", "nic", "name", "address", "deliberations"},
 *	@OA\Xml(
 *		name="Collectivite"
 *	)
 * )
 */
class Collectivite implements \JsonSerializable {

	/**
	* @OA\Property(
	*	title="Numéro SIREN de la collectivité",
	*	description="Numéro SIREN (pour « système d'identification du répertoire des entreprises »), numéro unique d’identification de chaque entreprise. Ce numéro permet d’identifier chaque entreprise auprès des administrations.",
	*	property="siren",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $siren;

	/**
	* @OA\Property(
	*	title="Numéro NIC de la collectivité",
	*	description="Numéro NIC (numéro interne de classement propre à chaque établissement) de la collectivité sur 5 chiffres (préfixé par des 0 si la taille du numéro est inférieur à 5). Ce numéro par concaténation, permet de constituer le numéro SIRET (SIREN + NIC).",
	*	property="nic",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $nic;

	/**
	* @OA\Property(
	*	title="Nom de la collectivité",
	*	description="Nom de la collectivité, qui est constitué de la raison sociale le nom des sociétés civiles qui doit impérativement figurer dans les statuts de la collectivité.",
	*	property="name",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $name;

	/**
	* @OA\Property(
	*	title="Code APE",
	*	description="Code APE (pour « code d'activité principale », on parle aussi de code NAF pour « nomenclature d’activité française »), permet d'identifier la branche d'activité principale de l'entreprise et de chacun de ses établissements.",
	*	property="ape_code",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $ape_code;

	/**
	* @OA\Property(
	*	title="Logo",
	*	description="URL du logo de la collectivité afin d'illustrer le bandeau sur laquelle elle figure en tant que producteur de données. Le logo est hébergé sur cette plateforme afin de centraliser cette ressource au cas où elle serait mise à jour par la collectivité.",
	*	property="logo",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $logo;

	/**
	* @OA\Property(
	*	title="Adresse de la collectivité",
	*	description="Dernière adresse connue de la collectivité remontée par le service Insee Sirene V3.",
	*	property="adresse",
	*	type="object",
	*	ref="#/components/schemas/Adresse"
	* )
	*
	* @var Adresse
	*/
	private $adresse;

	/**
	* @OA\Property(
	*	title="Liste des actes (réglementaires ou délibérations)",
	*	description="Liste des actes réglementaires ou délibérations de la collectivité classée par date.",
	*	property="actes",
	*	type="array",
	*	@OA\Items(
	*		oneOf={
	*			@OA\Schema(ref="#/components/schemas/ActeReglementaire"),
	*			@OA\Schema(ref="#/components/schemas/Deliberation"),
	*			@OA\Schema(ref="#/components/schemas/Budget")	
	*		}
	*	)
	* )
	*
	* @var array
	*/
	private $actes;

	/**
	* @OA\Property(
	*	title="Liste des équipements",
	*	description="Liste des équipements de la collectivité",
	*	property="equipements",
	*	type="array",
	*	@OA\Items(
	*		@OA\Schema(ref="#/components/schemas/Equipement")
	*	)
	* )
	*
	* @var array
	*/
	private $equipements;

    /**
	 * Default constructor
	 */
	function __construct() {
		$this->actes 		= new \ArrayObject();
		$this->equipements 	= new \ArrayObject();
	}

	/**
	 * Get the value of siren
	 */ 
	public function getSiren() {
		return $this->siren;
	}

	/**
	 * Set the value of siren
	 *
	 * @param  string  $siren
	 * @return  self
	 */ 
	public function setSiren(string $siren) {
		$this->siren = $siren;
		return $this;
	}

	/**
	 * Get the value of nic
	 */ 
	public function getNic() {
		return $this->nic;
	}

	/**
	 * Set the value of nic
	 *
	 * @param  string  $nic
	 * @return  self
	 */ 
	public function setNic(string $nic) {
		$this->nic = $nic;
		return $this;
	}

	/**
	 * Get the value of name
	 */ 
	public function getName() {
		return $this->name;
	}

	/**
	 * Set the value of name
	 *
	 * @param  string  $name
	 * @return  self
	 */ 
	public function setName(string $name) {
		$this->name = $name;
		return $this;
	}

	/**
	 * Get the value of ape_code
	 */ 
	public function getApe_code() {
		return $this->ape_code;
	}

	/**
	 * Set the value of ape_code
	 *
	 * @param  string  $ape_code
	 * @return  self
	 */ 
	public function setApe_code(string $ape_code) {
		$this->ape_code = $ape_code;
		return $this;
	}

	/**
	 * Get the value of logo
	 */ 
	public function getLogo() {
		return $this->logo;
	}

	/**
	 * Set the value of logo
	 *
	 * @param  string  $logo
	 * @return  self
	 */ 
	public function setLogo(string $logo) {
		$this->logo = $logo;
		return $this;
	}

	/**
	 * Get the value of adresse
	 */ 
	public function getAdresse() {
		return $this->adresse;
	}

	/**
	 * Set the value of adresse
	 *
	 * @param  Adresse  $adresse
	 * @return  self
	 */ 
	public function setAdresse(Adresse $adresse) {
		$this->adresse = $adresse;
		return $this;
	}

	/**
	 * Get the value of actes
	 */ 
	public function getActes() {
		return $this->actes;
	}

	/**
	 * Set the value of actes
	 *
	 * @param  array  $actes
	 * @return  self
	 */ 
	public function setActes(array $actes) {
		$this->actes = $actes;
		return $this;
	}

	/**
	 * Set the value of equipements
	 *
	 * @param  array  $equipements
	 * @return  self
	 */ 
	public function setEquipements(array $equipements) {
		$this->equipements = $equipements;
		return $this;
	}

	/**
	 * Get the value of equipements
	 */ 
	public function getEquipements() {
		return $this->equipements;
	}

	/**
	 * To string
	 */
	public function __toString() {
		try {
			return $this->siren.' - '.$this->nic.' - '.$this->adresse;
        } catch (\Exception $exception) {
            return '';
        }
	}

	/**
	 * JSON serialization
	 */
	public function jsonSerialize() {
        return get_object_vars($this);
    }
}