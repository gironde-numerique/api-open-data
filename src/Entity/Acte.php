<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Entity;

use ApiOpenData\Utils\StringUtils;

/**
 * Abstract class Acte
 *
 * @package ApiOpenData\Entity
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
abstract class Acte {

	/**
	* @OA\Property(
	*	title="Identifiant de l'acte (délibération, acte réglementaire)",
	*	description="DELIB_ID : Identifiant interne de délibération respectant le formalisme propre à la collectivité. Sa composition dépend des pratiques en vigueur au sein de chaque collectivité.",
	*	property="id",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $id;

	/**
	* @OA\Property(
	*	title="Date d'adoption de l'acte (délibération, acte réglementaire)",
	*	description="DELIB_DATE : Date de décision de l'acte, celle à laquelle la délibération a été adopté par la collectivité au format AAAA-MM-JJ.",
	*	property="date",
	*	type="string",
	*	format="datetime"
	* )
	*
	* @var Datetime
	*/
	private $date;

	/**
	* @OA\Property(
	*	title="Code de matière issu de la nomenclature ACTES",
	*	description="DELIB_MATIERE_CODE : Ce code correspond à celui de l'indexation de niveau 2 dans la structure arborescente de classement en matières et sous-matières (5 niveaux de profondeur) de la nomenclature ACTES (Aide au Contrôle de légaliTé dématErialiSé).",
	*	property="subject_code",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $subject_code;

	/**
	* @OA\Property(
	*	title="Nom de matière",
	*	description="DELIB_MATIERE_NOM : Ce nom peut être issu de la nomenclature ACTES ou d'un référentiel propre à la collectivité.",
	*	property="subject_name",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $subject_name;

	/**
	* @OA\Property(
	*	title="Objet de l'acte (délibération, acte réglementaire)",
	*	description="DELIB_OBJET : Description de l'objet de l'acte (délibération, acte réglementaire) saisie lors du dépôt de l'acte auprès du contrôle de légalité dématérialisé.",
	*	property="object",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $object;

	/**
	* @OA\Property(
	*	title="Url du document de l'acte (délibération, acte réglementaire)",
	*	description="DELIB_URL : Lien vers le système de fichier ouvert sur Internet où est déposé le document de l'acte (délibération, acte réglementaire).",
	*	property="url",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $url;

	/**
	* @OA\Property(
	*	title="Nom de la collectivité",
	*	description="COLL_NOM : Nom officiel de la collectivité délibérante (issu de la base de données Insee Sirene V3).",
	*	property="organization_name",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $organization_name;

	/**
	* @OA\Property(
	*	title="Code SIRET de la collectivité",
	*	description="COLL_SIRET : Identifiant du [Système d'Identification du Répertoire des Etablissements] (SIRET) de la collectivité qui a adopté la délibération, composé de 9 chiffres SIREN + 5 chiffres NIC d'un seul tenant.",
	*	property="organization_siret",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $organization_siret;

    /**
	 * Fields construtor
	 * 
	 * @param  string  $id Identifiant de la délibération
	 * @param  string  $date Date de la délibération
	 * @param  string  $object	Objet/descriptif de la délibération
	 * @param  string  $organization_name Nom de la collectivité
	 * @param  string  $organization_siret SIRET (SIREN + NIC) de la collectivité
	 * @param  string  $url URL où est hébergé de le document de la délibération
	 */
	function __construct(string $id, string $date, string $object, string $organization_name, string $organization_siret, string $url) {
		$this->id 					= $id;
		$this->date 				= $date;
		$this->object 				= $object;
		$this->organization_name 	= $organization_name;
		$this->organization_siret 	= $organization_siret;
		$this->url 					= $url;
	}

	/**
	 * Get the value of id
	 */ 
	public function getId() {
		return $this->id;
	}

	/**
	 * Set the value of id
	 *
	 * @param  string $id
	 * @return  self
	 */ 
	public function setId(string $id) {
		$this->id = $id;
		return $this;
	}

	/**
	 * Get the value of date
	 */ 
	public function getDate() {
		return $this->date;
	}

	/**
	 * Get the formatted date as d/m/Y
	 */ 
	public function getFormattedDate() {
		$formattedDate = '';
		if (!empty($this->date) && strlen($this->date) > 9) {
			$datetime = StringUtils::formatDate(substr($this->date, 0, 9), 'Y-m-d');
			$formattedDate = $datetime->format('d/m/Y');
		}
		return $formattedDate;
	}

	/**
	 * Set the value of date
	 *
	 * @param  string $date
	 * @return  self
	 */ 
	public function setDate(string $date) {
		$this->date = $date;
		return $this;
	}

	/**
	 * Get the value of subject_code
	 */ 
	public function getSubject_code() {
		return $this->subject_code;
	}

	/**
	 * Set the value of subject_code
	 *
	 * @param  string $subject_code
	 * @return  self
	 */ 
	public function setSubject_code(string $subject_code) {
		$this->subject_code = $subject_code;
		return $this;
	}

	/**
	 * Get the value of subject_name
	 */ 
	public function getSubject_name() {
		return $this->subject_name;
	}

	/**
	 * Set the value of subject_name
	 *
	 * @param  string $subject_name
	 * @return  self
	 */ 
	public function setSubject_name(string $subject_name) {
		$this->subject_name = $subject_name;
		return $this;
	}

	/**
	 * Get the value of object
	 */ 
	public function getObject() {
		return ucfirst($this->object);
	}

	/**
	 * Set the value of object
	 *
	 * @param  string $object
	 * @return  self
	 */ 
	public function setObject(string $object) {
		$this->object = $object;
		return $this;
	}

	/**
	 * Get the value of url
	 */ 
	public function getUrl() {
		return $this->url;
	}

	/**
	 * Set the value of url
	 *
	 * @param  string $url
	 * @return  self
	 */ 
	public function setUrl(string $url) {
		$this->url = $url;
		return $this;
	}

	/**
	 * Get the value of organization_name
	 */ 
	public function getOrganization_name() {
		return $this->organization_name;
	}

	/**
	 * Set the value of organization_name
	 *
	 * @param  string  $organization_name
	 * @return  self
	 */ 
	public function setOrganization_name(string $organization_name) {
		$this->organization_name = $organization_name;
		return $this;
	}

	/**
	 * Get the value of organization_siret
	 */ 
	public function getOrganization_siret() {
		return $this->organization_siret;
	}

	/**
	 * Set the value of organization_siret
	 *
	 * @param  string  $organization_siret
	 * @return  self
	 */ 
	public function setOrganization_siret(string $organization_siret) {
		$this->organization_siret = $organization_siret;
		return $this;
	}

	/**
	 * To string
	 */
	public function __toString() {
		try {
			return $this->id.' - '.$this->date->format('d/m/Y').' - '.$this->object.' - '.$this->url;
        } catch (\Exception $exception) {
            return '';
        }
	}

	/**
	 * JSON serialization
	 */
	public function jsonSerialize() { 
		return get_object_vars($this);
    }
}