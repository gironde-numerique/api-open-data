<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Entity\Enum;

use JsonSerializable;
use MyCLabs\Enum\Enum;

/**
 * Enum class Status
 * Implements MyCLabs\Enum\Enum, usefull to get all values for templates with function Enum::toArray()
 *
 * @package ApiOpenData\Entity\Enum
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class Status extends Enum implements JsonSerializable {

	/**
	 * Status OK code
	 * @var int
	 */
	const OK = 1;

	/**
	 * Status warning code
	 * @var int
	 */
	const WARN = 2;
	
	/**
	 * Status error code
	 * @var int
	 */
    const ERR = 3;

	/**
	* JSON serialization.
	*/
	public function jsonSerialize() {
		return get_object_vars($this);
	}
}