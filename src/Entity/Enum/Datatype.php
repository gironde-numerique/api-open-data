<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Entity\Enum;

use MyCLabs\Enum\Enum;
use ApiOpenData\Utils\ArrayUtils;

/**
 * Enum class Datatype. Set each properties for dataset type.
 * Implements MyCLabs\Enum\Enum, usefull to get all values for templates with function Enum::toArray()
 *
 * @package ApiOpenData\Entity\Enum
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class Datatype extends Enum {

    /**
	 * Datatype deliberations
	 * @var string
	 */
    const DELIB             = 'deliberations';
    const DELIB_COLUMNS     = array('COLL_NOM', 'COLL_SIRET', 'DELIB_ID', 'DELIB_DATE', 'DELIB_MATIERE_CODE', 'DELIB_MATIERE_NOM', 'DELIB_OBJET', 'BUDGET_ANNEE', 'BUDGET_NOM', 'PREF_ID', 'PREF_DATE', 'VOTE_EFFECTIF','VOTE_REEL', 'VOTE_POUR', 'VOTE_CONTRE', 'VOTE_ABSTENTION', 'DELIB_URL');
    const DELIB_PROPERTY    = array(
        'columns'       => Datatype::DELIB_COLUMNS,
        'title'         => 'Délibérations', 
        'description'   => '<div style="text-align:justify;">Au-delà des obligations légales de transmission au contrôle de légalité et de publicité des actes des autorités locales définies dans le Code Général des Collectivités Territoriales, la mise à disposition en Open Data des données relatives aux délibérations adoptées par une collectivité locale doit permettre d’améliorer la transparence des décisions publiques prises par les différentes instances habilitées quelque soit l’échelon territorial considéré.<br /><br />Les données sont structurées suivant le Socle Commun des Données Locales (<a href="https://scdl.opendatafrance.net/docs-next/schemas/deliberations.html" rel="nofollow ugc">voir le détail à cette page</a>).<br /></div>', 
        'keywords'      => array('deliberations', 'scdl', 'scdl-deliberations', 'opendata', 'gironde'),
        'distribution'  => 'Liste des actes de type délibération au format '
    );

    /**
	 * Datatype actesreglementaires
	 * @var string
	 */
    const ACTEREG           = 'actesreglementaires';
    const ACTEREG_PROPERTY  = array(
        'columns'       => Datatype::DELIB_COLUMNS,
        'title'         => 'Actes réglementaires', 
        'description'   => '<div style="text-align:justify;">Au-delà des obligations légales de transmission au contrôle de légalité et de publicité des actes réglementaires des autorités locales définies dans le Code Général des Collectivités Territoriales, la mise à disposition en Open Data des données relatives aux délibérations de type actes réglementaires adoptés par une collectivité locale doit permettre d’améliorer la transparence des décisions publiques prises par les différentes instances habilitées quelque soit l’échelon territorial considéré.<br /><br />Les données sont structurées suivant le Socle Commun des Données Locales (<a href="https://scdl.opendatafrance.net/docs-next/schemas/deliberations.html" rel="nofollow ugc">voir le détail à cette page</a>).<br /></div>', 
        'keywords'      => array('actes reglementaires', 'scdl', 'scdl-deliberations', 'opendata', 'gironde'),
        'distribution'  => 'Liste des actes réglementaires au format '
    );

    /**
	 * Datatype budgets
	 * @var string
	 */
    const BUDG          = 'budgets';
    const BUDG_PROPERTY = array(
        'columns'       => Datatype::DELIB_COLUMNS,
        'title'         => 'Budgets', 
        'description'   => '<div style="text-align:justify;">Au-delà des obligations légales de transmission au contrôle de légalité et de publicité des documents budgétaires et financiers des autorités locales définies dans le Code Général des Collectivités Territoriales, la mise à disposition en Open Data des données relatives aux délibérations de type documents budgétaires et financiers adoptés par une collectivité locale doit permettre d’améliorer la transparence des décisions publiques prises par les différentes instances habilitées quelque soit l’échelon territorial considéré.<br /><br />Les données sont structurées suivant le Socle Commun des Données Locales (<a href="https://scdl.opendatafrance.net/docs-next/schemas/deliberations.html" rel="nofollow ugc">voir le détail à cette page</a>).<br /></div>', 
        'keywords'      => array('budgets', 'scdl', 'scdl-deliberations', 'opendata', 'gironde'),
        'distribution'  => 'Liste des actes de type documents budgétaires et financiers au format '
    );

    /**
	 * Datatype equipements
	 * @var string
	 */
    const EQUIP            = 'equipements';
    const EQUIP_COLUMNS    = array('COLL_NOM', 'COLL_SIRET', 'EQUIP_UID', 'EQUIP_THEME', 'EQUIP_CODE', 'EQUIP_TYPE', 'EQUIP_NOM', 'ADR_NUMERO', 'ADR_NOMVOIE', 'ADR_CODEPOSTAL', 'ADR_COMMUNE', 'ADR_CLE_INTEROP', 'EQUIP_LAT', 'EQUIP_LONG', 'EQUIP_OUVERTURE', 'EQUIP_TEL', 'EQUIP_EMAIL', 'EQUIP_WEB');
    const EQUIP_PROPERTY   = array(
        'columns'       => Datatype::EQUIP_COLUMNS,
        'title'         => 'Équipements publics', 
        'description'   => '<div style="text-align:justify;">Liste des équipements collectifs publics. Les données sont structurées suivant le Socle Commun des Données Locales (<a href="https://scdl.opendatafrance.net/docs-next/schemas/equipements.html" rel="nofollow ugc">voir le détail à cette page</a>).<br /></div>', 
        'keywords'      => array('equipements', 'scdl', 'scdl-equipements', 'opendata', 'gironde'),
        'distribution'  => 'Liste des équipements publics au format '
    );

    public static function getProperties() : array {
        return array(
            Datatype::DELIB     => Datatype::DELIB_PROPERTY,
            Datatype::ACTEREG   => Datatype::ACTEREG_PROPERTY,
            Datatype::BUDG      => Datatype::BUDG_PROPERTY,
            Datatype::EQUIP     => Datatype::EQUIP_PROPERTY
        );
    }

    public static function getProperty(string $datatype) : array {
        $array = Datatype::getProperties();
        return ArrayUtils::get($array, $datatype);
    }
}