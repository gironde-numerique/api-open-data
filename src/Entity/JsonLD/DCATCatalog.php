<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Entity\JsonLD;

/**
 * Class Catalog to manipulate JSON-LD DCAT catalog format
 *
 * @package ApiOpenData\Entity\JsonLD
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class DCATCatalog {

    /**
     * Property structure default values
     * @var array
     */
    protected $structure = [
        '@context'      => 'https://project-open-data.cio.gov/v1.1/schema/catalog.jsonld',
        '@type'         => 'dcat:Catalog',
        'conformsTo'    => 'https://project-open-data.cio.gov/v1.1/schema',
        'describedBy'   => 'https://project-open-data.cio.gov/v1.1/schema/catalog.json',
        'language'      => ['fr-FR'],
        'datasets'      => []
    ];

    /**
     * Catalog constructor.
     *
     * @param array $datasets Catalog datasets
     * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @access public
     */
    public function __construct(array $datasets = []) {
        $this->structure['datasets'] = $datasets;
    }

    /**
     * Generate JSON-LD output
     *
     * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return string
     * @access public
     */
    public function generate() : string {
        return json_encode($this->structure, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }
}