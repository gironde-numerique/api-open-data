<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Entity\JsonLD;

use JsonLd\ContextTypes\AbstractContext;

/**
 * Class Dataset to manipulate JSON-LD DCAT catalog format
 *
 * @package ApiOpenData\Entity\JsonLD
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class DCATDataset {

	/**
	 * Property structure
	 * @var array
	 */
	protected $structure = [
		'@type'         	=> 'dcat:Dataset',
		'identifier'    	=> null,
		'license'			=> 'Licence Ouverte version 2.0',
		'licenseDocument' 	=> 'https://www.etalab.gouv.fr/wp-content/uploads/2017/04/ETALAB-Licence-Ouverte-v2.0.pdf',
		'language'      	=> ['fr-FR'],
		'title'         	=> null,
		'description'   	=> null,
		'keyword'       	=> [],
		'issued'        	=> null,
		'frequency'    		=> null,
		'modified'      	=> null,
		'publisher'     	=> 'API Open Data Gironde Numérique',
		'accessLevel'   	=> 'public',
		'spatial'			=> 'Gironde,France',
		'distribution'  	=> []
	];

	/**
	 * Dataset constructor.
	 *
	 * @param string $identifier Dataset unique identifier
	 * @param string $title Dataset title
	 * @param string $description Dataset description
	 * @param array $keywords Dataset keywords
	 * @param string $issued Dataset creation date
	 * @param int $frequency Dataset update frequency
	 * @param string $modified Dataset update date
	 * @param array $distribution Dataset distribution
	 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @access public
	 */
	public function __construct(string $identifier, string $title, string $description, array $keywords, string $issued, int $frequency, ?string $modified, ?array $distribution = []) {
		$this->structure['identifier']      = $identifier;
		$this->structure['title']           = $title;
		$this->structure['description']     = $description;
		$this->structure['keyword']         = $keywords;
		$this->structure['issued']          = $issued;
		$this->structure['modified']        = $modified;
		$this->structure['frequency']      	= $this->getFrequency($frequency);
		$this->structure['distribution']    = $distribution;
	}

	/**
	 * Return datasets array.
	 *
	 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return array
	 * @access public
	 */
	public function getArray() : array {
		return $this->structure;
	}

	protected function getFrequency(int $numberOfDays) {
		if ($numberOfDays < 1) {
			// Continuous [freq:continuous]
			$frequency = 'continuous';
		} else if ($numberOfDays < 2) {
			// Daily [freq:daily]
			$frequency = 'daily';
		} else if ($numberOfDays < 3) {
			// Three times a week [freq:threeTimesAWeek]
			$frequency = 'threeTimesAWeek';
		} else if ($numberOfDays < 4) {
			// Semiweekly [freq:semiweekly]
			$frequency = 'semiweekly';
		} else if ($numberOfDays < 8) {
			// Weekly [freq:weekly]
			$frequency = 'weekly';
		} else if ($numberOfDays < 11) {
			// Three times a month [freq:threeTimesAMonth]
			$frequency = 'threeTimesAMonth';
		} else if ($numberOfDays < 16) {
			// Semimonthly [freq:semimonthly]
			$frequency = 'semimonthly';
		} else if ($numberOfDays < 31) {
			// Monthly [freq:monthly]
			$frequency = 'monthly';
		} else if ($numberOfDays < 62) {
			// Bimonthly [freq:bimonthly]
			$frequency = 'bimonthly';
		} else if ($numberOfDays < 92) {
			// Three times a year [freq:threeTimesAYear]
			$frequency = 'threeTimesAYear';
		} else if ($numberOfDays < 123) {
			// Quarterly [freq:quarterly]
			$frequency = 'quarterly';
		} else if ($numberOfDays < 184) {
			// Semiannual [freq:semiannual]
			$frequency = 'semiannual';
		} else if ($numberOfDays < 366) {
			// Annual [freq:annual]
			$frequency = 'annual';
		} else if ($numberOfDays < 731) {
			// Biennial [freq:biennial]
			$frequency = 'biennial';
		} else {
			// Irregular [freq:irregular]
			$frequency = 'irregular';
		}

		return $frequency;
	}
}