<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Entity\JsonLD;

/**
 * Class Distribution to manipulate JSON-LD DCAT catalog format
 *
 * @package ApiOpenData\Entity\JsonLD
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class DCATDistribution {

    public const CSV   = 'CSV';
    public const JSON  = 'JSON';

    /**
     * Property structure
     * @var array
     */
    protected $structure = [
        '@type'             => 'dcat:Distribution',
        'title'             => null,
        'description'       => null,
        'license'		    => 'etalab-2.0',
		'licenseDocument' 	=> DOMAIN.'/swagger/ressources/docs/ETALAB-Licence-Ouverte-v2.0.pdf',
        'format'            => null,
        'mediaType'         => null,
        'filesize'          => null,
        'checksum'          => null,
        'accessURL'         => null
    ];

    /**
     * Distribution constructor.
     *
     * @param string $description Distribution's description
     * @param string $type Distribution type (CSV or JSON)
     * @param int $filesize Distribution file size
     * @param string $checksum Distribution file checksum
     * @param string $url Distribution access URL
     * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @access public
     */
    public function __construct(string $description, string $type, int $filesize, string $checksum, string $url) {
        $format = '';
        if ($type == self::CSV) {
            $format                         = self::CSV;
            $this->structure['title']       = $format;
            $this->structure['format']      = $format;
            $this->structure['mediaType']   = 'text/csv';
        } else if ($type == self::JSON) {
            $format                         = self::JSON;
            $this->structure['title']       = 'Open Data Gironde Numérique REST API';
            $this->structure['format']      = 'Open Data Gironde Numérique';
            $this->structure['mediaType']   = 'application/json';
        }
        $this->structure['description']     = $description.$format;
        $this->structure['filesize']        = $this->formatBytes($filesize);
        $this->structure['checksum']        = $checksum;
        $this->structure['accessURL']       = $url;
    }

    /**
     * Return distribution array.
     *
     * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return array
     * @access public
     */
    public function getArray() : array {
        return $this->structure;
    }

    /**
     * Format filesize with its close size unit.
	 * 
	 * @param int $bytes Filesize in bytes
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return string
	 * @access private
     */
    function formatBytes(int $bytes) {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes/1073741824, 2).'GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes/1048576, 2).'MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes/1024, 2).'KB';
        } elseif ($bytes < 1024) {
            $bytes = $bytes.'B';
        }

        return $bytes;
    }
}