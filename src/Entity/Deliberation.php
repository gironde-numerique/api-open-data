<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Entity;

/**
 * Class Deliberation
 *
 * @package ApiOpenData\Entity
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 *
 * @OA\Schema(
 *	description="Spécification du modèle de données relatif aux délibérations adoptées par une collectivité locale selon le modèle Open Data France : https://git.opendatafrance.net/scdl/deliberations version 2.1.3.",
 *	title="Modèle délibération",
 *	required={"id", "date", "object", "organization_name", "organization_siret", "url"},
 *	@OA\Xml(
 *		name="Deliberation"
 *	)
 * )
 */
class Deliberation extends Acte implements \JsonSerializable {

	/**
	* @OA\Property(
	*	title="Année du budget",
	*	description="BUDGET_ANNEE : Année de l'exercice budgétaire sur lequel s'applique la décision si celle-ci a un impact budgétaire. Format AAAA pour une année ou AAAA/AAAA pour un intervalle entre deux années.",
	*	property="budget_year",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $budget_year;

	/**
	* @OA\Property(
	*	title="Nom du budget",
	*	description="BUDGET_NOM : Ce champ ne peut être renseigné que si la délibération engendre une affection budgétaire.",
	*	property="budget_name",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $budget_name;

	/**
	* @OA\Property(
	*	title="Identifiant de l'entité exerçant le contrôle de légalité",
	*	description="PREF_ID : Cet identifiant dépend de l'entité concernée. Pour les préfectures, il est codé 'PREFNNN' sur 7 caractères. Pour les sous-préfectures, il est codé 'SPREFNNNM' sur 9 caractères. Pour les SGAR, il est codé 'SGARNNN' sur 7 caractères. 'NNN' correspond au numéro sur 3 caractères du département préfixé par '0' et inclant 'A' et 'B' pour les départements corses. 'M' correspond au numéro sur un chiffre de l'arrondissement.",
	*	property="prefecture_id",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $prefecture_id;

	/**
	* @OA\Property(
	*	title="Date d'enregistrement de la délibération auprès du contrôle de légalité",
	*	description="PREF_DATE : Date d'enregistrement de la délibération au contrôle de légalité au format AAAA-MM-JJ.",
	*	property="prefecture_date",	
	*	type="string",
	*	format="datetime"
	* )
	*
	* @var Datetime
	*/
	private $prefecture_date;

	/**
	* @OA\Property(
	*	title="Effectif théorique des votants",
	*	description="VOTE_EFFECTIF : Décompte de l'effectif total des représentants élus susceptibles de participer au vote.",
	*	property="effective_vote",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $effective_vote;

	/**
	* @OA\Property(
	*	title="Effectif réel des votants",
	*	description="VOTE_REEL : Décompte de l'effectif total des élus ayant réellement participé au vote (exclusion des absents).",
	*	property="real_vote",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $real_vote;

	/**
	* @OA\Property(
	*	title="Total des votes 'pour'",
	*	description="VOTE_POUR : Décompte du nombre total de votes 'Pour'.",
	*	property="for_vote",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $for_vote;

	/**
	* @OA\Property(
	*	title="Total des votes 'contre'",
	*	description="VOTE_CONTRE : Décompte du nombre total de votes 'Contre'.",
	*	property="against_vote",	
	*	type="string"
	* )
	*
	* @var string
	*/
	private $against_vote;

	/**
	* @OA\Property(
	*	title="Total des abstentions",
	*	description="VOTE_ABSTENTION : Décompte du nombre total d'abstentions.",
	*	property="abstention_vote",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $abstention_vote;

	/**
	 * Get the value of budget_year
	 */ 
	public function getBudget_year() {
		return $this->budget_year;
	}

	/**
	 * Set the value of budget_year
	 *
	 * @param  int $budget_year
	 * @return  self
	 */ 
	public function setBudget_year(int $budget_year) {
		$this->budget_year = $budget_year;
		return $this;
	}

	/**
	 * Get the value of budget_name
	 */ 
	public function getBudget_name() {
		return $this->budget_name;
	}

	/**
	 * Set the value of budget_name
	 *
	 * @param  string $budget_name
	 * @return  self
	 */ 
	public function setBudget_name(string $budget_name) {
		$this->budget_name = $budget_name;
		return $this;
	}

	/**
	 * Get the value of prefecture_id
	 */ 
	public function getPrefecture_id() {
		return $this->prefecture_id;
	}

	/**
	 * Set the value of prefecture_id
	 *
	 * @param  string $prefecture_id
	 * @return  self
	 */ 
	public function setPrefecture_id(string $prefecture_id) {
		$this->prefecture_id = $prefecture_id;
		return $this;
	}

	/**
	 * Get the value of prefecture_date
	 */ 
	public function getPrefecture_date() {
		return $this->prefecture_date;
	}

	/**
	 * Set the value of prefecture_date
	 *
	 * @param  DateTime $prefecture_date
	 * @return  self
	 */ 
	public function setPrefecture_date(DateTime $prefecture_date) {
		$this->prefecture_date = $prefecture_date;
		return $this;
	}

	/**
	 * Get the value of effective_vote
	 */ 
	public function getEffective_vote() {
		return $this->effective_vote;
	}

	/**
	 * Set the value of effective_vote
	 *
	 * @param  int  $effective_vote
	 * @return  self
	 */ 
	public function setEffective_vote(int $effective_vote) {
		$this->effective_vote = $effective_vote;
		return $this;
	}

	/**
	 * Get the value of real_vote
	 */ 
	public function getReal_vote() {
		return $this->real_vote;
	}

	/**
	 * Set the value of real_vote
	 *
	 * @param  int  $real_vote
	 * @return  self
	 */ 
	public function setReal_vote(int $real_vote) {
		$this->real_vote = $real_vote;
		return $this;
	}

	/**
	 * Get the value of for_vote
	 */ 
	public function getFor_vote() {
		return $this->for_vote;
	}

	/**
	 * Set the value of for_vote
	 *
	 * @param  int  $for_vote
	 * @return  self
	 */ 
	public function setFor_vote(int $for_vote) {
		$this->for_vote = $for_vote;
		return $this;
	}

	/**
	 * Get the value of against_vote
	 */ 
	public function getAgainst_vote() {
		return $this->against_vote;
	}

	/**
	 * Set the value of against_vote
	 *
	 * @param  int  $against_vote
	 * @return  self
	 */ 
	public function setAgainst_vote(int $against_vote) {
		$this->against_vote = $against_vote;
		return $this;
	}

	/**
	 * Get the value of abstention_vote
	 */ 
	public function getAbstention_vote() {
		return $this->abstention_vote;
	}

	/**
	 * Set the value of abstention_vote
	 *
	 * @param  int  $abstention_vote
	 * @return  self
	 */ 
	public function setAbstention_vote(int $abstention_vote) {
		$this->abstention_vote = $abstention_vote;
		return $this;
	}

	/**
	 * Get deliberation object line as array
	 *
	 * @return  array
	 */ 
	public function toArray() {
		return array($this->getOrganization_name(), $this->getOrganization_siret(), $this->getId(), $this->getFormattedDate(), $this->getSubject_code(), $this->getSubject_name(), $this->getObject(), $this->getBudget_year(), $this->getBudget_name(), $this->getPrefecture_id(), $this->getPrefecture_date(), $this->getEffective_vote(), $this->getReal_vote(), $this->getFor_vote(), $this->getAgainst_vote(), $this->getAbstention_vote(), $this->getUrl());
	}
}