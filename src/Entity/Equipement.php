<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Entity;

use ApiOpenData\Entity\Adresse;

/**
 * Class Equipement
 *
 * @package ApiOpenData\Entity
 * @author  Alexis ZUCHER <a.zucher@girondenumerique.fr>
 *
 * @OA\Schema(
 *	description="Spécification du modèle de données relatif aux équipements collectifs publics d'une collectivité selon le modèle Open Data France : https://git.opendatafrance.net/scdl/deliberations version 2.1.3.",
 *	title="Modèle équipement",
 *	required={"id", "object", "organization_name", "organization_siret"},
 *	@OA\Xml(
 *		name="Equipement"
 *	)
 * )
 */
class Equipement implements \JsonSerializable {

    /**
	* @OA\Property(
	*	title="Nom de la collectivité",
	*	description="COLL_NOM : Nom officiel de la collectivité sur le territoire de laquelle sont situés les équipements collectifs publics répertoriés dans le jeu de données. Ce nom est limité à 140 caractères maximum.",
	*	property="organization_name",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $organization_name;

    /**
	* @OA\Property(
	*	title="Code SIRET de la collectivité",
	*	description="COLL_SIRET : Identifiant du [Système d'Identification du Répertoire des Etablissements] (SIRET) de la collectivité qui a adopté la délibération, composé de 9 chiffres SIREN + 5 chiffres NIC d'un seul tenant.",
	*	property="organization_siret",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $organization_siret;

    /**
    * @OA\Property(
    *   title="Identifiant unique de l'équipement",
    *   description="EQUIP_UID : Cet identifiant unique est constitué du code INSEE de la commune où est implanté l'équipement sur 5 caractères (incluant 'A' ou 'B' pour la Corse) suivi du code d'identification de l'équipement (EQUIP_CODE), séparés par un tiret du milieu. Il s'agit donc d'une chaîne de 18 caractères qui permet d'identifier chacun des équipements référencés de manière univoque.",
    *   property="uid",
    *   type="string"
    * )
    *
    * @var string
    */
    private $uid;
    
    /**
    * @OA\Property(
    *   title="Thème de classement de l'équipement",
    *   description="EQUIP_THEME : Les entrées de la nomenclature des équipements collectifs publics sont divisées en 10 grandes familles. Les intitulés de ces grandes familles sont utilisés pour classer les équipements par thème. Ce champ doit donc être renseigné à partir d'une des valeurs suivantes : 'Equipement administratif', 'Equipement de justice', 'Equipement sanitaire', 'Equipement social et d'animation', 'Equipement sportif et de loisirs', 'Equipement d'enseignement', 'Equipement cultuel', 'Equipement culturel', 'Equipement de mobilité', ou 'Autre équipement'.",
    *   property="theme",
    *   type="string"
    * )
    *
    * @var string
    */
	private $theme;
	
	/**
    * @OA\Property(
    *   title="Code d'identification de l'équipement",
    *   description="EQUIP_CODE : Le code d'identification de l'équipement est constitué du code sur 8 chiffres des niveaux 3 ou 4 (quand il existe) de la nomenclature des équipements collectifs publics, suivi d'un numéro d'ordre sur 3 chiffres (de '001' minimum à '999' maximum), séparés par un tiret du milieu. Il est utilisé pour construire l'identifiant unique de l'équipement (EQUIP_UID). En fonction du niveau et donc du code choisi dans la nomenclature, un des termes associés doit être reporté en tant que valeur pour définir le type d'équipement (EQUIP_TYPE).",
    *   property="code",
    *   type="string"
    * )
    *
    * @var string
    */
	private $code;
    
    /**
    * @OA\Property(
    *   title="Type d'équipement",
    *   description="EQUIP_TYPE : Le type d'équipement correspond à un des termes associés au code choisi dans la nomenclature des équipements collectifs publics pour identifier l'équipement dans EQUIP_CODE. Il s'agit donc de renseigner ce champ avec une valeur, jugée la plus pertinente pour désigner l'équipement, dans la limite de 140 caractères maximum en prenant soin d'échapper ou de supprimer les éventuelles virgules.",
    *   property="type",
    *   type="string"
    * )
    *
    * @var string
    */
    private $type;
    
    /**
	* @OA\Property(
	*	title="Nom complet de l'équipement",
	*	description="EQUIP_NOM : Ce champ permet de nommer l'équipement collectif public par son nom d'usage complet afin de préciser ou compléter, si nécessaire, le terme utilisé pour désigner le type, dans la limite de 256 caractères maximum.",
	*	property="nom",
	*	type="string"
	* )
	*
	* @var string
	*/
    private $nom;
    
    /**
	* @OA\Property(
	*	title="Adresse de la collectivité",
	*	description="Dernière adresse connue de la collectivité remontée par le service Insee Sirene V3.",
	*	property="adresse",
	*	type="object",
	*	ref="#/components/schemas/Adresse"
	* )
	*
	* @var Adresse
	*/
	private $adresse;

    /**
	* @OA\Property(
	*	title="Latitude",
	*	description="EQUIP_LAT : Coordonnée de latitude exprimée en WGS 84 permettant de localiser l'équipement collectif public. Le signe de séparation entre les parties entière et décimale du nombre est le point.",
	*	property="latitude",
	*	type="number",
	* 	format="float"
	* )
	*
	* @var float
	*/
    private $latitude;

    /**
	* @OA\Property(
	*	title="Longitude",
	*	description="EQUIP_LONG : Un ou plusieurs mot(s) clé(s) utilisé(s) pour décrire le jeu de données en minuscules non accentuées. S'il y en a plusieurs, le séparateur est le point-virgule.",
	*	property="longitude",
	*	type="number",
	* 	format="float"
	* )
	*
	* @var float
	*/
	private $longitude;
	
	/**
	* @OA\Property(
	*	title="Jours et horaires d'ouverture",
	*	description="EQUIP_OUVERTURE : Ce champ permet de renseigner, si l'information est connue, les jours et horaires d'ouverture de l'équipement en respectant le format utilisé pour la clé 'opening_hours' dans OpenStreetMap. Un outil comme YoHours facilite la transformation des jours et horaires d'ouverture dans ce format. Celui-ci pouvant contenir des virgules comme signes de séparation, il est nécessaire d'entourer les valeurs de la chaîne de caractères par des guillemets doubles.",
	*	property="ouverture",
	*	type="string"
	* )
	*
	* @var string
	*/
    private $ouverture;

    /**
	* @OA\Property(
	*	title="Téléphone",
	*	description="EQUIP_TEL : Ce champ permet de renseigner, si l'information est connue, le numéro de téléphone (du gestionnaire) de l'équipement exprimé en suivant le code de rédaction interinstitutionnel européen.",
	*	property="tel",
	*	type="string"
	* )
	*
	* @var string
	*/
    private $tel;

    /**
	* @OA\Property(
	*	title="Adresse email",
	*	description="EQUIP_EMAIL : Ce champ permet de renseigner, si l'information est connue, l'adresse email (du gestionnaire) de l'équipement.",
	*	property="email",
	*	type="string"
	* )
	*
	* @var string
	*/
    private $email;

    /**
	* @OA\Property(
	*	title="Adresse du site web",
	*	description="EQUIP_WEB : Ce champ permet de renseigner, si l'information est connue, l'url d'accès au site web (du gestionnaire) de l'équipement.",
	*	property="web",
	*	type="string"
	* )
	*
	* @var string
	*/
    private $web;
    
    /**
	 * Constructor
	 */
	function __construct() {
		
    }

    /**
	 * Get the value of organization_name
	 */ 
	public function getOrganization_name() {
		return $this->organization_name;
    }
    
    /**
	 * Set the value of organization_name
	 *
	 * @param  string  $organization_name
	 * @return  self
	 */ 
	public function setOrganization_name(string $organization_name) {
		$this->organization_name = $organization_name;
		return $this;
    }
    
    /**
	 * Get the value of organization_siret
	 */ 
	public function getOrganization_siret() {
		return $this->organization_siret;
	}

	/**
	 * Set the value of organization_siret
	 *
	 * @param  string  $organization_siret
	 * @return  self
	*/ 
	public function setOrganization_siret(string $organization_siret) {
		$this->organization_siret = $organization_siret;
		return $this;
    }
    
    /**
	 * Get the value of uid
	 */ 
	public function getUid() {
		return $this->uid;
	}

	/**
	 * Set the value of uid
	 *
	 * @param  string $uid
	 * @return  self
	 */ 
	public function setUid(string $uid) {
		$this->uid = $uid;
		return $this;
    }
    
    /**
	 * Get the value of theme of equipment
	 */ 
	public function getTheme() {
		return $this->theme;
	}

    /**
	 * Set the value of theme of equipment
	 *
	 * @param  string $theme
	 * @return  self
	 */ 
	public function setTheme(string $theme) {
		$this->theme = $theme;
		return $this;
	}
	
	/**
	 * Get the value of code of equipment
	 */ 
	public function getCode() {
		return $this->code;
	}

	/**
	 * Set the value of code of equipment
	 *
	 * @param  string  $code
	 * @return  self
	 */ 
	public function setCode(string $code) {
		$this->code = $code;
		return $this;
	}
    
    /**
	 * Get the value of type of equipment
	 */ 
	public function getType() {
		return $this->type;
	}

    /**
	 * Set the value of type of equipment
	 *
	 * @param  string $type
	 * @return  self
	 */ 
	public function setType(string $type) {
		$this->type = $type;
		return $this;
    }
    
    /**
	 * Get the value of the name of the equipment
	 */ 
	public function getNom() {
		return $this->nom;
	}

    /**
	 * Set the value of the name of the equipment
	 *
	 * @param  string $nom
	 * @return  self
	 */ 
	public function setNom(string $nom) {
		$this->nom = $nom;
		return $this;
    }
    
    /**
	 * Get the value of adresse
	 *
	 * @return  Adresse
	 */ 
	public function getAdresse() {
		return $this->adresse;
	}

	/**
	 * Set the value of adresse
	 *
	 * @param  Adresse  $adresse
	 * @return  self
	 */ 
	public function setAdresse(Adresse $adresse) {
		$this->adresse = $adresse;
		return $this;
	}
    
    /**
	 * Get the value of the latitude of the equipment
	 */ 
	public function getLatitude() {
		return $this->latitude;
	}

    /**
	 * Set the value of the latitude of the equipment
	 *
	 * @param  string $latitude
	 * @return  self
	 */ 
	public function setLatitude(string $latitude) {
		$this->latitude = $latitude;
		return $this;
	}
	
	/**
	 * Get the value of the longitude of the equipment
	 */ 
	public function getLongitude() {
		return $this->longitude;
	}

    /**
	 * Set the value of the longitude of the equipment
	 *
	 * @param  string $longitude
	 * @return  self
	 */ 
	public function setLongitude(string $longitude) {
		$this->longitude = $longitude;
		return $this;
	}

	/**
     * Get the value of ouverture
     */ 
    public function getOuverture() {
        return $this->ouverture;
    }

    /**
     * Set the value of ouverture
     *
     * @param  string  $ouverture
     * @return  self
     */ 
    public function setOuverture(string $ouverture) {
        $this->ouverture = $ouverture;
        return $this;
    }
	
	/**
	 * Get the value of phone number
	 */ 
	public function getTel() {
		return $this->tel;
	}

    /**
	 * Set the value of phone number
	 *
	 * @param  string $tel
	 * @return  self
	 */ 
	public function setTel(string $tel) {
		$this->tel = $tel;
		return $this;
	}
	
	/**
	 * Get the value of email
	 */ 
	public function getEmail() {
		return $this->email;
	}

    /**
	 * Set the value of email
	 *
	 * @param  string $email
	 * @return  self
	 */ 
	public function setEmail(string $email) {
		$this->email = $email;
		return $this;
	}
	
	/**
	 * Get the value of web url
	 */ 
	public function getWeb() {
		return $this->web;
	}

    /**
	 * Set the value of web url
	 *
	 * @param  string $web
	 * @return  self
	 */ 
	public function setWeb(string $web) {
		$this->web = $web;
		return $this;
	}

    /**
	 * JSON serialization
	*/
	public function jsonSerialize() {
		return get_object_vars($this);
	}
	
	/**
	 * Get equipement object line as array
	 *
	 * @return  array
	 */ 
	public function toArray() {
		return array($this->getOrganization_name(), $this->getOrganization_siret(), $this->getUid(), $this->getTheme(), $this->getCode(), $this->getType(), $this->getNom(), $this->getAdresse()->getNumero_voie(), $this->getAdresse()->getNomVoie(), $this->getAdresse()->getCode_postal(), $this->getAdresse()->getCommune(), $this->getAdresse()->getCle_interop(), $this->getLatitude(), $this->getLongitude(), $this->getOuverture(), $this->getTel(), $this->getEmail(), $this->getWeb());
	}
}