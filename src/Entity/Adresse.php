<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Entity;

/**
 * Class Address
 *
 * @package ApiOpenData\Entity
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 *
 * @OA\Schema(
 *	description="Spécification du modèle de données relatif au type adresse employé pour les collectivités et dont les informations sont remontées par le service Insee Sirene V3.",
 *	title="Modèle adresse",
 *	required={"siren", "nic", "name", "address", "deliberations"},
 *	@OA\Xml(
 *		name="Adresse"
 *	)
 * )
 */
class Adresse implements \JsonSerializable {

	/**
	* @OA\Property(
	*	title="Numéro de voie",
	*	description="Numéro de voie de la ligne d'adresse 1.",
	*	property="numero_voie",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $numero_voie;

	/**
	* @OA\Property(
	*	title="Type de voie",
	*	description="Type de voie de la ligne d'adresse 1.",
	*	property="type_voie",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $type_voie;

	/**
	* @OA\Property(
	*	title="Libellé de la voie",
	*	description="Libellé de la voie de la ligne d'adresse 1.",
	*	property="libelle_voie",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $libelle_voie;

	/**
	* @OA\Property(
	*	title="Complément d'adresse",
	*	description="Complément d'adresse, constitue la ligne d'adresse 2.",
	*	property="complement",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $complement;

	/**
	* @OA\Property(
	*	title="Boîte postale",
	*	description="Boîte postale permettant d'externaliser la réception du courrier. Cette donnée peut-être vide.",
	*	property="boite_postale",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $boite_postale;

	/**
	* @OA\Property(
	*	title="Code postal",
	*	description="Code postal de l'adresse, sur 5 caractères.",
	*	property="code_postal",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $code_postal;

	/**
	* @OA\Property(
	*	title="Libellé de la commune",
	*	description="Libellé de la commune de l'adresse.",
	*	property="commune",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $commune;

	/**
	* @OA\Property(
	*	title="CEDEX",
	*	description="L'adresse postale d'entreprises ou d'administrations peut être suivi d'un numéro CEDEX (« Courrier d'Entreprise à Distribution Exceptionnelle »), qui permet aux services de distribution postale de mieux acheminer leur courrier. Cette donnée peut-être vide.",
	*	property="cedex",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $cedex;

	/**
	* @OA\Property(
	*	title="CLE_INTEROP",
	*	description="Cette clé est identique à celle décrite dans le modèle Base adresse locale. Elle combine le code INSEE de la commune sur 5 caractères (incluant 'A' ou 'B' pour la Corse) + le code de voie issu du FANTOIR de la DGFiP sur 4 caractères + le numéro d’adresse sur 5 caractères préfixé par des zéros + un suffixe s'il existe, qui peut être un indice de répétition ('bis', 'ter', 'qua', 'qui', etc... codés sur 3 caractères) et/ou un complément ('a', 'b', 'c', 'a1', 'b2', 'lesmimosas', etc... sans limitation du nombre de caractères). Chaque élément est séparé par un tiret du bas et les lettres sont en minuscules.",
	*	property="cle_interop",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $cle_interop;

    /**
	 * Default constructor
	 */
	function __construct() {
		
	}

	/**
	 * Get the value of numero_voie
	 */ 
	public function getNumero_voie() {
		return $this->numero_voie;
	}

	/**
	 * Set the value of numero_voie
	 *
	 * @param  string  $numero_voie
	 * @return  self
	 */ 
	public function setNumero_voie(string $numero_voie) {
		$this->numero_voie = $numero_voie;
		return $this;
	}

	/**
	 * Get the value of type_voie
	 */ 
	public function getType_voie() {
		return $this->type_voie;
	}

	/**
	 * Set the value of type_voie
	 *
	 * @param  string  $type_voie
	 * @return  self
	 */ 
	public function setType_voie(string $type_voie) {
		$this->type_voie = $type_voie;
		return $this;
	}

	/**
	 * Get the value of libelle_voie
	 */ 
	public function getLibelle_voie() {
		return $this->libelle_voie;
	}

	/**
	 * Set the value of libelle_voie
	 *
	 * @param  string  $libelle_voie
	 * @return  self
	 */ 
	public function setLibelle_voie(string $libelle_voie) {
		$this->libelle_voie = $libelle_voie;
		return $this;
	}

	/**
	 * Get the value of type_voie + libelle_voie
	 */
	public function getNomVoie() {
		$nomVoie = '';
		if (!empty($this->type_voie)) {
			$nomVoie .= $this->type_voie;
		}
		if (!empty($nomVoie) && !empty($this->libelle_voie)) {
			$nomVoie .= ' ';
		}
		if (!empty($this->libelle_voie)) {
			$nomVoie .= $this->libelle_voie;
		}
		return $nomVoie;
	}

	/**
	 * Get the value of complement
	 */ 
	public function getComplement() {
		return $this->complement;
	}

	/**
	 * Set the value of complement
	 *
	 * @param  string  $complement
	 * @return  self
	 */ 
	public function setComplement(string $complement) {
		$this->complement = $complement;
		return $this;
	}

	/**
	 * Get the value of boite_postale
	 */ 
	public function getBoite_postale() {
		return $this->boite_postale;
	}

	/**
	 * Set the value of boite_postale
	 *
	 * @param  string  $boite_postale
	 * @return  self
	 */ 
	public function setBoite_postale(string $boite_postale) {
		$this->boite_postale = $boite_postale;
		return $this;
	}

	/**
	 * Get the value of code_postal
	 */ 
	public function getCode_postal() {
		return $this->code_postal;
	}

	/**
	 * Set the value of code_postal
	 *
	 * @param  string  $code_postal
	 * @return  self
	 */ 
	public function setCode_postal(string $code_postal) {
		$this->code_postal = $code_postal;
		return $this;
	}

	/**
	 * Get the value of commune
	 */ 
	public function getCommune() {
		return $this->commune;
	}

	/**
	 * Set the value of commune
	 *
	 * @param  string  $commune
	 * @return  self
	 */ 
	public function setCommune(string $commune) {
		$this->commune = $commune;
		return $this;
	}

	/**
	 * Get the value of cedex
	 */ 
	public function getCedex() {
		return $this->cedex;
	}

	/**
	 * Set the value of cedex
	 *
	 * @param  string  $cedex
	 * @return  self
	 */ 
	public function setCedex(string $cedex) {
		$this->cedex = $cedex;
		return $this;
	}

	/**
	 * Get the value of cle_interop
	 */ 
	public function getCle_interop() {
		return $this->cle_interop;
	}

	/**
	 * Set the value of cle_interop
	 *
	 * @param  string  $cle_interop
	 * @return  self
	 */ 
	public function setCle_interop(string $cle_interop) {
		$this->cle_interop = $cle_interop;
		return $this;
	}

	/**
	 * To string
	 */
	public function __toString() {
		try {
			return $this->siren.$this->nic.' - '.$this->name;
        } catch (\Exception $exception) {
            return '';
        }
	}

	/**
	 * JSON serialization
	 */
	public function jsonSerialize() {
        return get_object_vars($this);
    }
}