<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Entity;

use JsonSerializable;
use ApiOpenData\Entity\Enum\Status;

/**
 * Class State to manipulate script calls state.
 *
 * @package ApiOpenData\Entity
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class State implements JsonSerializable {

	/**
	* State code
	* @var int
	*/
	private $code;

	/**
	* State message
	* @var string
	*/
	private $message;

    /**
	 * Fields construtor
	 * 
	 * @param Status $code State code
	 * @param string $message State message
	 */
	function __construct(int $code, string $message) {
		$this->code 	= $code;
		$this->message 	= $message;
	}

	/**
	 * Get the state code value
	 */ 
	public function getCode() {
		return $this->code;
	}

	/**
	 * Set the state code value
	 *
	 * @param  int  $code
	 * @return  self
	 */ 
	public function setCode(int $code) {
		$this->code = $code;
		return $this;
	}

	/**
	 * Get the state message value
	 */ 
	public function getMessage() {
		return $this->message;
	}

	/**
	 * Set the state message  value
	 *
	 * @param  string  $message
	 * @return  self
	 */ 
	public function setMessage(string $message) {
		$this->message = $message;
		return $this;
	}

	/**
	 * To string
	 */
	public function __toString() {
		return $this->code.' - '.$this->message;
	}

	/**
	* Sérialisation JSON.
	*/
	public function jsonSerialize() {
		return get_object_vars($this);
	}
}