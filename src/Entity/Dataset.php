<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Entity;

use DateTime;
use JsonSerializable;
use ApiOpenData\Entity\Enum\Datatype;

/**
 * Class Dataset to manipulate DCAT catalog datasets
 *
 * @package ApiOpenData\Entity
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class Dataset implements JsonSerializable {

	/**
	* Dataset organization siren
	* @var string
	*/
	private $siren;

	/**
	* Dataset type
	* @var Datatype
	*/
	private $type;

	/**
	* Dataset creation date
	* @var DateTime
	*/
	private $creationDate;

	/**
	* Dataset update date
	* @var DateTime
	*/
	private $updateDate;

	/**
	* Dataset number of line
	* @var int
	*/
	private $numberOfLine;

    /**
	 * Fields construtor
	 * 
	 * @param int $siren Dataset organization siren
	 * @param Datatype $type Dataset type
	 * @param int $numberOfLine Dataset number of line
	 */
	function __construct(int $siren, Datatype $type, int $numberOfLine) {
		$this->siren		= $siren;
		$this->type 		= $type;
		$this->numberOfLine	= $numberOfLine;
	}

	/**
	 * Get the dataset organization siren
	 */ 
	public function getSiren() {
		return $this->siren;
	}

	/**
	 * Set the dataset organization siren
	 *
	 * @param  string  $siren
	 * @return  self
	 */ 
	public function setSiren(string $siren) {
		$this->siren = $siren;
		return $this;
	}

	/**
	 * Get the dataset type
	 * @return  Datatype
	 */ 
	public function getType() {
		return $this->type;
	}

	/**
	 * Set the dataset type
	 *
	 * @param  Datatype  $type
	 * @return  self
	 */ 
	public function setType(Datatype $type) {
		$this->type = $type;
		return $this;
	}

	/**
	 * Get the dataset creation date
	 */ 
	public function getCreationDate() {
		return $this->creationDate;
	}

	/**
	 * Set the dataset creation date
	 *
	 * @param  DateTime  $creationDate
	 * @return  self
	 */ 
	public function setCreationDate(DateTime $creationDate) {
		$this->creationDate = $creationDate;
		return $this;
	}

	/**
	 * Get the dataset update date
	 */ 
	public function getUpdateDate() {
		return $this->updateDate;
	}

	/**
	 * Set the dataset update date
	 *
	 * @param  DateTime  $updateDate
	 * @return  self
	 */ 
	public function setUpdateDate(DateTime $updateDate) {
		$this->updateDate = $updateDate;
		return $this;
	}

	/**
	 * Get the dataset number of line
	 */ 
	public function getNumberOfLine() {
		return $this->numberOfLine;
	}

	/**
	 * Set the dataset number of line
	 *
	 * @param  int  $numberOfLine
	 * @return  self
	 */ 
	public function setNumberOfLine(int $numberOfLine) {
		$this->numberOfLine = $numberOfLine;
		return $this;
	}

	/**
	 * Transform the dataset object to an array
	 *
	 * @return  array
	 */
	public function toArray() {
		return array(
			'siren' 		=> $this->siren,
			'type' 			=> $this->type->getValue(),
			'creationDate' 	=> $this->creationDate != null ? $this->creationDate->format('') : null,
			'updateDate' 	=> $this->updateDate != null ? $this->updateDate->format('') : null,
			'numberOfLine' 	=> $this->numberOfLine
		);
	}

	/**
	 * To string
	 */
	public function __toString() {
		return $this->siren.' - '.$this->type->getValue().' - '.$this->numberOfLine;
	}

	/**
	* JSON serialization.
	*/
	public function jsonSerialize() {
		return get_object_vars($this);
	}
}