<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Entity;

use ApiOpenData\Entity\Token;
use ApiOpenData\Entity\Credentials;
use ApiOpenData\Entity\Enum\Rank;

/**
 * Class Consumer to manipulate API's users whose information comes from OpenLDAP and the database
 *
 * @package ApiOpenData\Entity
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class Consumer {

	/**
	* Consumer identifier
	* @var int
	*/
	private $id;

	/**
	* Consumer lastname as registered in GN OpenLDAP
	* @var string
	*/
	private $lastname;

	/**
	* Consumer firstname as registered in GN OpenLDAP
	* @var string
	*/
	private $firstname;

	/**
	* Consumer email as registered in GN OpenLDAP
	* @var string
	*/
	private $email;

	/**
	* Consumer key string
	* @var string
	*/
	private $key;

	/**
	* Consumer secret string
	* @var string
	*/
	private $secret;

	/**
	* Consumer current token (bearer/key)
	* @var Token
	*/
	private $token;

	/**
	* Consumer current credentials (hashedPassword/salt)
	* @var Credentials
	*/
	private $credentials;

	/**
	 * Consumer rank
	 * @var Rank
	 */
	private $rank;

    /**
	 * Fields construtor
	 * 
	 * @param int $id Consumer identifier
	 * @param string $firstname Consumer firstname
	 * @param string $lastname Consumer lastname
	 * @param string $email Consumer email
	 */
	function __construct(int $id, string $firstname, string $lastname, string $email) {
		$this->id 			= $id;
		$this->firstname 	= $firstname;
		$this->lastname 	= $lastname;
		$this->email 		= $email;
	}

	/**
	 * Get the value of	id
	 */ 
	public function getId() {
		return $this->id;
	}

	/**
	 * Set the value of id
	 *
	 * @param  int $id
	 * @return  self
	 */ 
	public function setId(int $id) {
		$this->id = $id;
		return $this;
	}

	/**
	 * Get the value of	lastname
	 */ 
	public function getLastname() {
		return $this->lastname;
	}

	/**
	 * Set the value of	lastname
	 *
	 * @param  string $lastname
	 * @return  self
	 */ 
	public function setLastname(string $lastname) {
		$this->lastname = $lastname;
		return $this;
	}

	/**
	 * Get the value of	firstname
	 */ 
	public function getFirstname() {
		return $this->firstname;
	}

	/**
	 * Set the value of firstname
	 *
	 * @param  string $firstname
	 * @return  self
	 */ 
	public function setFirstname(string $firstname) {
		$this->firstname = $firstname;
		return $this;
	}

	/**
	 * Get the value of	email
	 */ 
	public function getEmail() {
		return $this->email;
	}

	/**
	 * Set the value of email
	 *
	 * @param  string $email
	 * @return  self
	 */ 
	public function setEmail(string $email) {
		$this->email = $email;
		return $this;
	}

	/**
	 * Get the value of key
	 */ 
	public function getKey() {
		return $this->key;
	}

	/**
	 * Set the value of key
	 *
	 * @param  string $key
	 * @return  self
	 */ 
	public function setKey(string $key) {
		$this->key = $key;
		return $this;
	}

	/**
	 * Get the value of secret
	 */ 
	public function getSecret() {
		return $this->secret;
	}

	/**
	 * Set the value of secret
	 *
	 * @param  string $secret
	 * @return  self
	 */ 
	public function setSecret(string $secret) {
		$this->secret = $secret;
		return $this;
	}

	/**
	 * Get the value of token
	 */ 
	public function getToken() {
		return $this->token;
	}

	/**
	 * Set the value of token
	 *
	 * @param  Token $token
	 * @return  self
	 */ 
	public function setToken(Token $token) {
		$this->token = $token;
		return $this;
	}

	/**
	 * Get the value of credentials
	 */ 
	public function getCredentials() {
		return $this->credentials;
	}

	/**
	 * Set the value of credentials
	 *
	 * @param  Credentials $credentials
	 * @return  self
	 */ 
	public function setCredentials(Credentials $credentials) {
		$this->credentials = $credentials;
		return $this;
	}

	/**
	 * Get user rank
	 *
	 * @return  Rank
	 */ 
	public function getRank() {
		return $this->rank;
	}

	/**
	 * Set user rank
	 *
	 * @param  Rank  $rank  User rank
	 * @return  self
	 */ 
	public function setRank(Rank $rank) {
		$this->rank = $rank;
		return $this;
	}

	/**
	 * Get the value of base64 encoded credentials
	 */ 
	public function getEncodedSecret() {
		return base64_encode($this->key.':'.$this->secret);
	}

	/**
	 * Transform the consumer object to an array
	 *
	 * @return  array
	 */
	public function toArray() {
		return array(
			'id' 		=> $this->id,
			'firstname' => $this->firstname,
			'lastname' 	=> $this->lastname,
			'email' 	=> $this->email,
			'key' 		=> $this->key,
			'secret' 	=> $this->secret,
			'bearer' 	=> $this->token != null ? $this->token->getBearer() : null,
			'bearerKey' => $this->token != null ? $this->token->getKey() : null,
		);
	}

	/**
	 * To string
	 */
	public function __toString() {
		return $this->id.' - '.$this->firstname.' - '.$this->lastname.' - '.$this->email;
	}
}