<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Entity;

/**
 * Class ActeReglementaire
 *
 * @package ApiOpenData\Entity
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 *
 * @OA\Schema(
 *	description="Spécification du modèle de données relatif aux actes réglementaires adoptées par une collectivité locale, ne faisant pas l'objet d'une délibération (non soumis à un vote). Ces données répondent en partie au modèle préconisé par Open Data France : https://git.opendatafrance.net/scdl/deliberations version 2.1.3.",
 *	title="Modèle acte réglementaire",
 *	required={"id", "date", "object", "organization_name", "organization_siret", "url"},
 *	@OA\Xml(
 *		name="ActeReglementaire"
 *	)
 * )
 */
class ActeReglementaire extends Acte implements \JsonSerializable {

	/**
	 * Get acte reglementaire object line as array
	 *
	 * @return  array
	 */ 
	public function toArray() {
		return array($this->getOrganization_name(), $this->getOrganization_siret(), $this->getId(), $this->getFormattedDate(), $this->getSubject_code(), $this->getSubject_name(), $this->getObject(), null, null, null, null, null, null, null, null, null, $this->getUrl());
	}
	
}