<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Entity;

/**
 * Class Budget
 *
 * @package ApiOpenData\Entity
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 *
 * @OA\Schema(
 *	description="Spécification du modèle de données relatif aux documents budgétaires et financiers des collectivités locales adoptés par délibérations sur la base du modèle Open Data France : https://git.opendatafrance.net/scdl/deliberations version 2.1.3.",
 *	title="Modèle budget",
 *	required={"id", "date", "object", "organization_name", "organization_siret", "url"},
 *	@OA\Xml(
 *		name="Budget"
 *	)
 * )
 */
class Budget extends Acte implements \JsonSerializable {

	/**
	* @OA\Property(
	*	title="Année du budget",
	*	description="BUDGET_ANNEE : Année de l'exercice budgétaire sur lequel s'applique la décision si celle-ci a un impact budgétaire. Format AAAA pour une année ou AAAA/AAAA pour un intervalle entre deux années.",
	*	property="budget_year",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $budget_year;

	/**
	* @OA\Property(
	*	title="Nom du budget",
	*	description="BUDGET_NOM : Ce champ ne peut être renseigné que si la délibération engendre une affection budgétaire.",
	*	property="budget_name",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $budget_name;

	/**
	* @OA\Property(
	*	title="Identifiant de l'entité exerçant le contrôle de légalité",
	*	description="PREF_ID : Cet identifiant dépend de l'entité concernée. Pour les préfectures, il est codé 'PREFNNN' sur 7 caractères. Pour les sous-préfectures, il est codé 'SPREFNNNM' sur 9 caractères. Pour les SGAR, il est codé 'SGARNNN' sur 7 caractères. 'NNN' correspond au numéro sur 3 caractères du département préfixé par '0' et inclant 'A' et 'B' pour les départements corses. 'M' correspond au numéro sur un chiffre de l'arrondissement.",
	*	property="prefecture_id",
	*	type="string"
	* )
	*
	* @var string
	*/
	private $prefecture_id;

	/**
	* @OA\Property(
	*	title="Date d'enregistrement de la délibération auprès du contrôle de légalité",
	*	description="PREF_DATE : Date d'enregistrement de la délibération au contrôle de légalité au format AAAA-MM-JJ.",
	*	property="prefecture_date",	
	*	type="string",
	*	format="datetime"
	* )
	*
	* @var Datetime
	*/
	private $prefecture_date;

	/**
	 * Get the value of budget_year
	 */ 
	public function getBudget_year() {
		return $this->budget_year;
	}

	/**
	 * Set the value of budget_year
	 *
	 * @param  int $budget_year
	 * @return  self
	 */ 
	public function setBudget_year(int $budget_year) {
		$this->budget_year = $budget_year;
		return $this;
	}

	/**
	 * Get the value of budget_name
	 */ 
	public function getBudget_name() {
		return $this->budget_name;
	}

	/**
	 * Set the value of budget_name
	 *
	 * @param  string $budget_name
	 * @return  self
	 */ 
	public function setBudget_name(string $budget_name) {
		$this->budget_name = $budget_name;
		return $this;
	}

	/**
	 * Get the value of prefecture_id
	 */ 
	public function getPrefecture_id() {
		return $this->prefecture_id;
	}

	/**
	 * Set the value of prefecture_id
	 *
	 * @param  string $prefecture_id
	 * @return  self
	 */ 
	public function setPrefecture_id(string $prefecture_id) {
		$this->prefecture_id = $prefecture_id;
		return $this;
	}

	/**
	 * Get the value of prefecture_date
	 */ 
	public function getPrefecture_date() {
		return $this->prefecture_date;
	}

	/**
	 * Set the value of prefecture_date
	 *
	 * @param  DateTime $prefecture_date
	 * @return  self
	 */ 
	public function setPrefecture_date(DateTime $prefecture_date) {
		$this->prefecture_date = $prefecture_date;
		return $this;
	}

	/**
	 * Get budget object line as array
	 *
	 * @return  array
	 */ 
	public function toArray() {
		return array($this->getOrganization_name(), $this->getOrganization_siret(), $this->getId(), $this->getFormattedDate(), $this->getSubject_code(), $this->getSubject_name(), $this->getObject(), $this->getBudget_year(), $this->getBudget_name(), $this->getPrefecture_id(), $this->getPrefecture_date(), null, null, null, null, null, $this->getUrl());
	}
}