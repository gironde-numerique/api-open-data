<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Utils;

use Slim\Views\Twig;

/**
 * Utils class to send HTML email with template.
 *
 * @package ApiOpenData\Utils
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class MailGenerator {

	protected $twig;

	public function __construct(Twig $twig) {
		$this->twig = $twig;
	}

	/**
	 * Generate an HTML email then send it.
	 *
	 * @param String $_identifier Template identifier
	 * @param String $_sender Sender email
	 * @param String $_recipient Recipient email
	 * @param String $_subject Subject email
	 * @param String $_message Message email
	 * @param Array $_params Values array
	 * @return boolean
	 * @access public
	 */
	public function message($_identifier, $_sender, $_recipient, $_subject, $_params) {
		$message = $this->twig->fetchBlock('email/'.$_identifier.'.html', 'email', $_params);

		return $this->send($_sender, $_recipient, $_subject, $message);
	}

	/**
	 * Send an email.
	 *
	 * @param String $_sender Sender email
	 * @param String $_recipient Recipient email
	 * @param String $_subject Subject email
	 * @param String $_message Message email
	 * @return boolean
	 * @access private
	 */
	private function send($_sender, $_recipient, $_subject, $_message) {
		$headers = "From: ".strip_tags($_sender)."\r\n";
		$headers .= "Reply-To: ". strip_tags($_recipient)."\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=utf-8\r\n";

		return mail($_recipient, $_subject, $_message, $headers);
	}
}