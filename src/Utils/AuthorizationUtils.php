<?php
/**
 * @license Apache 2.0
 */

namespace ApiOpenData\Utils;

use Psr\Http\Message\ServerRequestInterface as Request;
use \Firebase\JWT\JWT;
use ApiOpenData\Lib\OpenDataDAOConsumer;
use ApiOpenData\Entity\Consumer;
use ApiOpenData\Entity\Exception\ApiOpenDataException;
use ApiOpenData\Utils\StringUtils;

/**
 * Utils class to help authorization functionnalities.
 *
 * @package ApiOpenData\Utils
 * @author  Xavier MADIOT <x.madiot@girondenumerique.fr>
 */
class AuthorizationUtils {

    /**
     * Open Data DAO consumer
     * @var OpenDataDAOConsumer
     */
    private static $dao;

    /**
     * Generate key and secret for a specific consumer.
     *
     * @param int $consumerId Consumer identifier
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return boolean
     * @access public
     */
    public static function generateKeys(int $consumerId) {
        $newKey     = StringUtils::generateRandomKey($consumerId);
        $newSecret  = StringUtils::generateRandomString();

        self::$dao = new OpenDataDAOConsumer();
        if (self::$dao->updateConsumerKeys($consumerId, $newKey, $newSecret)) {
            // Update key and secret in session only if consumer is authenticated, else it's a subscription
            if (isset($_SESSION['user'])) {
                $user = \unserialize($_SESSION['user']);
                $user->setKey($newKey);
                $user->setSecret($newSecret);
                $_SESSION['user'] = \serialize($user);
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if user is registered then generate JWT bearer with some informations.
     *
     * @param ServerRequestInterface $request Request from Slim framework routing
     * @param string $key Consumer key string
     * @param string $secret Consumer secret string
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return string
     * @throws \Exception if the user does not exist
     * @access public
     */
    public static function generateBearerForKeyAndSecret(Request $request, string $key, string $secret) {
        self::$dao = new OpenDataDAOConsumer();
        $consumer = self::$dao->getConsumerByKeyAndSecret($key, $secret);
        if ($consumer != null) {
            $token = self::generateTokenArray($consumer);
            $bearerKey = StringUtils::generateRandomString();
            $bearer = JWT::encode($token, $bearerKey);

            if (self::$dao->updateBearer($consumer->getId(), $bearer, $bearerKey) 
                    && self::$dao->registerActivity($consumer->getId(), $request->getUri()->getPath())) {
                return $bearer;
            } else {
                throw new ApiOpenDataException('Une erreur est survenue lors de la génération de votre jeton d\'accès');
            }
        } else {
            throw new ApiOpenDataException('Vous n\'êtes pas autorisé à accéder à cette ressource', 403);
        }
    }

    /**
     * Check if user is registered then generate JWT bearer with some informations.
     *
     * @param string $email Consumer email as login
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return boolean
     * @access public
     */
    public static function generateBearerForConsumer(string $email) {
        self::$dao = new OpenDataDAOConsumer();
        $consumer = self::$dao->getConsumerByEmail($email);
        if ($consumer != null) {
            $token = self::generateTokenArray($consumer);
            $bearerKey = StringUtils::generateRandomString();
            $bearer = JWT::encode($token, $bearerKey);

            return self::$dao->updateBearer($consumer->getId(), $bearer, $bearerKey);
        }
        return false;
    }

    /**
     * Generate a token array with creation and expiration date, and consumer information.
     *
     * @param Consumer $consumer Consumer object
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return array
     * @access private
     */
    private static function generateTokenArray(Consumer $consumer) {
        $startDate      = new \DateTime('now');
        $expirationDate = new \DateTime('now');
        $expirationDate->add(new \DateInterval('P30D'));

        return array (
            'iss'   => ISS,
            'aud'   => AUD,
            'iat'   => $startDate->getTimestamp(),
            'exp'   => $expirationDate->getTimestamp(),
            'data'  => array(
                'id'        => $consumer->getId(),
                'firstname' => $consumer->getFirstname(),
                'lastname'  => $consumer->getLastname(),
                'email'     => $consumer->getEmail()
            )
        );
    }
    
}
