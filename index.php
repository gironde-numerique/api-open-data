<?php
/**
 * @license Apache 2.0
 */

require_once __DIR__.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php'; 
require_once __DIR__.DIRECTORY_SEPARATOR.'src'.DIRECTORY_SEPARATOR.'inc.config.php';

use DI\Container;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Csrf\Guard;
use Slim\Exception\HttpBadRequestException;
use Slim\Factory\AppFactory;
use Slim\Flash\Messages;
use Slim\Routing\RouteCollectorProxy;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Odan\Session\PhpSession;
use ApiOpenData\Middleware\EntryMiddleware;
use ApiOpenData\Middleware\AuthenticationMiddleware;
use ApiOpenData\Lib\OpenDataDAOConsumer;
use ApiOpenData\Lib\SireneConsumer;
use ApiOpenData\Lib\SolrConsumer;
use ApiOpenData\Lib\AccesLibreConsumer;

// /!\ Don't forget to indicate current API version in legal mentions /!\ 

/**
 * @OA\Info(
 *     description="Service de mise à disposition des données ouvertes des collectivités du département de la Gironde par le syndicat mixte Gironde numérique.",
 *     version="1.4.2",
 *     title="API Open Data Gironde numérique",
 *     @OA\License(
 *         name="Apache 2.0",
 *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *     ),
 *     @OA\Contact(
 *         email="x.madiot@girondenumerique.fr"
 *     )
 * )
 * @OA\Tag(
 *     name="Collectivités",
 *     description="Les collectivités du département de la Gironde ayant déposées des données."
 * )
 * @OA\Tag(
 *     name="Délibérations",
 *     description="Les délibérations en données ouvertes des collectivités du département de la Gironde."
 * )
 * @OA\Tag(
 *     name="Actes réglementaires",
 *     description="Les actes réglementaires en données ouvertes des collectivités du département de la Gironde."
 * )
 * @OA\Tag(
 *     name="Budgets",
 *     description="Les budgets en données ouvertes des collectivités du département de la Gironde."
 * )
 * @OA\Tag(
 *     name="Équipements",
 *     description="Les équipements (collectifs ou publics) en données ouvertes des collectivités du département de la Gironde."
 * )
 * @OA\Server(
 *     description="Environnement de production du service Open Data Gironde numérique.",
 *     url="https://api-opendata.girondenumerique.fr/api/v1"
 * )
 * @OA\Server(
 *     description="Environnement de test du service Open Data Gironde numérique.",
 *     url="https://api-opendata-test.girondenumerique.fr/api/v1"
 * )
 */

// Initialize API
$container = new Container();
AppFactory::setContainer($container);
$api = AppFactory::create();

// Container initialization
$responseFactory = $api->getResponseFactory();
$container->set('session', function () {
    return new PhpSession();
});
$container->set('csrf', function () use ($responseFactory) {
    $guard = new Guard($responseFactory);
    $guard->setPersistentTokenMode(true);
    $guard->setFailureHandler(function (Request $request) {
        throw new HttpBadRequestException($request);
    });
    return $guard;
});
$container->set('flash', function () { return new Messages(); });
$container->set('view', function() {
    if (!DEVELOPMENT) {
        return Twig::create('swagger/templates', ['cache' => DIR_ROOT.DIRECTORY_SEPARATOR.'swagger/templates/cache']);
    } else {
        return Twig::create(DIR_ROOT.DIRECTORY_SEPARATOR.'swagger/templates');
    }
});
$container->set('dao', function () { return new OpenDataDAOConsumer(); });
$container->set('solr', function () { return new SolrConsumer(); });
$container->set('sirene', function () { return new SireneConsumer(); });
$container->set('accesLibre', function () { return new AccesLibreConsumer(); });
$container->set('user', function() { return null; });

$api->add(TwigMiddleware::createFromContainer($api));
$api->add(new EntryMiddleware($container));

// Redirect to swagger interface only in production environment
if (PRODUCTION) {
    $api->redirect('/', '/swagger', 301);
}

// Routing swagger UI pages and actions
$api->group('/swagger', function (RouteCollectorProxy $group) use ($api) {
    // Page mapping
    $group->get('/',                                ApiOpenData\Controller\View\HomeViewController::class);
    $group->get('/inscription',                     ApiOpenData\Controller\View\SubscribeViewController::class);
    $group->get('/connexion',                       ApiOpenData\Controller\View\ConnectionViewController::class);

    // Action mapping
    $group->post('/action/login',                   ApiOpenData\Controller\Action\AuthenticationAction::class.':login');
    $group->post('/action/forgotten-password',      ApiOpenData\Controller\Action\ForgottenPasswordAction::class);
    $group->post('/action/logout',                  ApiOpenData\Controller\Action\AuthenticationAction::class.':logout');
    $group->post('/action/subscription',            ApiOpenData\Controller\Action\SubscriptionAction::class);
    $group->post('/action/contact',                 ApiOpenData\Controller\Action\ContactAction::class);

    // Authenticated actions and mapping
    $group->group('', function (RouteCollectorProxy $group) {
        $group->get('/compte',                      ApiOpenData\Controller\View\AccountViewController::class);
        $group->post('/action/information-update',  ApiOpenData\Controller\Action\InformationUpdateAction::class);
        $group->post('/action/password-update',     ApiOpenData\Controller\Action\PasswordUpdateAction::class);
        $group->post('/action/regenerate-keys',     ApiOpenData\Controller\Action\RegenerateKeysAction::class);
        $group->post('/action/regenerate-bearer',   ApiOpenData\Controller\Action\RegenerateBearerAction::class);
        $group->post('/action/load/publication',    ApiOpenData\Controller\Action\PublicationAction::class.':load');
        $group->post('/action/add/publication',     ApiOpenData\Controller\Action\PublicationAction::class.':add');
        $group->post('/action/update/publication',  ApiOpenData\Controller\Action\PublicationAction::class.':update');
        $group->post('/action/delete/publication',  ApiOpenData\Controller\Action\PublicationAction::class.':delete');
    })->add(new AuthenticationMiddleware($api->getContainer()));
})->add('csrf');

// Get "token" function
$api->get('/api/token', ApiOpenData\Controller\AuthorizationController::class.':generateBearer');

// Routing API methods
$api->group('/api/v1', function (RouteCollectorProxy $group) {
    // Get "collectivites" function
    $group->get('/collectivites',           ApiOpenData\Controller\Api\v1\CollectiviteController::class);
    // Get "equipements collectifs" function
    $group->get('/equipements-collectifs',  ApiOpenData\Controller\Api\v1\EquipementController::class);
    // Get "equipements publics" function
    $group->get('/equipements-publics',     ApiOpenData\Controller\Api\v1\EquipementPublicController::class);
    // Get "deliberations" function
    $group->get('/deliberations',           ApiOpenData\Controller\Api\v1\DeliberationController::class);
    // Get "actes reglementaires" function
    $group->get('/actes-reglementaires',    ApiOpenData\Controller\Api\v1\ActeReglementaireController::class);
    // Get "budgets" function
    $group->get('/budgets',                 ApiOpenData\Controller\Api\v1\BudgetController::class);
    // Generate budget PDF from XML function
    $group->get('/budgetPDF-from-XML',      ApiOpenData\Controller\Api\v1\BudgetPDFController::class);
})->add(ApiOpenData\Controller\AuthorizationController::class.':checkBearerValidity');

// Define Custom Error Handler
$errorHandler = function (Request $request, Throwable $exception) use ($api) {
    // For swagger UI, display custom error page
    if (strpos($request->getUri()->getPath(), '/swagger') === 0) {
        $request = $request->withAttribute('exception', $exception);
        return $api->get($request->getUri()->getPath(), ApiOpenData\Controller\View\ErrorViewController::class)->run($request);
    } else {
        // For API, return error as JSON
        $response = $api->getResponseFactory()->createResponse();
        $response->withHeader('Content-type', 'application/json');
        $response = $response->withStatus($exception->getCode());
        $response->getBody()->write(
                    json_encode([
                        'success' => false,
                        'message' => $exception->getMessage()
                    ], JSON_UNESCAPED_UNICODE)
        );
        return $response;
    }
};
// Add Error Middleware
$errorMiddleware = $api->addErrorMiddleware(DEVELOPMENT, LOGGING, DEBUG);
if (!DEVELOPMENT) {
    $errorMiddleware->setDefaultErrorHandler($errorHandler);
}

$api->run();