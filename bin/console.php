<?php
/**
 * @license Apache 2.0
 */

use Slim\App;
use App\Factory\LoggerFactory;
use DI\ContainerBuilder;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Slim\Factory\AppFactory;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputOption;
use ApiOpenData\Lib\OpenDataDAOConsumer;
use ApiOpenData\Lib\SireneConsumer;
use ApiOpenData\Lib\SolrConsumer;

require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/../src/inc.config.php';

// Add container definitions
$containerBuilder = new ContainerBuilder();
$containerBuilder->addDefinitions([
    // Application settings
    'settings' => function () {
        $settings = [];
        // Path settings
        $settings['root'] = dirname(__DIR__);
        // Error handler : TODO with config.inc.php
        $settings['error'] = [
            // Should be set to false in production
            'display_error_details' => DEVELOPMENT,
            // Should be set to false for unit tests
            'log_errors' => LOGGING,
            // Display error details in error log
            'log_error_details' => DEVELOPMENT,
        ];
        // Logger settings
        $settings['logger'] = [
            'name'              => 'app',
            'path'              => $settings['root'] . '/logs',
            'filename'          => 'app.log',
            'level'             => Logger::DEBUG,
            'file_permission'   => 0775,
        ];
        // Console commands
        $settings['commands'] = [
            \ApiOpenData\Console\PublicationCommand::class,
        ];
        return $settings;
    },

    'dao' => function () {
        return new OpenDataDAOConsumer();
    },

    'solr' => function () {
        return new SolrConsumer();
    },

    'sirene' => function () {
        return new SireneConsumer();
    },

    App::class => function (ContainerInterface $container) {
        AppFactory::setContainer($container);
        return AppFactory::create();
    },

    // The logger factory
    LoggerFactory::class => function (ContainerInterface $container) {
        return new LoggerFactory($container->get('settings')['logger']);
    },

    Application::class => function (ContainerInterface $container) {
        $application = new Application();
        $application->getDefinition()->addOption(
            new InputOption('--siren', '-s', InputOption::VALUE_REQUIRED, 'SIREN number', 'All by default')
        );
        foreach ($container->get('settings')['commands'] as $class) {
            $application->add($container->get($class));
        }
        return $application;
    }
]);

// Build PHP-DI Container instance
$container = $containerBuilder->build();

// Create App instance
$application = $container->get(App::class);

/** @var ContainerInterface $container */
$containerInterface = $application->getContainer();
$console = $containerInterface->get(Application::class);
$console->run();