--
-- Table structure for table consumer
--
CREATE TABLE consumer (
    id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    firstname varchar(100) NOT NULL,
    lastname varchar(100) NOT NULL,
    email varchar(100) NOT NULL,
    passwordHash varchar(100) NOT NULL,
    salt varchar(10) NOT NULL,
    userKey varchar(36) DEFAULT NULL,
    userSecret varchar(30) DEFAULT NULL,
    admin tinyint(1) NOT NULL DEFAULT 0,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE UNIQUE INDEX i_consumer_email ON consumer (email);

--
-- Table structure for table bearer
--
CREATE TABLE bearer (
    token varchar(500) NOT NULL,
    tokenKey varchar(35) NOT NULL,
    consumerId int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE UNIQUE INDEX i_fk_bearer_consumerId ON bearer (consumerId ASC);
ALTER TABLE bearer ADD CONSTRAINT fk_bearer_consumerId FOREIGN KEY (consumerId) REFERENCES consumer(id) ON DELETE CASCADE;

--
-- Table structure for table activity
--
CREATE TABLE activity (
    uri varchar(500) NOT NULL,
    consumerId int(11) UNSIGNED NOT NULL,
    executionDate timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE activity ADD CONSTRAINT fk_activity_consumerId FOREIGN KEY (consumerId) REFERENCES consumer(id) ON DELETE CASCADE;

--
-- Table structure for table publication
--
CREATE TABLE publication ( 
    siren VARCHAR(9) NOT NULL,
    name VARCHAR(250) NOT NULL,
    frequency INT(11) NOT NULL, 
    state INT(1) NULL,
    message VARCHAR(500) NULL,
    active INT NOT NULL,
    url VARCHAR(500) NULL, 
    PRIMARY KEY (siren(9))
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table publication
--
CREATE TABLE dataset ( 
    siren VARCHAR(9) NOT NULL,
    type VARCHAR(25) NOT NULL,
    creationDate timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updateDate timestamp NULL,
    numberOfLine INT(11) NULL
) ENGINE = InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE dataset ADD CONSTRAINT fk_dataset_siren FOREIGN KEY (siren) REFERENCES publication(siren) ON DELETE CASCADE;
ALTER TABLE dataset ADD UNIQUE(siren, type);