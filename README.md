# API Open Data Gironde Numérique

## Description
API REST Open Data des données locales des collectivités adhérentes de Gironde Numérique et ayant souscrits au service "Données publiques et Open Data".
Cette API s'accompagne en production d'une interface de documentation de type "Swagger". Cette interface ne doit pas être déployée sur un environnement hors production. Il convient de supprimer le dossier swagger sur les environnements de pré-production.

## Pré-requis
- Serveur Apache avec PHP > 7.2
- Extension Apache Solr
- Module Apache header et deflate
- Compte utilisateur Insee Sirene V3
- Serveur Solr

## Installation
- Pour obtenir les dépendances du projet, lancer dans un terminal la commande (composer doit préalablement être installé) : `composer install`
- Générer l'autoload des classes du projet avec la commande : `composer dump-autoload -o`
- Lancer le script de génération du schéma de données dans une base MySQL
- Vérifier les droits d'écriture du dossier controllers/cache (écriture/lecture/suppression)
- Vérifier les droits d'exécution du fichier du dossier controllers/Utils/print-comp.jar
- Paramètrer ensuite le fichier de propriétés src/inc.config.php

## Utilisation
Afin de pouvoir appeler l'API, l'utilisateur doit être inscrit dans le serveur OpenLDAP de Gironde Numérique dans le domaine opendata.girondenumerique.fr.
Une chaîne secrête et une clé doivent être générées afin d'obtenir un jeton d'authentification (*bearer*).
Ce jeton d'une durée de validité de 30 jours est un paramètre obligatoire pour récupérer n'importe quel jeu de données.
Les appels sont limités à 30 par minute. Au delà, l'utilisateur est bloqué pendant 30 secondes. Ces valeurs sont paramétrables à l'aide du fichier de paramétrage.

## Modification de l'interface de l'API
Un rechargement du fichier swagger.json est nécessaire après toutes modifications du contrat d'interface de l'API.
Ces modifications doivent être commentées par annotation Open API (OA) en entête de l'index de l'API et/ou des méthodes impactées.

Le rechargement de ce fichier s'effectue par le biais du projet https://github.com/zircote/swagger-php en lançant la commande suivante depuis le répertoire vendor/bin/ :

```
./openapi /<chemin_projet>/API -o /<chemin_projet>/API/swagger/swagger.json
```

## Intégration d'une nouvelle collectivité dans l'Open Data Gironde Numérique
Pour chaque collectivité souscrivant à l'offre données publiques (Pastell et autres), et de facto à l'offre Open Data permettant l'exposition de ses données par d'autres agrégateurs de données ou autres plateformes de type resourcerie, il est nécessaire de configurer le logo de cette collectivité avant l'indexation de ses données.

Pour cela, il suffit d'ajouter son logo dans le répertoire /logo de l'API en respectant la convention de nommage suivante :
<SIREN>.png

Exemple pour Gironde Numérique : 200010049.png
