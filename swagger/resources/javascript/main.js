$(document).ready(function() {
    // Form validation control
	(() => {
        'use strict';
        const forms = document.querySelectorAll('.needs-validation');
        Array.prototype.slice.call(forms).forEach((form) => {
            form.addEventListener('submit', (event) => {
                if (!form.checkValidity()) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    })();

    // Keep active tab open
    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
        localStorage.setItem('activeTab', $(e.target).attr('href'));
    });
    var activeTab = localStorage.getItem('activeTab');
    if (activeTab) {
        $('#menu a[href="'+activeTab+'"]').tab('show');
    }

    // Close message area.
    $('a[data-toggle="tab"]').click(function() {
        $('.message-area .alert').hide();
    });

    // Copy to clipboard
    $('[data-toggle="tooltip"]').tooltip({
        container: 'body', 
        trigger: 'hover', 
        placement: 'top'
    });
    $('.copy-btn').bind('click', function() {
        var elementId = $(this).closest('div.input-group').find('.input-btn').attr('id');
        var input = document.getElementById(elementId);
        input.select();
        try {
            var success = document.execCommand('copy');
            if (success) {
                $(this).trigger('copied', ['Copié !']);
            } else {
                $(this).trigger('copied', ['Copier avec Ctrl+C']);
            }
        } catch (err) {
            $(this).trigger('copied', ['Copier avec Ctrl+C']);
        }
    });
    $('.copy-btn').hover(function() {
        $(this).tooltip('dispose').attr('title', 'Copier dans le presse-papier').tooltip('show');
    });
    // Handler for updating the tooltip message.
    $('.copy-btn').bind('copied', function(event, message) {
        $(this).tooltip('dispose').attr('title', message).tooltip('show');
    });

});

/** @description Override jQuery show function to show an HTML element. 
 * @param {string} element HTML element to show
 */
jQuery.fn.show = function() {
    $(this).removeClass('d-none').addClass('show');
}

/** @description Override jQuery hide function to hide an HTML element.  
 * @param {string} element HTML element to hide
 */
jQuery.fn.hide = function() {
    $(this).removeClass('show').addClass('d-none');
}

/** @description Function to remove class starting with a specific filter.  
 * @param {string} filter String filter class to remove
 */
jQuery.fn.removeClassStartingWith = function (filter) {
    $(this).removeClass(function (index, className) {
        return (className.match(new RegExp("\\S*" + filter + "\\S*", 'g')) || []).join(' ');
    });
    return this;
};

/**
 * @description Display an HTML element and hide another one.
 * @param {String} a HTML bloc id to display
 * @param {String} b HTML bloc id to hide
 */
function showhide(a, b) {
	$(a).show();
	$(b).hide();
}

/**
 * @description Display a modal view identified by its HTML id.
 * @param {String} idPopUp Modal bloc HTML id.
 */
function showPopUp(idPopUp) {
	$('#'+idPopUp).modal('show');
}